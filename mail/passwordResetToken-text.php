<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $user common\models\User */

?>
Здравствуйте <?= Html::encode($user->fio) ?>,

Введите следующий код на странице восстановления пароля:

<?= $user->password_reset_token ?>