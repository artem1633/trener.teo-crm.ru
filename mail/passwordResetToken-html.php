<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

?>
<div class="password-reset">
    <p>Здравствуйте <?= Html::encode($user->fio) ?>,</p>

    <p>Введите следующий код на странице восстановления пароля:</p>

    <h3><?= $user->password_reset_token ?></h3>
</div>
