<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NutritionEating */

$this->title = 'Create Nutrition Eating';
$this->params['breadcrumbs'][] = ['label' => 'Nutrition Eatings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nutrition-eating-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelne' => $modelne,
        'modelep' => $modelep,
    ]) ?>

</div>
