<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Код Восстановление';

?>


<h1><a class="back_arrow" href="<?=Yii::$app->homeUrl?>"></a><?=$this->title?></h1>
<div class="register-box-body">
        <p>Введите код который вы получили по Email</p>
        <?php $form = ActiveForm::begin(['id' => 'recover-form', 'enableClientValidation' => false]); ?>

        <?= $form
            ->field($model, 'password_reset_token')
            ->label(false)
            ->textInput(['placeholder' => 'XXXXX','autocomplete' => 'off']) ?>

        <?= Html::submitButton('ОТПРАВИТЬ', ['class' => 'btn btn-primary btn-flat', 'name' => 'recover-button']) ?>

        <?php ActiveForm::end(); ?>
</div>
<!-- /.login-box-body -->