<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Восстановление пароля';

?>


<h1><a class="back_arrow" href="<?=Yii::$app->homeUrl?>"></a><?=$this->title?></h1>
<div class="register-box-body">
	<?php 
        if(isset($model)){
    ?>
            <?php $form = ActiveForm::begin(['id' => 'recover-form', 'enableClientValidation' => false]); ?>

            <?= $form
                ->field($model, 'email')
                ->label(false)
                ->textInput(['placeholder' => 'Email:','autocomplete' => 'off']) ?>

            <p>На указанную почту придёт письмо с инструкиями по восстановлению пароля</p>
            <?= Html::submitButton('ОТПРАВИТЬ', ['class' => 'btn btn-primary btn-flat', 'name' => 'recover-button']) ?>

            <?php ActiveForm::end(); ?>

        	<div class="register-footer">
        		Создавая аккаунт в  сервисе Дневник Тренера, вы принимаете<br>
        		<a href="/site/agreement">Пользовательское соглашение</a> и <a href="/site/privacy-policy">Политику конфеденциальности</a>
        	</div>

    <?php  
        } else {
            echo '<p>Мы отправили письмо с инструкциями по восстановления пароля на вашу почту</p>';
        }
    ?>
</div>
<!-- /.login-box-body -->