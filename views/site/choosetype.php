<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Выбор типа профиля';

?>


<h1><a class="back_arrow" href="<?=Yii::$app->homeUrl?>"></a><?=$this->title?></h1>
<div class="register-box-body">
	        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>

	        <?= $form
	            ->field($model, 'username')
	            ->label(false)
	            ->textInput(['placeholder' => 'Email:','autocomplete' => 'off']) ?>

	        <?= $form
	            ->field($model, 'password')
	            ->label(false)
	            ->passwordInput(['placeholder' => 'Пароль:','autocomplete' => 'off']) ?>

	        <!-- <div class="row">
	            <div class="col-xs-8">
	                <?= $form->field($model, 'rememberMe')->checkbox()->label('Запомни меня') ?>
	            </div>
	            <div class="col-xs-4">
	            </div>
	        </div> -->
	        <?= Html::submitButton('ВОЙТИ', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
			<div class="login-box-links">
				<a class="btn btn-block btn-link" href="/site/recover">Забыли пароль?</a>
				<a class="btn btn-block btn-link" href="/site/register">Зарегистрироваться</a>
			</div>

	        <?php ActiveForm::end(); ?>
		</div>

        <div class="social-auth-links text-center">
            <p>Или войти с помощью:</p>
            
            <?= yii\authclient\widgets\AuthChoice::widget([
                 'baseAuthUrl' => ['site/auth'],
                 'popupMode' => true,
            ]) ?>
        </div>
		
		<a class="register-link" href="/site/register">Зарегистрироваться для тренера</a>
		<div class="login-footer">
			Создавая аккаунт в  сервисе Дневник Тренера, вы принимаете<br>
			<a href="">Пользовательское соглашение</a> и <a href="">Политику конфеденциальности</a>
		</div>

</div><!-- /.login-box -->
