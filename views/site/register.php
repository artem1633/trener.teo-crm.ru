<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Регистрация';

?>


<h1><a class="back_arrow" href="<?=Yii::$app->homeUrl?>"></a><?=$this->title?></h1>
<div class="register-box-body">
	
    <?php $form = ActiveForm::begin(['id' => 'register-form', 'enableClientValidation' => false]); ?>

    <?= $form
        ->field($model, 'email')
        ->label(false)
        ->textInput(['placeholder' => 'Email:','autocomplete' => 'off']) ?>

    <?= $form
        ->field($model, 'password')
        ->label(false)
        ->passwordInput(['placeholder' => 'Пароль:','autocomplete' => 'off']) ?>
    <?= $form
        ->field($model, 'name')
        ->label(false)
        ->textInput(['placeholder' => 'Имя:','autocomplete' => 'off']) ?>
    <?= $form
        ->field($model, 'surname')
        ->label(false)
        ->textInput(['placeholder' => 'Фамилия:','autocomplete' => 'off']) ?>

    <div class="hidden">
        <?= $form->field($model, 'referralId')->hiddenInput() ?>
    </div>

    <?= Html::submitButton('ЗАРЕГИСТРИРОВАТЬСЯ', ['class' => 'btn btn-primary btn-flat', 'name' => 'register-button']) ?>

    <?php ActiveForm::end(); ?>

	<div class="register-footer">
		Создавая аккаунт в  сервисе Дневник Тренера, вы принимаете<br>
		<a href="/site/agreement">Пользовательское соглашение</a> и <a href="/site/privacy-policy">Политику конфеденциальности</a>
	</div>
</div>
<!-- /.login-box-body -->