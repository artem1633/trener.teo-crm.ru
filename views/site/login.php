<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title ='Войти';
?>
<div class="mobile-box">
    <div class="login-box-body">
        <h2 style="font-family: 'roboto-thin' !important; text-transform: uppercase;"><?=Yii::$app->name?></h2>
		<div class="login-bg">
	        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>

	        <?= $form
	            ->field($model, 'username')
	            ->label(false)
	            ->textInput(['placeholder' => 'Email:','autocomplete' => 'off']) ?>

	        <?= $form
	            ->field($model, 'password')
	            ->label(false)
	            ->passwordInput(['placeholder' => 'Пароль:','autocomplete' => 'off']) ?>

	        <?= Html::submitButton('ВОЙТИ', ['class' => 'btn btn-primary btn-flat', 'name' => 'login-button']) ?>
			<div class="login-box-links">
				<a class="btn btn-block btn-link" href="/site/recover">Забыли пароль?</a>
				<a class="btn btn-block btn-link" href="/site/register">Зарегистрироваться</a>
			</div>

	        <?php ActiveForm::end(); ?>
		</div>

        <div class="social-auth-links text-center">
            <p style="font-family: 'roboto-thin' !important; text-transform: uppercase; font-weight: 200; font-size: 13px;">Или войти с помощью:</p>
        </div>
		
		<a class="register-link" href="/site/register?type=trener" style="font-family: 'roboto-thin'; text-transform: uppercase;">Зарегистрироваться для тренера</a>
		<div class="login-footer" style="color: #cccccc !important;">
			Создавая аккаунт в  сервисе Дневник Тренера, вы принимаете<br>
			<a style="color: #cccccc !important;" href="/site/agreement">Пользовательское соглашение</a> и <a style="color: #cccccc !important;" href="/site/privacy-policy">Политику конфеденциальности</a>
		</div>
    </div>
    <!-- /.login-box-body -->
</div><!-- /.login-box -->
