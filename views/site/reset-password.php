<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Изменение пароля';
?>
<h1><a class="back_arrow" href="<?=Yii::$app->homeUrl?>"></a><?=$this->title?></h1>
<div class="register-box-body">

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

            <?= $form->field($model, 'password')->passwordInput(['autofocus' => true])->label(false) ?>
            <p>Введите новый пароль для авторизации</p>

            <div class="form-group field-resetpasswordform-password">
                <?= Html::submitButton('Изменить пароль', ['class' => 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
