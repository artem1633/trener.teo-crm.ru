<?php

use Yii;

/**
 * @var \app\models\AffiliateProgram $myReferrals
 */

$this->title = 'Партнерская программа';

?>

<h4 style="font-family: Ubuntu !important; font-weight: 400; text-align: center; font-size: 18px;">Ваша партнерская ссылка</h4>
<h4 id="partner-url" style="font-family: Ubuntu !important; font-weight: 500; text-align: center; font-size: 17px;"><?=Yii::$app->user->identity->referralUrl?> <i class="fa fa-copy"></i></h4>

<ul style="padding-left: 0; margin-top: 45px;">
    <li style="list-style-type: none; border-bottom: 1px solid #dedede; padding: 10px 3%;">
        <span style="text-transform: uppercase; font-family: Ubuntu; font-weight: 200; color: #8c8c8c">Зарегистрировано парнеров<b class="pull-right" style="color: #000;"><?=count($myReferrals)?></b></span>
    </li>
    <li style="list-style-type: none; border-bottom: 1px solid #dedede; padding: 10px 3%;">
        <span style="text-transform: uppercase; font-family: Ubuntu; font-weight: 200; color: #8c8c8c">Сумма платежей<b class="pull-right" style="color: #000;">0 руб.</b></span>
    </li>
</ul>


<h4 style="margin-left: 3%; margin-top: 40px; font-size: 20px; font-family: Ubuntu !important; font-weight: 600;">Партнеры</h4>

<div class="items-list">
    <?php foreach ($myReferrals as $referral): ?>
        <li>
            <a href="#" class="list-item" style="width: 100%;" data-pjax="0">
                <img class="profile-image image-bordered" src="/<?=$referral->registered->photo?>" style="float: left;"><div style="margin-left: 90px; margin-top: 15px;"><span style="font-size: 17px;"><?=$referral->registered->fio?></span><br><span style="font-size: 13px;">Дата регистрации: <?=date('d.m.Y', strtotime($referral->register_date))?></span></div>
            </a>
        </li>
    <?php endforeach; ?>
</div>

<?php

$script = <<< JS
    $('#partner-url').click(function(){
        if(copyToClipboard(document.getElementById("partner-url")))
        {
            alert('Скопировано');
        }
    });
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>