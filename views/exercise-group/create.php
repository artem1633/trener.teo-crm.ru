<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ExerciseGroup */

?>
<div class="exercise-group-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
