<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this \yii\web\View */
/* @var $content string */

dmstr\web\AdminLteAsset::register($this);
\app\assets\plugins\MmenuAsset::register($this);

if(Yii::$app->user->isGuest == false){
    $clientRequestsCount = \app\models\ClientsRequests::find()->where(['trainer_id' => Yii::$app->user->getId(), 'accepted' => null])->count();
    $clientRequestsTrainerMarker = Yii::$app->user->identity->isEnableNotificationMarker('client_requests_trainer');
} else {
    $clientRequestsCount = 0;
    $clientRequestsTrainerMarker = false;
}

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="<?=Yii::$app->controller->id.' '.Yii::$app->controller->action->id?>-page">

<?php $this->beginBody() ?>

<?php if(Yii::$app->user->isGuest == false): ?>
    <nav id="menu">
        <ul>
            <div class="menu-profile">
                <img class="menu-profile-avatar image-bordered" data-img-upload="upload" src="/<?=Yii::$app->user->identity->photo?>">
                <div class="menu-profile-name"><?=Yii::$app->user->identity->fio?></div>
            </div>
            <li><a href="<?=Url::toRoute(['users/profile', 'id' => Yii::$app->user->getId()])?>"><i class="fa fa-user-circle text-theme"></i> <span class="a-text">Мой профиль</span></a></li>

            <li><a href="/exercise-group"><i class="fa fa-file-text text-theme"></i> <span class="a-text">Упражнения</span></a></li>
            <li><a href="/training"><i class="fa fa-list text-theme"></i> <span class="a-text">Тренировки</span></a></li>
            <li><a href="/training/coachings-users"><i class="fa fa-list text-theme"></i> <span class="a-text">Активные Тренировки</span></a></li>
            <li><a href="/nutritions"><i class="fa fa-cutlery text-theme"></i> <span class="a-text">Питание</span></a></li>
            <?php if(Yii::$app->user->identity->permission == \app\models\Users::USER_ROLE_TRENER): ?>
                <li><a href="/payments"><i class="fa fa-credit-card text-theme"></i> <span class="a-text">Оплаты</span></a></li>
            <?php else: ?>
                <li><a href="/payments/my"><i class="fa fa-credit-card text-theme"></i> <span class="a-text">Оплаты</span></a></li>
            <?php endif; ?>
            <?php if(Yii::$app->user->identity->permission == \app\models\Users::USER_ROLE_TRENER): ?>
                <li><a href="/clients"><i class="fa fa-user-o text-theme"></i> <span class="a-text">Клиенты</span></a></li>
                                <li><a href="/clients/requests">
                                                <i class="fa fa-user-o text-theme"></i>
                                                <span class="a-text">Запросы клиентов</span>
                                                <?php if($clientRequestsCount > 0): ?>
                                                        <?php if($clientRequestsTrainerMarker): ?>
                                                                <span class="marker marker-danger pull-right"><?=$clientRequestsCount?></span>
                                                            <?php else: ?>
                                                                <span class="marker pull-right"><?=$clientRequestsCount?></span>
                                                            <?php endif; ?>
                                                    <?php endif; ?>
                                            </a>
                                    </li>
                <li><a href="/affiliate-program"><i class="fa fa-users text-theme"></i> <span class="a-text">Партнерская программа</span></a></li>
            <?php endif; ?>
            <?php if(Yii::$app->user->identity->permission == \app\models\Users::USER_ROLE_CLIENT): ?>
                <li><a href="/treners"><i class="fa fa-user-o text-theme"></i> <span class="a-text">Тренеры</span></a></li>
            <?php endif; ?>
            <li><a href="<?=Url::toRoute(['site/logout'])?>" class="text-danger"><i class="fa fa-sign-out text-theme"></i> <span class="a-text">Выйти</span></a></li>
        </ul>
    </nav>
<?php endif; ?>
<div class="mobile-header">
    <div class="header-content">
        <?php if(Yii::$app->user->isGuest == false): ?>
            <a href="#menu">
                <div id="menu-icon" class="hamburger hamburger--slider" type="button">
                    <div class="hamburger-box">
                        <div class="hamburger-inner"></div>
                    </div>
                </div>
            </a>
        <?php endif; ?>
        <div class="mobile-heading"> <?= Html::encode($this->title) ?> </div>
        <div class="header-action-button">
            <?php if(isset($this->params['header-action-button']) && $this->params['header-action-button'] != null): ?>
                <?= $this->params['header-action-button'] ?>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php if(isset($this->params['header-after']) && $this->params['header-after'] != null): ?>
    <div class="mobile-after-header clearfix">
        <?php foreach ($this->params['header-after'] as $item): ?>
            <div class="header-tab <?=$item['active'] == true ? 'active' : ''?>" style="width: 50%;"><a href="<?=$item['url']?>"><?=$item['label']?></a></div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>

<div class="content_wrapper">
	<div class="mobile-box">
		<?= $content ?>
	</div>
</div>

<?php if(Yii::$app->user->isGuest == false): ?>
    <?php $form = ActiveForm::begin([
        'id' => 'upload-img-form',
        'action' => ['users/set-avatar'],
//        'enableClientValidation' => false,
        'options' => ['enctype' => 'multipart/form-data', 'class' => 'hidden', 'data-avatar-url' => Url::toRoute(['users/set-avatar']), 'data-form-url' => Url::toRoute(['users/upload-form-image'])],
    ]);
    $uploadModel = new \app\models\form\UploadForm();
    ?>

        <?= $form->field($uploadModel, 'imageFile[]')->fileInput() ?>

    <?php ActiveForm::end() ?>
<?php endif; ?>

<div id="upload-img-form-depricated" class="form-action" data-action="<?=Url::toRoute(['users/set-avatar'], true)?>">

</div>

<?php

$this->registerJs(file_get_contents('js/scripts/upload-image.js'), \yii\web\View::POS_READY);

?>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
