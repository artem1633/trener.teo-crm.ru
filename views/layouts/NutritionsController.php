<?php

namespace app\controllers;

use Yii;
use app\models\Nutritions;
use yii\data\ActiveDataProvider;

use app\models\NutritionEating;
use app\models\NutritionComments;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;



/**
 * NutritionsController implements the CRUD actions for Nutritions model.
 */
class NutritionsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Nutritions models.
     * @return mixed
     */
    public function actionIndex()
    {

      //  $dataProvider = new ActiveDataProvider([
       //    'query' => Nutritions::find(),
     //  ]);

      //  return $this->render('index', [
       //    'dataProvider' => $dataProvider,
     //  ]);
        //kaloriya summasi
            $query = (new \yii\db\Query())->where(['nutrition_id'=> '1'])->from('Nutrition_eating');
            $sum = $query->sum('calorie');
               
         $nutritions = Nutritions::find()->all();
        $calories = NutritionEating::find()->where(['nutrition_id' => '1'])->all();
        $caloriess = NutritionEating::find()->where(['nutrition_id' => '1'])->count();
       
        return $this->render('index',compact('nutritions', 'caloriess','sum','calories'));
    
       

    }

    /**
     * Displays a single Nutritions model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)

    {
         $query = (new \yii\db\Query())->where(['nutrition_id'=> '1'])->from('Nutrition_eating');
            $sum = $query->sum('calorie');
        $comments = NutritionComments::find()->where(['nutrition_id' => '1'])->all();
        $calories = NutritionEating::find()->where(['nutrition_id' => '1'])->all();
        $nutritions = Nutritions::findOne($id);
        return $this->render('view', compact('nutritions','calories', 'comments' ,'sum'));
            
        
    }

    /**
     * Creates a new Nutritions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Nutritions();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Nutritions model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Nutritions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Nutritions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Nutritions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Nutritions::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
