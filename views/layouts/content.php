<?php
use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;

?>
<div class="content-wrapper" style="background-color: #fff">
    <section class="content-header">
        <?php if (isset($this->blocks['content-header'])) { ?>
            <h1><?= $this->blocks['content-header'] ?></h1>
        <?php } else { ?>
            <h1>
                <?php
                if ($this->title !== null) {
                    echo \yii\helpers\Html::encode($this->title);
                } else {
                    echo \yii\helpers\Inflector::camel2words(
                        \yii\helpers\Inflector::id2camel($this->context->module->id)
                    );
                    echo ($this->context->module->id !== \Yii::$app->id) ? '<small>Module</small>' : '';
                } ?>
            </h1>
        <?php } ?>

        <?=
        Breadcrumbs::widget(
            [
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]
        ) ?>
    </section>

    <section class="content">
        <?= Alert::widget() ?>
        <?= $content ?>
    </section>
</div>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.0
    </div>
    <strong>Copyright &copy; 2018 <a href="http://trener-crm.ru">TEO</a>.</strong> All rights
    reserved.
</footer>

<?php if (isset(Yii::$app->user->identity->id)) {
    $check_role = \app\models\Functions::getUserRole(Yii::$app->user->identity->id);
    if ($check_role == "Тренер") { ?>
        <!-- The Right Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Content of the sidebar goes here -->
            <h5 class="text-center">Мои Клиенты</h5>
            <?php
            $clients = \app\models\Functions::getTrainerClients();

            if (count($clients) > 0) { ?>
                <div class="right_menu_ul">
                    <ul class="text-center">
                        <?php
                        foreach ($clients as $client) { ?>
                            <li>
                                <a href="/users/profile?id=<?= $client -> id ?>"><?= $client -> fio ?></a>
                            </li>
                        <?php   }
                        ?>
                    </ul>
                </div>
            <?php }
            ?>
        </aside>
        <!-- The sidebar's background -->
        <!-- This div must placed right after the sidebar for it to work-->
        <div class="control-sidebar-bg"></div>
  <?php  } ?>

<?php } ?>

