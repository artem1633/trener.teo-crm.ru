<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */


$this->title .=' - '.Yii::$app->name;
if (Yii::$app->user->identity['permission']=='administrator') { 

    echo $this->render(
        'main-login',
        ['content' => $content]
    );
} else {
    $this->registerCssFile("@web/css/new.css", ['depends' => [\yii\bootstrap\BootstrapAsset::className()],]);
    echo $this->render(
        'mobile',
        ['content' => $content]
    );    
} ?>
