<?php

use app\models\Users;
use app\models\Functions;
use yii\helpers\Html;

$permission = Yii::$app->user->identity->permission;
$id = Yii::$app->user->identity->id;
?>
<aside class="main-sidebar left_menu">
    <section class="sidebar">
        <?php

        if(isset(Yii::$app->user->identity->id))
            {
                $current_user = Yii::$app->user->identity->id; ?>

                <ul class="user">
                    <li class="user-header">
                        <div class="row">
                            <div class="col-xs-4">
                                <?php
                                    if (!empty(Yii::$app->user->identity->photo)) { ?>
                                        <?php $res = '/web/' ?>
                                        <img style="width: 50px;height: 50px;border-radius: 50px;" src="<?=Yii::$app->user->identity->photo?>" class="" alt="User Image"/>

                                    <?php    } else { ?>
                                        <img style="width: 50px" src="/logos/nouser.png" class="img-circle" alt="User Image"/>

                                    <?php    }
                                ?>

                            </div>
                            <div class="col-xs-8">
                                <p class="user_p">
                                    <?= Yii::$app->user->identity->fio ?>
                                </p>
                            </div>
                        </div>

                    </li>
                </ul>

                <?php

                if (Functions::getUserRole($current_user) == "Администратор") {
                    echo dmstr\widgets\Menu::widget(
                        [
                            'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                            'items' => [
                                ['label' => 'Admin Menu', 'options' => ['class' => 'header']],
                                ['label' => 'Пользователи', 'icon' => 'users', 'url' => ['/users'],],
                                ['label' => 'создание упражнений', 'icon' => 'tasks', 'url' => ['/exercise'],],
                                ['label' => 'база данных тренеров', 'icon' => 'users', 'url' => ['/users'],],
                                ['label' => 'база данных клиентов', 'icon' => 'users', 'url' => ['/users'],],
                                ['label' => 'настройка оплаты', 'icon' => 'gear', 'url' => ['/payment'],],
                                ['label' => 'партнерская программа', 'icon' => 'gear', 'url' => ['/partner-program'],],
                                [
                                    'label' => 'Справочники',
                                    'icon' => 'book',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Группа упражнений', 'icon' => 'file-text-o', 'url' => ['/exercise-group'],],
                                        ['label' => 'Тренировка', 'icon' => 'file-text-o', 'url' => ['/training'],],
                                    ],
                                ],
                            ],
                        ]
                    );
                }
                if (Functions::getUserRole($current_user) == "Тренер") {
                    echo dmstr\widgets\Menu::widget(
                        [
                            'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                            'items' => [
                                ['label' => 'Trainer Menu', 'options' => ['class' => 'header']],
                                ['label' => 'Мой Профиль', 'icon' => 'user', 'url' => ['/users/profile?id=' . $current_user],],
                                ['label' => 'Упражнения', 'icon' => 'tasks', 'url' => ['/exercise'],],
                                ['label' => 'Мои Клиеты', 'icon' => 'users', 'url' => ['/users/trainer-users'],],
                                ['label' => 'Питание', 'icon' => 'cutlery', 'url' => ['/nutrition-program'],],
                                ['label' => 'Оплаты', 'icon' => 'money', 'url' => ['/payments'],],
                                ['label' => 'Партнерская Программа', 'icon' => 'user-circle', 'url' => ['/payment'],],
                                ['label' => 'Сообщения', 'icon' => 'envelope-o', 'url' => ['/chat/trainer-chat'],],
                                [
                                    'label' => 'Справочники',
                                    'icon' => 'book',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Группа упражнений', 'icon' => 'file-text-o', 'url' => ['/exercise-group'],],
                                        ['label' => 'Тренировка', 'icon' => 'file-text-o', 'url' => ['/training'],],
                                    ],
                                ],
                            ],
                        ]
                    );
                }
                if (Functions::getUserRole($current_user) == "Клиент") {
                    echo dmstr\widgets\Menu::widget(
                        [
                            'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                            'items' => [
                                ['label' => 'Client Menu', 'options' => ['class' => 'header']],
                                ['label' => 'Профиль', 'icon' => 'users', 'url' => ['/users/client-profile?id='.$current_user],],
                                ['label' => 'Тренировки', 'icon' => 'fa fa-bullhorn', 'url' => ['/training/training-list'],],
                                ['label' => 'Питание', 'icon' => 'file-text-o', 'url' => ['/exercise/nutrition'],],
                                ['label' => 'Оплаты', 'icon' => 'money', 'url' => ['/payments'],],
                                ['label' => 'Список тренеров', 'icon' => 'file-text-o', 'url' => ['users/trainers-list'],],
                                [
                                    'label' => 'Справочники',
                                    'icon' => 'book',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Группа упражнений', 'icon' => 'file-text-o', 'url' => ['/exercise-group'],],
                                        ['label' => 'Тренировка', 'icon' => 'file-text-o', 'url' => ['/training'],],
                                    ],
                                ],
                            ],
                        ]
                    );
                }
            }
        ?>
    </section>
</aside>




