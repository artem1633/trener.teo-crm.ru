<?php
use yii\helpers\Html;

if (class_exists('backend\assets\AppAsset')) {
        backend\assets\AppAsset::register($this);
    } else {
        app\assets\AppAsset::register($this);
    }

    dmstr\web\AdminLteAsset::register($this);

    $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
$this->beginPage() ?><!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <?php
    $session = Yii::$app->session;
    if($session['menu'] == null || $session['menu'] == 'large') $position="sidebar-collapse";
    else $position="";
    ?>
<body class="hold-transition skin-blue sidebar-mini <?=$position ?>">
    <?php $this->beginBody() ?>
    <div class="wrapper">

        <?= $this->render(
            'header.php',
            ['directoryAsset' => $directoryAsset]
        ) ?>

        <?= $this->render(
            'left.php',
            ['directoryAsset' => $directoryAsset]
        )
        ?>

        <?= $this->render(
            'content.php',
            ['content' => $content, 'directoryAsset' => $directoryAsset]
        ) ?>

<?php $script = <<< JS
    $(document).ready(function() {
        // $(document).ready(function() {});
        $('.user_p').width() <= 10 ? $('.user').hide() : $('.user').show();
        console.log($('.user_p').width());
        
        $('.sidebar-toggle').on('click', function() {
            
            if ($('.user_p').width() >= 100) {
                console.log($('.user_p').width());
                $('.user').hide();
            } else if ($('.user_p').width() == 0){
                console.log($('.user_p').width());
                $('.user').show();
            }
            
            //$('.user_p').width() == 0 ? $('.user').hide() : $('.user').show();
        });
    });         
JS;
$this -> registerJS($script);
?>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>