<?php
use yii\helpers\Html;
use app\models\Soglosovaniya;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset; 
use yii\helpers\Url;
use app\models\Settings;

?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">Trener</span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" onclick="$.post('/site/menu-position');" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

                <?php if (isset(Yii::$app->user->identity->id)) { ?>
                    <li class="user-menu">
                        <?= Html::a(
                            '',
                            ['/site/logout'],
                            [
                                'data-method' => 'post',
                                'class' => 'btn fa fa-sign-out',
                                'style' => 'background-color: #3c8dbc;
                                border: none; 
                                padding-top: 5px!important; 
                                padding-bottom:5px!important;',
                            ]
                        ) ?>
                    </li>
                <?php   } ?>

                <li>
                    <button class="btn right_menu_btn" data-toggle="control-sidebar"><span class="fa fa-users"></span></button>
                </li>
            </ul>
        </div>
    </nav>
</header>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => "modal-lg",
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>