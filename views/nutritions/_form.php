<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Nutritions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="nutritions-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'protein')->textInput() ?>

    <?= $form->field($model, 'fat')->textInput() ?>

    <?= $form->field($model, 'carbohydrate')->textInput() ?>

    <?php
    
    foreach($model->nutritionEatings as $key=>$eating){
     
     echo $form->field($eating, "[$key]name")->textInput();
     
     echo $form->field($eating, "[$key]begintime")->textInput();
    
    } ?>


    
      
    



    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
