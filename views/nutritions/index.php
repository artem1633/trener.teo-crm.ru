<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

$this->title = 'Программы питания';

if(Yii::$app->user->identity->permission == 'trener'){
    $this->params['header-action-button'] = Html::a('<i class="fa fa-plus"></i>', ['create']);
}

\johnitvn\ajaxcrud\CrudAsset::register($this);
\app\assets\plugins\TouchSwipeAsset::register($this);

?>



<?php \yii\widgets\Pjax::begin(['id' => 'crud-list-pjax', 'enablePushState' => false]) ?>
      <ul class="items-list">
        <?php foreach ($programs as $key => $program): ?>
            <li>
                <?= Html::a($program['name'].'<br><span style="font-size: 12px;">'.$program["count"].' приема</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <span style="font-size: 12px;">Тренер: '.$program['trenerName'].'</span>',
                    ['view', 'id' => $key], ['class' => 'list-item', 'style' => 'width: 100%;', 'data-pjax' => 0]) ?>
                <?php if(Yii::$app->user->identity->permission != \app\models\Users::USER_ROLE_CLIENT): ?>
                    <a class="item-action item-success" href="<?=Url::toRoute(['update', 'id' => $key])?>"><i class="fa fa-pencil"></i></a>
                    <?= Html::a('<i class="fa fa-trash"></i>', ['delete', 'id' => $key], ['class' => "item-action item-danger",
                        'role'=>'modal-remote','title'=>'Удалить',
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                        'data-toggle'=>'tooltip',
                        'data-confirm-title'=>'Подтвердите действие',
                        'data-confirm-message'=>'Вы уверены что хотите удалить этот элемент?'
                    ]) ?>
                <?php endif; ?>
            </li>
        <?php endforeach; ?>
      </ul>
<?php \yii\widgets\Pjax::end()?>



<?php

if(Yii::$app->user->identity->permission != \app\models\Users::USER_ROLE_CLIENT)
{
    $this->registerJs(file_get_contents('js/scripts/list-swipe.js'), \yii\web\View::POS_READY);
}

?>