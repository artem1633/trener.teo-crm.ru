<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Nutritions */

$this->title = 'Добавить питание';
$this->params['breadcrumbs'][] = ['label' => 'Nutritions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nutritions-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'newEating' => $newEating,
    ]) ?>

</div>
