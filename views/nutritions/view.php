<?php
use php_rutils\RUtils;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use app\models\form\NutritionComment;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Программи питания';

$this->params['header-action-button'] = Html::a('<i class="fa fa-plus"></i>', ['nutrition-eating/create', 'nutrition_id' => $id]);

?> 

<div class="registerr-box">
  <div class="nutrition-body">
  
<?php foreach($programs as $program):?>
   <h4 style="font-family: Ubuntu-Regular !important; margin-left: 4%; margin-bottom: 20px;"> <?= $program['name']?> </h4>
   <table class="table">
    <tr>
      <th scope="col" class="nutrition-table-shrift">Всего Ккал:</th>
      <th scope="col"></th>
      <th scope="col" class="nutrition-table-shrift2"><?= $program['callorySumm'];?> ккал</th>
    </tr>
    <tr>
      <th scope="col" class="nutrition-table-shrift">Белки:</th>
      <th scope="col"></th>
      <th scope="col" class="nutrition-table-shrift2"><?= $program['protein'];?> г.</th>
    </tr>
    <tr>
      <th scope="col" class="nutrition-table-shrift">Жиры:</th>
      <th scope="col"></th>
      <th scope="col" class="nutrition-table-shrift2"><?= $program['fat'];?> г.</th>
    </tr>
    <tr style="border-bottom: 1px solid #f4f4f4">
      <th scope="col" class="nutrition-table-shrift">Углеводы:</th>
      <th scope="col"></th>
      <th scope="col" class="nutrition-table-shrift2"><?= $program['carbohydrate'];?> г.</th>
    </tr>
    </table>
      
<?php foreach($nutritionEatings as $eating):?>
    <table class="table table_profile">

    <tr>
      <th score="col"><p style="color: #303030;"><?= $eating->name;?>&nbsp&nbsp<?= $eatings->eating_begintime;?></p>
          <?php if(Yii::$app->user->identity->permission == 'trener' && Yii::$app->user->getId() == $nutrition->trener_id): ?>
              <?= Html::a('Добавить продукт', ['nutritions-eating-products/create', 'nutrition_eating_id' => $eating->id]); ?>
          <?php endif; ?>
      </th>
    </tr>
    <tr style="border-bottom: 1px solid #f4f4f4">
      <th scope="col" class="nutrition-table-shrift">Продукт:</th>
      <th scope="col" class="nutrition-table-shrift">Порция</th>
      <th scope="col" class="nutrition-table-shrift">Калория</th>
    </tr>
    <?php foreach($eating->nutritionsEatingProducts as $neps):?>
     
    <tr style="border-bottom: 1px solid #f4f4f4">
      <th scope="col" class="nutrition-table-shrift2"><?= $neps['name'];?></th>
      <th scope="col" class="nutrition-table-shrift2"><?= $neps['portion'];?> г.</th>
      <th scope="col" class="nutrition-table-shrift2"><?= $neps['calorie'];?></th>
    </tr>
  
  <?php endforeach;?>
      
</table>
<?php endforeach;?>
<?php endforeach;?>
    <h6 style="font-family: Ubuntu-Regular !important; font-size: 16px; margin-top: 35px;">&nbsp&nbsp Комментарии клиента по программе</h6>
      <?php $commentModel = new NutritionComment(); $form = ActiveForm::begin(['id' => 'comment-form', 'action' => Url::toRoute(['nutritions/add-comment'])]) ?>
            <?= $form->field($commentModel, 'text')->textarea() ?>

            <div class="hidden">
                <?= $form->field($commentModel, 'nutritionId')->hiddenInput(['value' => $id]) ?>
            </div>

      <?= Html::submitButton('Отправить', ['class' => 'btn btn-theme btn-block', 'style' => 'margin-bottom: 20px;']); ?>

      <?php ActiveForm::end() ?>
      <div class="items-list">
<?php \yii\widgets\Pjax::begin(['id' => 'container-comments-pjax', 'enablePushState' => false]) ?>
          <?php foreach($comments as $comment):?>
              <li>
                  <a href="<?=Url::toRoute(['client', 'id' => $comment->author->id])?>" class="list-item no-after" style="width: 100%;" data-pjax="0">
                      <img class="profile-image image-bordered" src="/<?=$comment->author->photo?>" style="float: left; width: 50px; height: 50px;"><div style="margin-left: 63px; margin-top: 1px;"><span style="font-size: 15px;"><?=$comment->author->fio?></span>
                          <span class="pull-right" style="font-size: 13px; color: #707070; padding-right: 10px;"><?=Yii::$app->RUtilsDtFormatter->asTimePass($comment->created);?></span><br><p style="color: #565656; font-size: 13px; padding-right: 10px;"><?=$comment->text?></p></div>
                  </a>
              </li>
          <?php endforeach;?>
<?php \yii\widgets\Pjax::end() ?>
      </div>
  </div>
</div>
</div>

<?php

$script = <<< JS
    $('#comment-form').unbind('submit');
    $('#comment-form').submit(function(e){
        e.preventDefault();
        
        if($('#nutritioncomment-text').val() != '')
        {
            $.ajax({
                method: 'POST',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                success: function(){
                    $.pjax.reload('#container-comments-pjax');
                    $('#nutritioncomment-text').val('');
                }
            });       
        }
    });
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>

