<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NutritionsEatingProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Nutritions Eating Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nutritions-eating-products-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Nutritions Eating Products', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'portion',
            'calorie',
            'nutrition_eating_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
