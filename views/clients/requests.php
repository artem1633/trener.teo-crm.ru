<?php

use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var \app\models\ClientsRequests[] $requests */

?>

<ul class="items-list">
    <?php foreach ($requests as $model): ?>
        <?php $word = $model->client->gender === \app\models\Users::USER_FEMALE ? 'отправила' : 'отправил'; ?>
        <li>
            <?=Html::a( '<img class="profile-image image-bordered" src="/'.$model->client->photo.'" style="float: left; width: 50px; height: 50px;"><span style="margin-left: 10px;">'.$model->client->fio."</span><br><span style='margin-left: 10px; font-size: 12px;'>".Yii::$app->RUtilsDtFormatter->asTimePass($model->created_at)." ".$word." запрос</span>
                        ".Html::a("Принять запрос",'#', ['class' => 'btn btn-xs btn-theme btn-accept', 'style' => 'margin-left: 11px; margin-bottom: 15px; display: inline-block;',
                            'onclick' => ' var self = $(this);
                                Controlls.sendBinaryRequest($(this), "'.\yii\helpers\Url::toRoute(['accept']).'", {"client_id": '.$model->client_id.', "trainer_id": '.$model->trainer_id.'}, null, "Ошибка", function(data){
                                    var btnRefuse = self.parent().find(".btn-refuse");
                                    var width = Math.round(self.width() + btnRefuse.width() + 2);
                                    self.parent().find(".btn-refuse").hide();
                                    self.width(width);
                                    self.html("Принято");
                                });
                            '])
                .Html::a("Отклонить",'#', ['class' => 'btn btn-xs btn-default btn-refuse', 'style' => 'margin-bottom: 15px; margin-left: 2px;', 'onclick' => ' var self = $(this);
                                Controlls.sendBinaryRequest($(this), "'.\yii\helpers\Url::toRoute(['refuse']).'", {"client_id": '.$model->client_id.', "trainer_id": '.$model->trainer_id.'}, null, "Ошибка", function(data){
                                    var btnAccept = self.parent().find(".btn-accept");
                                    var width = Math.round(self.width() + btnAccept.width() + 2);
                                    self.parent().find(".btn-accept").hide();
                                    self.width(width);
                                    self.html("Отклонено");
                                });
                            ']), ['users/profile', 'id' => $model->client_id], ['class' => 'list-item no-after', 'style' => 'width: 100%;',])?>
        </li>
    <?php endforeach; ?>
</ul>
