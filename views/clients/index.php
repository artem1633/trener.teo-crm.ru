<?php

use yii\helpers\Html;

/**
 * @var $this \yii\web\View
 */


$this->title = 'Клиенты';


?>

<ul class="items-list">
    <?php foreach ($dataProvider->models as $model): ?>

        <?php
        $title = '<img class="profile-image image-bordered" src="/'.$model->photo.'" style="float: left;"><div style="margin-left: 90px;"><span>'.$model->fio.'</span>
            <div class="raiting"><i class="fa fa-star text-gold"></i><i class="fa fa-star text-gold"></i><i class="fa fa-star text-gold"></i><i class="fa fa-star text-gold"></i><i class="fa fa-star text-gold"></i></div>
            <div>Ежемесячный тренинг</div>
            <div>'.$model->cost_month.' руб.</div>
            </div>';
        ?>

        <li>
            <?= Html::a($title, ['users/profile', 'id' => $model->id], ['class' => 'list-item', 'style' => 'width: 100%;', 'data-pjax' => 0]) ?>
        </li>
    <?php endforeach; ?>
</ul>
