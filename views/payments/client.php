<?php

use yii\helpers\Url;
use yii\helpers\Html;

/** @var $model \app\models\Users */

?>

<div class="items-list" style="margin: 0 3%;">
    <li style="margin-bottom: 30px; border-bottom: 0;">
        <a href="<?=Url::toRoute(['users/profile', 'id' => $model->id])?>" class="list-item no-after" style="float: none; width: 100%;" data-pjax="0">
            <img class="profile-image image-bordered" src="/<?=$model->photo?>" style="float: left;"><div style="margin-left: 90px; margin-top: 15px;"><span style="font-size: 17px;"><?=$model->fio?></span></div>
        </a>
    </li>
</div>

<div class="items-list">
    <?php foreach($model->payments as $payment): ?>
        <li style="position: relative;">
            <a href="" class="list-item no-after"><span style="font-family: Ubuntu; font-weight: 200; color: #a0a0a0;">Занятие</span><br><span><?=$payment->quantity?> руб.</span><br>
                <span style="color: #a0a0a0;"><?=date('d.m.Y', strtotime($payment->payments_date))?></span></a>
            <div class="pull-right" style="position: absolute; right: 5%; top: 18%; text-align: right;">
                <?php if($payment->isPayed): ?>
                    <span style="color: #0084ea; font-size: 20px; position: absolute; right: 0px; top: 13px;"><i class="fa fa-check"></i></span><br>
                <?php else: ?>
                    <span style="color: #ea0036; font-size: 20px; position: absolute; right: 0px; top: 13px;"><i class="fa fa-times"></i></span><br>
                <?php endif; ?>
            </div>
        </li>
    <?php endforeach; ?>
</div>
