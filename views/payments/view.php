<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Payments */
?>
<div class="payments-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'client_id',
            'trainer_id',
            'quantity',
            'payments_date',
            'period',
        ],
    ]) ?>

</div>
