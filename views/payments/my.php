<?php

/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \app\models\PaymentsSearch $searchModel */

?>

<div class="items-list">
    <?php foreach ($dataProvider->models as $model): ?>
        <li>
            <a href="#" class="list-item no-after" style="width: 100%; position: relative;" data-pjax="0">
                <div style="margin-left: 10px;">
                    <span style="font-size: 16px;"><?=$model->getPeriodsLabels()[$model->period]?></span>
                    <br>
                    <span style="font-size: 14px;"><?=$model->quantity?> руб</span>
                    <br>
                    <span style="font-size: 13px;">Тренер: <?=$model->trainer->fio?></span>
                    <br>
                    <span style="font-size: 13px; color: #a0a0a0"><?=date('d.m.Y', strtotime($model->payments_date))?></span>
                </div>
                <div style="position: absolute; top: 44px; right: 28px; font-size: 20px;">
                    <?php if($model->isPayed): ?>
                        <i class="fa fa-check" style="color: #0084ea;"></i>
                    <?php else: ?>
                        <i class="fa fa-times" style="color: #ea0036;"></i>
                    <?php endif; ?>
                </div>
            </a>
        </li>
    <?php endforeach; ?>
</div>
