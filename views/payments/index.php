<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PaymentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $clientsDataProvider yii\data\ActiveDataProvider */

$this->title = '';

CrudAsset::register($this);

if(Yii::$app->user->identity->permission == \app\models\Users::USER_ROLE_TRENER){
    $this->params['header-action-button'] = Html::a('<i class="fa fa-plus"></i>', ['create']);
}

?>
<div class="payments-index" style="margin: 0 3%;">
        <?php
//
//        echo GridView::widget([
//            'id'=>'crud-datatable',
//            'dataProvider' => $dataProvider,
//            'filterModel' => $searchModel,
//            'pjax'=>true,
//            'columns' => require(__DIR__.'/_columns.php'),
//            'toolbar'=> [
//                ['content'=>
//                    Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],
//                    ['role'=>'modal-remote','title'=> 'Новая оплата','class'=>'btn btn-default']).
//                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
//                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Сбросить сетку']).
//                    '{toggleData}'.
//                    '{export}'
//                ],
//            ],
//            'striped' => true,
//            'condensed' => true,
//            'responsive' => true,
//            'panel' => [
//                'type' => 'primary',
//                'heading' => '<i class="glyphicon glyphicon-list"></i> Список оплат',
////                'before'=>'<em>* Resize table columns just like a spreadsheet by dragging the column edges.</em>',
//                'after'=>BulkButtonWidget::widget([
//                            'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить все',
//                                ["bulk-delete"] ,
//                                [
//                                    "class"=>"btn btn-danger btn-xs",
//                                    'role'=>'modal-remote-bulk',
//                                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//                                    'data-request-method'=>'post',
//                                    'data-confirm-title'=>'Вы уверены?',
//                                    'data-confirm-message'=>'Вы уверены что хотите удалить все эти элементы?'
//                                ]),
//                        ]).
//                        '<div class="clearfix"></div>',
//            ]
//        ])
        ?>

    <table class="table table_profile">
        <tbody>
        <tr>
            <th>
                <p><i class="fa fa-calendar"></i> Сумма платежей за последние 30 дней</p>
            </th>
        </tr>
        <tr>
            <td class="th-value">
                <p><?=number_format($monthPayments, 0, '.', ' ')?> <?=\php_rutils\RUtils::numeral()->choosePlural($monthPayments, ['рубль', 'рубля', 'рублей'])?></p>
            </td>
        </tr>
        <tr>
            <th>
                <p><i class="fa fa-calendar"></i> Сумма платежей за последние 7 дней</p>
            </th>
        </tr>
        <tr>
            <td class="th-value">
                <p><?=number_format($weekPayments, 0, '.', ' ')?> <?=\php_rutils\RUtils::numeral()->choosePlural($weekPayments, ['рубль', 'рубля', 'рублей'])?></p>
            </td>
        </tr>
        </tbody>
    </table>

    <h3>Клиенты</h3>
    <div class="items-list">
        <?php foreach ($clientsDataProvider->models as $model): ?>
            <li>
                <a href="<?=Url::toRoute(['client', 'id' => $model->id])?>" class="list-item" style="width: 100%;" data-pjax="0">
                    <img class="profile-image image-bordered" src="/<?=$model->photo?>" style="float: left;"><div style="margin-left: 90px; margin-top: 15px;"><span style="font-size: 17px;"><?=$model->fio?></span></div>
                </a>
            </li>
        <?php endforeach; ?>
    </div>
</div>


<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
