<?php

use yii\widgets\DetailView;
use kartik\rating\StarRating;

/* @var $this yii\web\View */
/* @var $model app\models\Feedback */
?>
<div class="feedback-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'client_id',
            'trainer_id',
            [
               'attribute' => 'rating',
                'value' => function($data) {

                     return StarRating::widget(['model' => $data, 'attribute' => 'rating',
                        'pluginOptions' => [
                            'theme' => 'krajee-uni',
                            'filledStar' => '&#x2605;',
                            'emptyStar' => '&#x2606;'

                        ],
                         'disabled' => true
                    ]);
                },
                'format' => 'raw'
            ],
            'client_img',
            'message:ntext',
            'created_date',
            'created_by',
        ],
    ]) ?>

</div>
