<?php
use yii\helpers\Url;
use kartik\rating\StarRating;
use app\models\Functions;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'client_id',
        'filter' => Functions::getClientsList(),
        'value' => function($data){
            return Functions::getUser($data -> client_id)->fio;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'trainer_id',
        'filter' => Functions::getTrainersList(),
        'value' => function($data){
            return Functions::getUser($data -> trainer_id)->fio;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'rating',
        'value' => function($data) {

            return StarRating::widget(['model' => $data, 'attribute' => 'rating',
                'pluginOptions' => [
                    'theme' => 'krajee-uni',
                    'filledStar' => '&#x2605;',
                    'emptyStar' => '&#x2606;'

                ],
                'disabled' => true
            ]);
        },
        'format' => 'raw'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'client_img',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'message',
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'created_date',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'created_by',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'Просмотр','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Редактировать', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Вы уверены?',
                          'data-confirm-message'=>'Вы уверены что хотите удалить все эти элементы?'],
    ],

];   