<?php
use yii\helpers\Url;
use app\models\Users;
use yii\helpers\ArrayHelper;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'exercise_id',
        'content' => function($data){
            return $data->exercise->name;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'weight',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'repitition',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'approach',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'trener_id',
        'filter' => ArrayHelper::map(Users::find()->where(['permission' => Users::USER_ROLE_TRENER])->all(),'id','fio'),
        'content' => function($data){
            return $data->trener->fio;
        }
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Are you sure?',
                          'data-confirm-message'=>'Are you sure want to delete this item'], 
    ],

];   