<?php

use yii\helpers\Html;

/** @var \app\models\CoachingUsers[] $coachingUsers */

?>

<ul class="items-list">
        <?php foreach ($coachingUsers as $coachingUser): ?>
            <li>
                <?=Html::a($coachingUser->coaching->name."<br>Клиент: {$coachingUser->users->fio}", ['training/view', 'id' => $coachingUser->coaching->id], ['class' => 'list-item', 'style' => 'width: 100%;'])?>
            </li>
        <?php endforeach; ?>
</ul>
