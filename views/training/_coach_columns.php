<?php
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\grid\GridView;
return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'kartik\grid\ExpandRowColumn', 
        'width'=>'50px',
        'value'=>function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
        },
        'detail'=>function ($model, $key, $index, $column) {
            return \Yii::$app->controller->renderPartial('_expand-training', ['model'=>$model]);
        },
        'headerOptions'=>['class'=>'kartik-sheet-style'],
        'expandOneOnly'=>true
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'description',
        'format'=>'html', 
    ], 
    [
        'attribute'=>'id',
        'header' => 'Добавить',
        'class'=>'\kartik\grid\DataColumn',
        'content' => function ($data) 
        {
            return '&nbsp;&nbsp;&nbsp;&nbsp;<a style="font-size:10px" class="btn btn-primary" role="modal-remote" href="'.Url::toRoute(['/training/add', 'trener_id' => $data->trener_id, 'coaching_id' => $data->id, ]).'"><i class="glyphicon glyphicon-plus"></i></a>';
        },
    ],
    [
        'class'    => 'kartik\grid\ActionColumn',
        'template' => '{leadUpdate} {leadDelete}',
        'buttons'  => [
            'leadUpdate' => function ($url, $model) {
                $url = Url::to(['/training/update-coach', 'id' => $model->id]);
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip']);
            },
            'leadDelete' => function ($url, $model) {
                $url = Url::to(['/training/coach-remove', 'id' => $model->id]);
                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                    'role'=>'modal-remote','title'=>'', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить этот элемент?'
                ]);
            },
        ]
    ]

];   