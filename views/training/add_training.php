<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Exercise;

/** @var $this \yii\web\View */
/** @var $model \app\models\Training */
/** @var $coaching \app\models\Coaching */

$this->title = 'Добавить упражнение';

$this->params['header-action-button'] = Html::a('<i class="fa fa-check"></i>', '#', ['onclick' => 'event.preventDefault(); $("#main-form").submit();']);

$exercisesPks = $coaching->getUsingExercisesPks();

if($model->isNewRecord == false) {
    $exercisesPks = array_filter($exercisesPks, function($el) use($model){
        return $el != $model->exercise_id;
    });
}

$exercises = ArrayHelper::map(Exercise::find()->where(['not in', 'id', $exercisesPks])->with('exerciseGroup')->all(), 'id', 'name', 'exerciseGroup.name');

$withMap = [];

foreach ($trainingUnions as $key => $value){
    $newValues = [];
    if(is_array($value)){
        foreach ($value as $row){
            $exercise = Exercise::findOne($row);
            if($exercise) {
                $newValues[] = $exercise->name;
            }
        }
        $withMap[$key] = implode(',', $newValues);
    } else {
        $exercise = Exercise::findOne($value);
        if($exercise) {
            $withMap[$key] = $exercise->name;
        }
    }
}

?>

<?php $form = ActiveForm::begin(['id' => 'main-form']); ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'exercise_id')->dropDownList($exercises, ['prompt' => 'Выберите упражнение']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'with')->dropDownList($withMap, ['prompt' => 'Выберите упражнения']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'weight')->textInput(['type' => 'number']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'repitition')->textInput(['type' => 'number']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'approach')->textInput(['type' => 'number']) ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>