<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\Training */
/* @var $form yii\widgets\ActiveForm */

$this->params['header-action-button'] = Html::a('<i class="fa fa-check"></i>', '#', ['onclick' => 'event.preventDefault(); $("#main-form").submit();']);

?>

<div class="training-form">

    <?php $form = ActiveForm::begin(['id' => 'main-form']); ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'description')->widget(CKEditor::className(),[
                'editorOptions' => [
                    'preset' => 'basic', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                    'inline' => false, //по умолчанию false
                    'height' => '150px',
                ],
            ]);
            ?>
        </div>
    </div>
  
    <?php ActiveForm::end(); ?>
    
</div>
