<?php
/**
 * Created by PhpStorm.
 * User: Ilusha
 * Date: 06.05.2017
 * Time: 13:38
 */

use kartik\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;

?>

<?=GridView::widget([
    'id'=>'crud-datatable',
    'responsiveWrap' => false,
    'dataProvider' => $model->orderTrainingData,
    'columns' => [
        [
            'class' => 'kartik\grid\SerialColumn',
            'width' => '30px',
        ],
        [
            'class'=>'\kartik\grid\DataColumn', 
            'attribute'=>'exercise_id',
            'content' => function ($data) {
                return $data->exercise->name;
            },
        ],
        'weight',
        'repitition',
        'approach',
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'trener_id',
            'content' => function($data){
                return $data->trener->fio;
            }
        ],
        [
            'class'    => 'kartik\grid\ActionColumn',
            'template' => '{leadDelete}',
            'buttons'  => [
                
                'leadUpdate' => function ($url, $model) {
                    $url = Url::to(['/goods/update-goods', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip']);
                },
                'leadDelete' => function ($url, $model) {
                    $url = Url::to(['/training/remove', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                        'role'=>'modal-remote','title'=>'', 
                              'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                              'data-request-method'=>'post',
                              'data-toggle'=>'tooltip',
                              'data-confirm-title'=>'Подтвердите действие',
                              'data-confirm-message'=>'Вы уверены что хотите удалить этот элемент?'
                    ]);
                },
            ]
        ]
    ],
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
])?>
