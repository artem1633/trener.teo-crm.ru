<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;
use app\models\CoachingCompleting;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AtelierSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Тренировка';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

if(Yii::$app->user->identity->permission == \app\models\Users::USER_ROLE_TRENER) {
    $this->params['header-action-button'] = Html::a('<i class="fa fa-plus"></i>', ['add-coach']);
}

?>

<?php \yii\widgets\Pjax::begin(['id' => 'crud-list-pjax', 'enablePushState' => false]) ?>
<?php \app\assets\plugins\TouchSwipeAsset::register($this); ?>
<ul class="items-list">
    <?php foreach ($dataProvider->models as $model): ?>
        <li>
            <?php   $trener = \app\models\Users::find()->where(['id' => $model->trener_id])->one();
                    if($trener != null) {
                        $trenerName = $trener->fio;
                    } else {
                        $trenerName = null;
                    }
                    $completedCount = CoachingCompleting::find()->where(['coaching_id' => $model->id, 'client_id' => Yii::$app->user->getId()])->count();
            ?>
            <?= Html::a($model->name.'<br><span style="font-size: 12px;"></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <span style="font-size: 12px;">Выполнена '.$completedCount.' '.\php_rutils\RUtils::numeral()->choosePlural($completedCount, ['раз', 'раза', 'раз']).'</span><br>
                    <span style="font-size: 12px;">Тренер: '.$trenerName.'</span>',
                ['view', 'id' => $model->id], ['class' => 'list-item', 'style' => 'width: 100%;', 'data-pjax' => 0]) ?>
            <?php if(Yii::$app->user->identity->permission != \app\models\Users::USER_ROLE_CLIENT): ?>
                <a class="item-action item-success" href="<?=Url::toRoute(['update-coach', 'id' => $model->id])?>" data-pjax="0"><i class="fa fa-pencil"></i></a>
                <?= Html::a('<i class="fa fa-trash"></i>', ['delete-coach', 'id' => $model->id], ['class' => "item-action item-danger",
                    'role'=>'modal-remote','title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-toggle'=>'tooltip',
                    'data-confirm-title'=>'Подтвердите действие',
                    'data-confirm-message'=>'Вы уверены что хотите удалить этот элемент?'
                ]) ?>
            <?php endif; ?>
        </li>
    <?php endforeach; ?>
</ul>
<?php

if(Yii::$app->user->identity->permission != \app\models\Users::USER_ROLE_CLIENT)
{
    $this->registerJs(file_get_contents('js/scripts/list-swipe.js'), \yii\web\View::POS_READY);
}


?>
<?php \yii\widgets\Pjax::end()?>


<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>