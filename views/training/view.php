<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Coaching */
/* @var $trainnings app\models\Training[] */
/* @var $exercises array */
/* @var $exercisesTrainnings array */
/* @var $exercisesGroups array */

if(Yii::$app->user->id == $model->trener_id) {
    $this->params['header-action-button'] = Html::a('<i class="fa fa-plus"></i>', ['add-new-training', 'coaching_id' => $model->id]);
}

$groups = array_unique(array_values($exercisesGroups));

$groupsColors = [];

foreach ($groups as $id)
{
//    $r = rand(0, 255);
//    $g = rand(0, 255);
//    $b = rand(0, 255);
//    $groupsColors[$id] = " rgba({$r}, {$g}, {$b}, .3)";
}

$groupedTrainings = array_values($trainingUnions);

foreach ($groupedTrainings as $trainings){
    $r = rand(0, 255);
    $g = rand(0, 255);
    $b = rand(0, 255);
    $groupsColors[" rgba({$r}, {$g}, {$b}, .3)"] = $trainings;
}

?>
<div class="training-view">

    <div class="row">
        <div class="col-md-12">
            <?=Html::a('Редактировать', ['update-coach', 'id' => $model->id], ['class' => 'btn btn-theme pull-right', 'style' => 'border-radius: 1em;'])?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h3 style="margin-bottom: 30px; padding: 0 3%;"><?=$model->name?></h3>
        </div>
    </div>

    <table style="width: 100%; margin-bottom: 40px; background: #fff;" class="table">
        <thead>
        <tr>
            <th style="text-align: center;">Наименование</th>
            <th style="text-align: center;">Вес</th>
            <th style="text-align: center;">Повторы</th>
            <th style="text-align: center;">Подходы</th>
        </tr>
        </thead>
        <tbody class="items-list">
        <?php foreach($exercises as $id => $name): ?>

            <?php
                $trainingId = $exercisesTrainnings[$id]->id;
                $style = '';
                foreach ($groupsColors as $style => $trainings){
                    if(in_array($trainingId, $trainings) && count($trainings) > 1){
                        $style = "background-color:{$style}";
                        break;
                    }
                }
            ?>


            <tr style="border-bottom: 1px solid #f4f4f4; <?=$style?>">
                <td style="text-align: center;"><?=Html::a($name, ['exercise/view', 'id' => $id])?></td>
                <td style="text-align: center;"><?=$exercisesTrainnings[$id]->weight?></td>
                <td style="text-align: center;"><?=$exercisesTrainnings[$id]->repitition?></td>
                <td style="text-align: center;"><?=$exercisesTrainnings[$id]->approach?></td>
                <?php if(Yii::$app->user->identity->permission != \app\models\Users::USER_ROLE_CLIENT): ?>
                    <td class="action">
                        <div class="list-wrapper">
                            <a class="item-action item-success" href="<?=Url::toRoute(['update-training', 'id' => $exercisesTrainnings[$id]->id])?>"><i class="fa fa-pencil"></i></a>
                            <?= Html::a('<i class="fa fa-trash"></i>', ['delete-training', 'id' => $exercisesTrainnings[$id]->id], ['class' => "item-action item-danger", 'title'=>'Удалить']) ?>
                        </div>
                    </td>
                <?php endif; ?>
            </tr>

        <?php endforeach; ?>
        </tbody>
    </table>

    <?php if(Yii::$app->user->identity->permission == \app\models\Users::USER_ROLE_CLIENT): ?>
        <?php if(\app\models\CoachingCompleting::hasIsCompleted($model->id, Yii::$app->user->getId()) == false): ?>
            <a href="#" class="btn btn-theme btn-lg btn-block transition-25" onclick="Controlls.sendBinaryRequest($(this), '<?=Url::toRoute(['make-coaching-completed'])?>', {'coaching_id': <?=$model->id?>}, '<i class=\'fa fa-check\'></i> Выполнена')">Отметить как выполненую</a>
        <?php else: ?>
            <a href="#" class="btn btn-default btn-lg btn-block transition-25"><i class="fa fa-check"></i> Выполнена</a>
        <?php endif; ?>
    <?php endif; ?>

</div>

<?php

if(Yii::$app->user->identity->permission != \app\models\Users::USER_ROLE_CLIENT)
{
    $this->registerJs(file_get_contents('js/scripts/list-swipe-table.js'), \yii\web\View::POS_READY);
}

?>