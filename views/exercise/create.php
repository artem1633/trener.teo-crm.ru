<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Exercise */

?>
<div class="exercise-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
