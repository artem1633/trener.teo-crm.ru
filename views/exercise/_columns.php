<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\ExerciseGroup;
use app\models\Users;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'video',
    ], 
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'photo',
        'content' => function ($data) {
            return ($data->photo) ? Html::img('/uploads/'.$data->photo,['style'=>'width:150px; height:100px;text-align:center']) : null;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'description',
        'format'=>'html', 
    ], 
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'exercise_group_id',
        'filter' => ArrayHelper::map(ExerciseGroup::find()->all(),'id','name'),
        'content' => function($data){
            return $data->exerciseGroup->name;
        }
    ],    
    [
        'class'=>'\kartik\grid\DataColumn',
        'filter' => ArrayHelper::map(Users::find()->where(['permission' => Users::USER_ROLE_TRENER])->all(),'id','fio'),
        'attribute'=>'trener_id',
        'content' => function($data){
            return $data->trener->fio;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'check',
        'content' => function ($data) {
            if($data->check == 1) return 'Да';
            else return 'Нет';
        },
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'template' => '{update} {delete}',
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить этот элемент?'], 
    ],

];   