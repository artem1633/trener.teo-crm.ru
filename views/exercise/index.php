<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AtelierSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список упражнений';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

if(Yii::$app->user->identity->permission == 'trener'){
    $this->params['header-action-button'] = Html::a('<i class="fa fa-plus"></i>', ['create']);
}


?>
<div class="atelier-index">
    <div id="ajaxCrudDatatable">
        <?php //echo GridView::widget([
//            'id'=>'crud-datatable',
//            'dataProvider' => $dataProvider,
//            'filterModel' => $searchModel,
//            'pjax'=>true,
//            'responsiveWrap' => false,
//            'columns' => require(__DIR__.'/_columns.php'),
//            'toolbar'=> [
//                ['content'=>
//                    Html::a('Создать', ['create'],
//                        ['role'=>'modal-remote','title'=> 'Создать','class'=>'btn btn-primary']).
//                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
//                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Обновить']).
//                    '{toggleData}'
//                ],
//            ],
//            'striped' => true,
//            'condensed' => true,
//            'responsive' => true,
//            'panel' => [
//                'type' => 'primary',
//                'heading' => '<i class="glyphicon glyphicon-list"></i> Список',
//                'before'=>'',
//                'after'=>'',
//            ]
//        ])

        $models = $dataProvider->models;
        ?>

        <?php \yii\widgets\Pjax::begin(['id' => 'crud-list-pjax', 'enablePushState' => false]) ?>
        <?php \app\assets\plugins\TouchSwipeAsset::register($this); ?>
        <ul class="items-list">
            <?php foreach ($models as $model): ?>
                <li>
                    <?= Html::a($model->name, ['view', 'id' => $model->id], ['class' => 'list-item', 'style' => 'width: 100%;', 'data-pjax' => 0]) ?>
                    <?php if(Yii::$app->user->identity->permission != \app\models\Users::USER_ROLE_CLIENT): ?>
                        <a class="item-action item-success" href="<?=Url::toRoute(['update', 'id' => $model->id])?>" data-pjax="0"><i class="fa fa-pencil"></i></a>
                        <?= Html::a('<i class="fa fa-trash"></i>', ['delete', 'id' => $model->id], ['class' => "item-action item-danger",
                            'role'=>'modal-remote','title'=>'Delete',
                            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                            'data-request-method'=>'post',
                            'data-toggle'=>'tooltip',
                            'data-confirm-title'=>'Подтвердите действие',
                            'data-confirm-message'=>'Вы уверены что хотите удалить этот элемент?'
                        ]) ?>
                    <?php endif; ?>
                </li>
            <?php endforeach; ?>
        </ul>
        <?php


        if(Yii::$app->user->identity->permission != \app\models\Users::USER_ROLE_CLIENT)
        {
            $this->registerJs(file_get_contents('js/scripts/list-swipe.js'), \yii\web\View::POS_READY);
        }


        ?>
        <?php \yii\widgets\Pjax::end() ?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
