<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Exercise */

if(Yii::$app->user->getId() == $model->trener_id){
    $this->params['header-action-button'] = Html::a('<i class="fa fa-pencil"></i>', ['update', 'id' => $model->id]);
}

?>
<div class="exercise-view">

    <?php if($model->photo == null): ?>
        <a href="#" data-upload="image" style="text-align: center; display: block; width: 100%; margin: 50px 0; font-size: 18px; color: #a5a5a5;">Добавить фото</a>
    <?php else: ?>
        <img data-upload="image" src="/<?=$model->photo?>">
    <?php endif; ?>

    <h3 style="margin: 20px 5px;">
        <?=$model->name?>
        <?php if($model->video != null): ?>
            <?= Html::a('<i class="fa fa-youtube-play text-danger" style="font-size: 35px;"></i>', $model->video); ?>
        <?php endif; ?>
    </h3>

    <p style="margin-left: 10px; color: #b2b2b2; font-family: Ubuntu;"><i class="fa fa-file-text" style="color: #0087e0;"></i> <?= $model->getAttributeLabel('description') ?></p>

    <table class="table table_profile">
        <tr>
            <td class="">
                <p style="padding-left: 0; border-bottom: 0;"><?= $model->description ?></p>
            </td>
        </tr>
    </table>

    <?php if(stripos($model->video, 'http') != false || stripos($model->video, 'https') != false): ?>
        <iframe width="100%" height="315" src="<?=$model->video?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    <?php endif; ?>

</div>

<?php

$uploadAction = \yii\helpers\Url::toRoute(['upload-photo', 'id' => $model->id], true);

$script = <<< JS

    $('[data-upload="image"]').click(function(e){
        e.preventDefault();
        
        android.selectFile('{$uploadAction}', 0, yii.getCsrfToken());
    });

JS;


$this->registerJs($script, \yii\web\View::POS_READY);

?>