<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Exercise */
?>
<div class="exercise-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
