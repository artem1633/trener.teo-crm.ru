<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\Exercise */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="exercise-form">

    <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'video')->textInput() ?>

            <?= $form->field($model, 'exercise_group_id')->dropDownList($model->getGroupList(), ['prompt' => 'Выберите группу' ]) ?>
                <?= $form->field($model, 'check')->checkBox(); ?>

            <?= $form->field($model, 'description')->widget(CKEditor::className(),[
                'editorOptions' => [
                    'preset' => 'basic', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                    'inline' => false, //по умолчанию false
                    'height' => '200px',
                ],
            ]);
            ?>

	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<?php 
$this->registerJs(<<<JS

$(document).ready(function(){
    var fileCollection = new Array();

    $(document).on('change', '.btn_image', function(e){
        var files = e.target.files;
        var button_id = $(this).attr("id");
        //alert(button_id);
        $.each(files, function(i, file){
            fileCollection.push(file);
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function(e){
                var template = '<img style="width:100%;" src="'+e.target.result+'"> ';
                $('#images-to-upload').html('');
                //document.getElementById('theID').value = 'new value';
                $('#images-to-upload').append(template);
            };
        });
    });

});
JS
);
?>