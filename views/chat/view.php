<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Chat */
?>
<div class="chat-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'client_id',
            'trainer_id',
            'chat_key',
            'message:ntext',
            'created_date',
            'created_by',
        ],
    ]) ?>

</div>
