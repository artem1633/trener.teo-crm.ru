<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\models\Users;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use app\models\Functions;
use app\models\Chat;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;

$this->title = '';
CrudAsset::register($this);

?>
<?php Pjax::begin(['enablePushState' => false, 'id' => 'profile-pjax']) ?>

<div class="row">
    <div class="scrollmenu">

    </div>
    <div class="col-xs-12">
        <div class="">
            <h3 class=" text-center">Чат</h3>
            <div class="messaging">
                <div class="inbox_msg">
                    <div class="mesgs">
                        <div class="msg_history">

                        </div>
                        <div class="type_msg">
                            <div class="input_msg_write">
                                <?php $chat_model = new Chat(); ?>

                                <?php $form = ActiveForm::begin([
                                    'action' => ['/chat/create']
                                ]); ?>
                                <?php
                                if (Functions::getUserRole(Yii::$app->user->identity->id) == "Клиент") { ?>

                                    <input type="hidden" name="client_id" value="<?= Yii::$app->user->identity->id?>">
                                    <?php
                                    echo $form->field($chat_model, 'trainer_id')->label(false)->widget(\kartik\select2\Select2::classname(), [
                                        'data' => Functions::getTrainersList(),
                                        'options' => ['placeholder' => 'Тренер'],
                                        'pluginOptions' => [
                                            'tags' => true,
                                            'allowClear' => true,
                                        ],
                                    ]);
                                } else if (Functions::getUserRole(Yii::$app->user->identity->id) == "Тренер") { ?>

                                    <input type="hidden" name="trainer_id" value="<?= Yii::$app->user->identity->id?>">
                                    <?php
                                    echo $form->field($chat_model, 'client_id')->label(false)->widget(\kartik\select2\Select2::classname(), [
                                        'data' => Functions::getClientsList(),
                                        'options' => ['placeholder' => 'Клиент'],
                                        'pluginOptions' => [
                                            'tags' => true,
                                            'allowClear' => true,
                                        ],
                                    ]);
                                }
                                ?>
                                <div class="scrolls">
                                    <?= $form->field($chat_model, 'message')->widget(CKEditor::className(), [
                                        'editorOptions' => [
                                            ElFinder::ckeditorOptions('elfinder', [
                                                'preset' => 'basic',
                                            ]),
                                            'preset' => 'basic',
                                        ]
                                    ]); ?>
                                </div>
                                <?php if (!Yii::$app->request->isAjax){ ?>
                                    <div class="form-group">
                                        <?php
                                        echo Html::submitButton('Написать' ,['class' => 'btn btn-success send_message']);
                                        ?>
                                    </div>
                                <?php } ?>

                                <?php ActiveForm::end(); ?>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <div id="ajaxCrudDatatable">
            </div>
        </div>
    </div>
</div>

<?php Pjax::end() ?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
    "size" => "modal-lg",
    "options" => [
        "open.bs.modal" => "function(){ console.log('123'); }",
        "tabindex" => false,
    ],
])?>
<?php Modal::end(); ?>

<?php
$script = <<< JS
$(document).ready(function() {
  
    console.log('ready!');
    //style="visibility: hidden; display: none;"
    $('.cke_dialog_body').addClass('scrollmenu');
    var chat_message = $('#chat-message');
     // console.log('chat_message');
     // console.log(chat_message);
    if ($(chat_message).css('visibility') == 'hidden') {
        // console.log('hidden!');
    } else {
        // $(chat_message).css({'visibility': 'hidden'});
        // $(chat_message).css({'display': 'none'});
        var editor = 
        '<div ' +
           'id="cke_chat-message" ' +
           'class="cke_1 cke cke_reset cke_chrome cke_editor_chat-message cke_ltr cke_browser_webkit" ' +
           'dir="ltr" lang="ru" ' +
           'role="application" ' +
           'aria-labelledby="cke_chat-message_arialbl"' +
        '>' +
       
           '<span ' +
               'id="cke_chat-message_arialbl" ' +
               'class="cke_voice_label">' +
               'Визуальный текстовый редактор, chat-message' +
           '</span>' +
           
           '<div ' +
              'class="cke_inner cke_reset" ' +
              'role="presentation"' +
           '>' +
              
             '<span ' +
                'id="cke_1_top" ' +
                'class="cke_top cke_reset_all" role="presentation" ' +
                'style="height: auto; ' +
                'user-select: none;"' +
             '>' +
                
                '<span ' +
                   'id="cke_8" ' +
                   'class="cke_voice_label"' +
                '>' +
                  'Панели инструментов редактора' +
                '</span>' +
                
               '<span ' +
                  'id="cke_1_toolbox" ' +
                  'class="cke_toolbox" ' +
                  'role="group" ' +
                  'aria-labelledby="cke_8" ' +
                  'onmousedown="return false;"' +
               '>' +
               
                 '<span ' +
                     'id="cke_9" ' +
                     'class="cke_toolbar" ' +
                     'aria-labelledby="cke_9_label" ' +
                     'role="toolbar"' +
                  '>' +
                  
                      '<span ' +
                         'id="cke_9_label" ' +
                         'class="cke_voice_label"' +
                      '>' +
                         'Вставка' +
                      '</span>' +
                      
                        '<span class="cke_toolbar_start"></span>' +
                        '<span ' +
                            'class="cke_toolgroup" ' +
                            'role="presentation"' +
                         '>' +
                            
                            '<a ' +
                                'id="cke_10" ' +
                                'class="cke_button cke_button__image  cke_button_off" href="javascript:void(' + "Изображение" + ')" ' +
                                'title="Изображение" ' +
                                'tabindex="-1" ' +
                                'hidefocus="true" ' +
                                'role="button" ' +
                                'aria-labelledby="cke_10_label" ' +
                                'aria-haspopup="false" ' +
                                'onkeydown="return CKEDITOR.tools.callFunction(1,event);" onfocus="return CKEDITOR.tools.callFunction(2,event);" onclick="CKEDITOR.tools.callFunction(3,this);return false;">' +
                                
                                 '<span class="cke_button_icon cke_button__image_icon" style="background-image:url(http://trainer.crm/avatars/icons.png?t=E8PB);background-position:0 -936px;background-size:auto;">' +
                                    '&nbsp;' +
                                 '</span>' +
                                 '<span ' +
                                    'id="cke_10_label" ' +
                                    'class="cke_button_label cke_button__image_label" ' +
                                    'aria-hidden="false"' +
                                 '>' +
                                    'Изображение' +
                                '</span>' +
                            '</a>' +
                       '</span>' +
                       
                       '<span class="cke_toolbar_end"></span>' +
                     '</span>' +
                  '</span>' +
               '</span>' +
               
               '<div ' +
                  'id="cke_1_contents" ' +
                  'class="cke_contents cke_reset" ' +
                  'role="presentation" ' +
                  'style="height: 100px;"' +
               '>' +
                   '<span id="cke_14" class="cke_voice_label">' +
                           'Нажмите ALT-0 для открытия справки' +
                    '</span>' +
                    '<iframe ' +
                          'src="" ' +
                          'frameborder="0" ' +
                          'class="cke_wysiwyg_frame cke_reset" ' +
                          'style="width: 100%; height: 100%;" ' +
                          'title="Визуальный текстовый редактор, chat-message" ' +
                          'aria-describedby="cke_14" ' +
                          'tabindex="0" ' +
                          'allowtransparency="true">' +
                    '</iframe>' +
             '</div>' +
           '</div>' +
        '</div>';
            
        // $(chat_message).parent().append(editor);
        // console.log('editor appended!');
    }
    //chat
    $(document).on('change','.field-chat-trainer_id select, .field-chat-client_id select', function(event) {
        
        var chat_form = $(this).closest('#w0');
        console.log('chat_form');
        console.log(chat_form);
        
        var selected_item = $(chat_form).find(":selected");
        console.log('selected_item');
        console.log(selected_item);
        
        var client_id = null;
        var trainer_id = null;
        
        
        var check_client = $('select#chat-client_id');
        var check_trainer = $('select#chat-trainer_id');
        
        if (check_client.length > 0) {
            
            client_id = $(check_client).val();
            console.log('client_id');
            console.log(client_id);
            
            trainer_id = $(chat_form).find('input[name="trainer_id"]').val();
            console.log('trainer_id');
            console.log(trainer_id);
            
        } else if (check_trainer.length > 0) {
            
            trainer_id = $(check_trainer).val();
            console.log('trainer_id');
            console.log(trainer_id);
            
            client_id = $(chat_form).find('input[name="client_id"]').val();
            console.log('client_id');
            console.log(client_id);
            
        }
        
        $.ajax({
                url: '/chat/chat-by-user',
                data: {
                    trainer_id: trainer_id,
                    client_id: client_id,
                },
                async: false,
                // type: 'json',
                type: 'get',
                success: function (chat) {
                    console.log('chat');
                    console.log(chat);
                    //
                    var chats = JSON.parse(chat);
                    console.log('chats');
                    console.log(chats);
                    for(key in chats){
                        var chat = chats[key];
                        var message = chat.message;
                        var whose_message = chat.whose_message;
                        var trainer_image = chat.trainer_image;
                        var created_date = chat.created_date;
                       
                        // console.log('message');
                        // console.log(message);
                        // console.log('whose_message');
                        // console.log(whose_message);
                        // console.log('trainer_image');
                        // console.log(trainer_image);
                        // console.log('created_date');
                        // console.log(created_date);
                       
                        if (whose_message === 'Тренер') {
                            // <img class="msg_img" src="/web/' + trainer_image + '" alt="sunil"> \
                            var element =
                            '<div class="incoming_msg"> \
                                <div class="incoming_msg_img"> \
                                    <img class="msg_img" src="' + trainer_image + '" alt="sunil"> \
                                </div> \
                                <div class="received_msg"> \
                                    <div class="received_withd_msg"> \
                                        <p> \
                                            ' +  message + ' \
                                        </p> \
                                        <span class="time_date">' +  created_date + '</span></div> \
                                </div> \
                            </div>';
                            $('.msg_history').append(element);
                         } else if (whose_message === 'Клиент') {
                            var element =
                            '<div class="outgoing_msg"> \
                                <div class="sent_msg"> \
                                    <p> \
                                        ' +  message + ' \
                                    </p> \
                                    <span class="time_date">' +  created_date + '</span></div> \
                            </div>';
                            $('.msg_history').append(element);
                         }
                    }
                }, 
                fail: function(data) {
                   console.log('error data');
                    console.log(data);
                }
        });
        
    });

});
JS;
$this->registerJs($script);
?>