<?php
use yii\helpers\Url;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'fio',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'telephone',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'information',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'occupation',
    ],[
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'training_experience',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'fitnes',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'training_goal',
    ],

];   