<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use app\models\Users;
use yii\helpers\Url;
use yii\web\JsExpression;
use kartik\file\FileInput;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */

$this->params['header-action-button'] = Html::a('<i class="fa fa-check"></i>', '#', ['onclick' => 'event.preventDefault(); $("#profile-update-form").submit();']);

?>

<div class="users-form">
    <?php $form = ActiveForm::begin([ 'options' => ['id' => 'profile-update-form', 'method' => 'post', 'enctype' => 'multipart/form-data']]); ?>

    <div class="row">
        <div style="display: none;" >
            <?php
            $education_scans_hidden = json_decode($model -> education_scans, true);
            if(!empty($education_scans_hidden)) {
                $education_scans_hidden = implode(", ", $education_scans_hidden);
            } else {
                $education_scans_hidden = "";
            }

            $model_photos_hidden = json_decode($model -> photos, true);
            if(!empty($model_photos_hidden)) {
                $model_photos_hidden = implode(", ", $model_photos_hidden);
            } else {
                $model_photos_hidden = "";
            }
            ?>
            <input id="education_scans_hidden" name="education_scans_hidden" type="hidden" value="<?=$education_scans_hidden?>">
            <input id="model_photos_hidden" name="model_photos_hidden" type="hidden" value="<?=$model_photos_hidden?>">
            <input id="photos_hidden" name="photos_hidden" type="hidden" value="<?=$model -> photos?>">
            <input id="model_id" name="model_id" type="hidden" value="<?=$model -> id?>">
            <input id="updateRecord" name="updateRecord" type="hidden" value="<?= $model -> isNewRecord ? 0 : 1?>">
        </div>
    </div>

    <div class="row" style="margin-bottom: 30px;">
        <div class="col-xs-4">
            <?php
//            echo $form->field($model, 'file')->widget(FileInput::classname(), [
//                'name' => 'file',
//                'pluginOptions' => [
//                    'browseClass' => 'btn btn-success',
//                    'showPreview' => false,
//                    'showCaption' => true,
//                    'showRemove' => true,
//                    'showUpload' => false,
//                    //                                                                'removeClass' => 'btn btn-danger',
//                    'removeIcon' => '<i class="glyphicon glyphicon-trash"></i> ',
//                    'uploadUrl' => Url::to(['/users/file-upload?id=' . $model->id]),
//                ],
//                'options' => [
//                    'multiple' => false,
//                ]
//            ])->label(false);


            ?>

            <?php if($model->id == Yii::$app->user->getId()){ ?>

                <div class="profile">
                    <img src="/<?=$model->photo?>" data-img-upload="upload" class="profile-image image-bordered">
                </div>

            <?php } else if(($model->permission == Users::USER_ROLE_CLIENT && $model->trener == Yii::$app->user->getId()) || Yii::$app->user->permission == 'administrator') { ?>
                <div class="profile">
                    <img src="/<?=$model->photo?>" data-form-img-upload="users-photo" class="profile-image image-bordered">
                </div>
            <?php } ?>

        </div>
        <div class="col-xs-8"><?= $form->field($model, 'fio')->textInput(['maxlength' => true, 'class' => 'input-flat', 'placeholder' => 'Фамилия и имя'])->label(false) ?></div>
    </div>

    <?php if($model->id != Yii::$app->user->getId()): ?>
        <div class="hidden">
            <?= $form->field($model, 'photo')->hiddenInput() ?>
        </div>
    <?php endif; ?>

    <?php
    if (!empty(Yii::$app->user->identity -> permission)) {
        if($role == 'administrator') { ?>
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <?= $form->field($model, 'permission')->dropDownList($model->getRoleList(), ['id' => 'type'])->label('Выберите должность'); ?>
                </div>
            </div>

        <?php   } else if(Yii::$app->user->identity -> permission == 'trener'){ ?>
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <?php
                    if ($model -> isNewRecord)
                        echo $form->field($model, 'permission')->textInput(['readonly' => true, 'value' => 'client']);
                    else
                        echo $form->field($model, 'permission')->dropDownList($model->getRoleList(), ['id' => 'type'])->label('Выберите должность');
                    ?>
                </div>
            </div>
        <?php }
    }

    ?>
            <?php

//                echo $form->field($model, 'birthday')->widget(DatePicker::classname(), [
//                    'options' => ['placeholder' => 'Выберите дату',],
//                    'removeButton' => false,
//                    'pluginOptions' => [
//                        'autoclose' => true,
//                        'startView'=>'year',
//                        //'minViewMode'=>'years',
//                        'format' => 'yyyy-mm-dd'
//                    ]
//                ]);

                echo $form->field($model, 'birthday')->input('date');
            ?>
    <div class="row">
        <div class="col-md-4 col-md-12">
            <?= $form->field($model, 'country')->dropDownList(\app\values\Country::getList(), ['prompt' => 'Выберите страну']) ?>
        </div>
        <div class="col-md-4 col-md-12">
            <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'gender')->dropDownList($model->getGenderList(), [/*'prompt' => 'Выберите пол'*/ ]) ?>
        </div>

    </div>
    <div id="trener" <?php if($model->permission == Users::USER_ROLE_CLIENT) echo 'style=" display: none;"';?> >
        <div class="row" style="margin-top: 20px;">
            <div class="col-md-12 col-xs-12">
                <div class="work-wrapper">
                    <?php if($model->permission != Users::USER_ROLE_CLIENT): ?>
                        <div class="work-select">
                            <p>Стаж работы тренером:</p>
                            <div class="col-xs-6">
                                <input id="trainer_year" type="number" min="1" max="40" class="form-control" name="trainer_year" placeholder="Количество лет">
                            </div>
                            <div class="col-xs-6">
                                <input id="trainer_month" type="number" min="1" max="11" required class="form-control" name="trainer_month" placeholder="Количество месяцев">
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>

            <div class="col-md-3 col-xs-12">
                <?= $form->field($model, 'education')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="row">
                <!--        //doc_scans Сканы документов об образовании-->
                <div class="col-xs-12">
                    <?php

                    if ($model -> isNewRecord) {
                        echo $form->field($model, 'doc_scans[]')->widget(FileInput::classname(),[
                            'name' => 'file',
                            'pluginOptions' => [
                                'browseClass' => 'btn btn-success',
                                'uploadClass' => '',
                                'showRemove' => true,
                                'showUpload' => true,
//                                                                'removeClass' => 'btn btn-danger',
                                'removeIcon' => '<i class="glyphicon glyphicon-trash"></i> ',
                            ],
                            'options' => [
                                'multiple' => true,
                            ]
                        ])->label("Сканы документов об образовании");

                    }
                    ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3 col-xs-12">
                <?= $form->field($model, 'props')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row portfolio_photo">

                       <div class="col-xs-12">
                               <?php

                               if ($model -> isNewRecord) {
                                       echo $form->field($model, 'files[]')->widget(FileInput::classname(),[
                                               'name' => 'file',
                                               'pluginOptions' => [
                                                       'browseClass' => 'btn btn-success',
                                                       'uploadClass' => '',
                                                       'showRemove' => true,
                                                       'showUpload' => true,
                                                                                            'removeClass' => 'btn btn-danger',
                                                       'removeIcon' => '<i class="glyphicon glyphicon-trash"></i>',
                                                   ],
                                               'options' => [
                                                           'multiple' => true,
                                                       ]
                                               ])->label("Фото портфолио");

               } else {

                                       if(!empty($model_photos_hidden)) {
                                               $photos = \app\models\Functions::additionalPhotosHtmlEdit($model -> photos);
                                               echo $form->field($model, 'files[]')->widget(FileInput::classname(), [
                                                       'name' => 'attachment_48[]',
                                                       'options'=>[
                                                               'multiple'=>true,
                                                               'accept' => 'image/*'
                                                               ],
                                                       'pluginOptions' => [
                                                               'initialPreviewShowDelete' => true,
                                                               'allowedFileExtensions' => ['jpg', 'jpeg', 'png'],
                                                               'overwriteInitial' => false,
                                                               'showPreview' => true,
                                                               'showCaption' => true,
                                                               'showRemove' => true,
                                                               'showUpload' => true,
                                                               'initialPreview' => $photos,
                                                               'initialPreviewConfig' => [
                                                                   ],
                                                           ],
                                                   ])->label("Фото портфолио");
                   }
                   else {
                                               echo $form->field($model, 'files[]')->widget(FileInput::classname(), [
                                                       'name' => 'attachment_48[]',
                                                       'options' => [
                                                               'multiple' => true,
                                                               'accept' => 'image/*'
                                                               ],
                                                       'pluginOptions' => [
                                                                   'showUpload' => false,
                                                                   'allowedFileExtensions' => ['jpg', 'jpeg', 'png'],
                                                                   'browseLabel' => '',
                                                                   'removeLabel' => '',
                                                                   'mainClass' => 'input-group-lg'
                                                                   ]
                                                       ])->label("Фото портфолио");
                   }
               }
               ?>
                           </div>
                   </div>


        <div class="row">
            <div class="col-md-4 col-xs-12">
                <?= $form->field($model, 'instagram')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4 col-xs-12">
                <?= $form->field($model, 'cost')->textInput(['type' => 'number']) ?>
            </div>
            <div class="col-md-4 col-xs-12">
                <?= $form->field($model, 'cost_month')->textInput(['type' => 'number']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4 col-xs-12">
                <?= $form->field($model, 'cost_program')->textInput(['type' => 'number']) ?>
            </div>
            <div class="col-md-4 col-xs-12">
                <?= $form->field($model, 'last_pay')->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => 'Выберите дату',],
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'startView'=>'year',
                        //'minViewMode'=>'years',
                        'format' => 'yyyy-mm-dd'
                    ]
                ]);
                ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3 col-xs-12">
            <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>
        </div>

            <div class="col-md-3 col-xs-12">
                <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4 col-xs-12">
                <?= $form->field($model, 'status')->dropDownList($model->getStatusList(), [/*'prompt' => 'Выберите статуса'*/ ]) ?>
            </div>
        <div class="col-md-3 col-xs-12">
            <?= $model->isNewRecord ? $form->field($model, 'password')->textInput(['maxlength' => true]) : $form->field($model, 'new_password')->textInput(['maxlength' => true]) ?>
        </div>

    </div>
    <div class="row">

        <div style="display: none;" >
            <?= $form->field($model, 'education_scans')->textarea(['rows' => 3]) ?>
        </div>

        <input id="number" name="number" type="hidden" value="<?=$number?>">
        <input id="path" name="path" type="hidden" value="<?= "images/products/".$number."/" ?>">
        <input id="url" name="url" type="hidden" value="<?= "http://" . $_SERVER['SERVER_NAME'] . '/' ?>">

    </div>

    <div id="client" <?php if($model->permission != Users::USER_ROLE_CLIENT) echo 'style=" display: none;"';?> >
        
        <div class="row">
            <?php if(Yii::$app->user->identity->permission == Users::USER_ROLE_ADMIN): ?>
                <div class="col-md-3">
                    <?= $form->field($model, 'trener')->dropDownList($model->getTrenerList(), [/*'prompt' => 'Выберите пол'*/ ]) ?>
                </div>
            <?php endif; ?>
            <div class="col-md-3">
                <?= $form->field($model, 'fitnes')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'training_goal')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4 col-xs-12">
                <?= $form->field($model, 'instagram')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="work-wrapper">
                    <?php if($model->permission == Users::USER_ROLE_CLIENT): ?>
                        <div class="work-select">
                            <p>Стаж тренировок:</p>
                            <div class="col-xs-6">
                                <input id="client_year" type="number" min="1" max="40" class="form-control" name="client_year" placeholder="Количество лет">
                            </div>
                            <div class="col-xs-6">
                                <input id="client_month" type="number" min="1" max="11" required class="form-control" name="client_month" placeholder="Количество месяцев">
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'information')->textArea(['rows' => 3]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'occupation')->textArea(['rows' => 3]) ?>
            </div>
        </div>
    </div>

  
    <?php ActiveForm::end(); ?>
    
</div>


<?php 
$this->registerJs(<<<JS
    $('#type').on('change', function() 
    {  
        var type = this.value; //console.log(type);
        $('#trener').hide(); $('#client').hide();
        if(type == 'client'){
             var html = $('.work-select').html();
             $('.work-select').remove();
             $('#client .work-wrapper').html(html);
             $('#client').show();
        }
        if(type == 'trener'){
            var html = $('.work-select').html();
             $('.work-select').remove();
             $('#trener .work-wrapper').html(html);
             $('#trener').show();
        }
    }
);
//
$( document ).ready(function() {
    var update_record = $('#updateRecord').val();
         console.log('update_record');
         console.log(update_record);
    if (update_record == 1) {
         var deleted_item = null;
     
         // var model_id = $('#model_id').val();
    
         $(".kv-file-remove.btn.btn-sm.btn-kv.btn-default.btn-outline-secondary").on('click', function () {
            console.log('v-file-remove clicked');
            var parent = $(this).closest('.file-preview-frame.krajee-default.file-preview-initial');
         
            deleted_item = parent.find('.file-preview-image').attr('src');
            console.log('deleted_item');
            console.log(deleted_item);
            //
            parent.addClass('hidden');
            // parent.addClass('hidden');
            $.post(
                '/users/additional-delete',
                {
                    deleted_item: deleted_item,
                    type: 'photos',
                    model_id: $('#model_id').val()
                },
                function(data) {
                    console.log('data');
                    console.log(data);
                }
            ).fail(function(data) {
                    console.log('error data');
                    console.log(data);
            });
        //     $('.nomenclature_update').on('click', function() {
        //     $.post(
        //         '/users/additional-delete',
        //         {
        //             deleted_item: deleted_item,
        //             education_scans: education_scans, 
        //             model_id: model_id
        //         },
        //         function(data) {
        //             console.log('data');
        //             console.log(data);
        //         }
        //     ).fail(function(data) {
        //             console.log('error data');
        //             console.log(data);
        //     });
        // });
    });   
    }
});

//
JS
);
?>