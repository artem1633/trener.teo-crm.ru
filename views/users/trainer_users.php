<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AtelierSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '';
//$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>

<div class="atelier-index">
    <?php if (!empty($trainer_users)) {

        foreach ($trainer_users as $trainer_user) { ?>

        <div class="row" style="padding-bottom: 10px; padding-top: 10px; border-bottom: 1px solid #999">
            <div class="col-xs-4 text-center">
                <img
                        class="img_round"
                        src="/web/<?= $trainer_user -> photo?>"
                        style="min-width: 80px;
                            width: 80px;
                            min-height: 80px;
                            height: 80px;
                            border-radius: 50%;"
                        alt=""
                >
            </div>
            <div class="col-xs-8">
                <a href="/users/tr-client?id=<?= $trainer_user->id?>">
                    <h4><?= $trainer_user -> fio?></h4>
                </a>
            </div>

        </div>

    <?php
        }
    } ?>

    <div class="row text-center">
        <div class="col-xs-12">
            <img
                    style="width: 160px;
                           height: 140px;
                           object-fit: cover;
                           margin-top: 20px;
                           cursor: pointer;"
                    src="/web/logos/add_user.png"
            >
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 text-right">
            <?= Html::a('<img src="/web/logos/plus.png" alt="">',
                [
                    '/users/create',
//                    'id' => $model->id,
//                    'only_photo' => 1,
                ],
                [
                    'role' => 'modal-remote',
                    'title' => 'Изменить',
                    'class' => 'btn'
                ]) ?>
        </div>
    </div>


    <div id="ajaxCrudDatatable">
<!--        --><?//=GridView::widget([
//            'id'=>'crud-datatable',
//            'dataProvider' => $dataProvider,
//            //'filterModel' => $searchModel,
//            'pjax'=>true,
//            'responsiveWrap' => false,
//            'columns' => require(__DIR__.'/_columns.php'),
//            'toolbar'=> [
//                ['content'=>
//                    Html::a('Создать', ['create'],
//                        ['role'=>'modal-remote','title'=> 'Создать','class'=>'btn btn-primary']).
//                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
//                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Обновить']).
//                    '{toggleData}'
//                ],
//            ],
//            'striped' => true,
//            'condensed' => true,
//            'responsive' => true,
//            'panel' => [
//                'type' => 'primary',
//                'heading' => '<i class="glyphicon glyphicon-list"></i> Список',
//                'before'=>'',
//                'after'=>'',
//            ]
//        ])?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

<?php
$this->registerJs(<<<JS

$( document ).ready(function() {
    // $( "table tbody" ).sortable( {
    //     update: function( event, ui ) {
    //     $(this).children().each(function(index) {
    //             $(this).find('td').last().html(index + 1)
    //     });
    //   }
    // });
});
JS
);
?>