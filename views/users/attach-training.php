<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\CoachingUsers;

/** @var int $user_id */
/** @var \app\models\CoachingUsers $model */

?>

<div class="nutritions-form">

    <?php

    $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'coaching_id')->dropDownList(\yii\helpers\ArrayHelper::map(CoachingUsers::getAvailableCoachings($user_id), 'id', 'name')) ?>

    <div class="hidden">
        <?= $form->field($model, 'users_id')->textInput(['maxlength' => true]) ?>
    </div>

    <?= Html::submitButton('Готово', ['class' => 'btn btn-theme btn-block']) ?>

    <?php ActiveForm::end(); ?>

</div>
