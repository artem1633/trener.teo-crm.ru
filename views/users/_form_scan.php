<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use app\models\Users;
use yii\helpers\Url;
use yii\web\JsExpression;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="users-form" style="padding: 20px;">

    <?php $form = ActiveForm::begin([ 'options' => ['method' => 'post', 'enctype' => 'multipart/form-data']]); ?>

    
    <div class="row">

        <div style="display: none;" >
            <?= $form->field($model, 'education_scans')->textarea(['rows' => 3]) ?>
        </div>

        <input id="number" name="number" type="hidden" value="<?=$number?>">
        <input id="path" name="path" type="hidden" value="<?= "images/products/".$number."/" ?>">
        <input id="url" name="url" type="hidden" value="<?= "http://" . $_SERVER['SERVER_NAME'] . '/' ?>">

        <input id="model_id" name="model_id" type="hidden" value="<?=$model -> id?>">

    </div>

        <div class="row">
            <div class="col-xs-12">
                <?php
                $education_scans_hidden = json_decode($model -> education_scans, true);
                if(!empty($education_scans_hidden)) {
                    $education_scans_hidden = implode(", ", $education_scans_hidden);
                } else {
                    $education_scans_hidden = "";
                }
                if(!empty($education_scans_hidden)) {
                    $education_scans = \app\models\Functions::additionalPhotosHtmlEdit($model -> education_scans);
                    echo $form->field($model, 'doc_scans[]')->widget(FileInput::classname(), [
                        'name' => 'attachment_48[]',
                        'options'=>[
                            'multiple'=>true
                        ],
                        'pluginOptions' => [
                            'initialPreviewShowDelete' => true,
                            'overwriteInitial' => false,
                            'showPreview' => true,
                            'showCaption' => true,
                            'showRemove' => true,
                            'showUpload' => false,
                            'initialPreview' => $education_scans,
                            'initialPreviewConfig' => [
                            ],
                        ],
                    ])->label(false);
                } else {
                    echo $form->field($model, 'doc_scans[]')->widget(FileInput::classname(), [
                        'name' => 'attachment_48[]',
                        'options' => [
                            'multiple' => true,
                        ],
                        'pluginOptions' => [
                            'showUpload' => false,
                            'browseLabel' => '',
                            'removeLabel' => '',
                            'mainClass' => 'input-group-lg'
                        ]
                    ])->label(false);
                }
                ?>
            </div>
        </div>


  
    <?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<?php
$this->registerJs(<<<JS
$( document ).ready(function() {
       
    var deleted_item = null;
    
         $(".kv-file-remove.btn.btn-sm.btn-kv.btn-default.btn-outline-secondary").on('click', function () {
            console.log('v-file-remove clicked');
            var parent = $(this).closest('.file-preview-frame.krajee-default.file-preview-initial');
         
            deleted_item = parent.find('.file-preview-image').attr('src');
            console.log('deleted_item');
            console.log(deleted_item);
            //
            parent.addClass('hidden');
            $.post(
                '/users/additional-delete',
                {
                    deleted_item: deleted_item,
                    type: 'education_scans',
                    model_id: $('#model_id').val()
                },
                function(data) {
                    console.log('data');
                    console.log(data);
                }
            ).fail(function(data) {
                    console.log('error data');
                    console.log(data);
            });
    });  
});

JS
);
?>