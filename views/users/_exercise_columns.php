<?php
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\grid\GridView;
return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'video',
    ], 
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'photo',
        'content' => function ($data) {
            return ($data->photo) ? Html::img('/uploads/'.$data->photo,['style'=>'width:150px; height:100px;text-align:center']) : null;
        },
    ], 
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'description',
        'format'=>'html', 
    ], 
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'exercise_group_id',
        'content' => function ($data) {
            return $data->exerciseGroup->name;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'trener_id',
        'content' => function ($data) {
            return $data->trener->fio;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'check',
        'content' => function ($data) {
            if($data->check == 1) return 'Да';
            else return 'Нет';
        },
    ],
    [
        'class'    => 'kartik\grid\ActionColumn',
        'template' => '{leadUpdate} {leadDelete}',
        'buttons'  => [
            'leadUpdate' => function ($url, $model) {
                $url = Url::to(['/exercise/change', 'id' => $model->id]);
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip']);
            },
            'leadDelete' => function ($url, $model) {
                $url = Url::to(['/exercise/remove', 'id' => $model->id]);
                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                    'role'=>'modal-remote','title'=>'', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить этот элемент?'
                ]);
            },
        ]
    ]

];   