<?php
use yii\helpers\Url;
use app\models\Users;
use yii\helpers\ArrayHelper;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'exercise_id',
        'content' => function($data){
            return $data->exercise->name;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'weight',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'repitition',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'approach',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'trener_id',
        'filter' => ArrayHelper::map(Users::find()->where(['permission' => Users::USER_ROLE_TRENER])->all(),'id','fio'),
        'content' => function($data){
            return $data->trener->fio;
        }
    ],

];   