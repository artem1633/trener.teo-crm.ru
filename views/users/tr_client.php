<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\models\Users;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use app\models\Functions;
use app\models\Chat;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use kartik\rating\StarRating;

$this->title = '';
CrudAsset::register($this);

if (!file_exists('avatars/' . $model->photo) || $model->photo == '') {
    $path = '@web/avatars/nouser.jpg';
//    $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/nouser.jpg';
} else {
    $path = '@web/avatars/' . $model->photo;
//    $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/'.$model->photo;
}
function getAge($birthday)
{
    $now = new DateTime();
    $birthtime = new DateTime($birthday);
    $interval = $now->diff($birthtime);
    return $interval->format("%Y");
}

if ($model->birthday != null) $year = getAge($model->birthday);
else $year = 0;

?>

<?php Pjax::begin(['enablePushState' => false, 'id' => 'profile-pjax']) ?>
    <div class="box box-solid box-warning">
        <div class="box-header">
            <h3 class="box-title">Профиль клиента</h3>
            <?php if ($role == 'administrator' || ($role == 'trener' && $model -> trener == Yii::$app->user->identity->id)): ?>
                <span class="pull-right">
            <?= Html::a('<i class="glyphicon glyphicon-pencil"></i>', ['/users/update', 'id' => $model->id], ['role' => 'modal-remote', 'title' => 'Изменить', 'class' => 'btn btn-primary btn-xs']) ?>
        </span>
            <?php endif; ?>
        </div>
        <div class="box-body" style="padding: 10px;">

            <div class="row" style="padding: 20px;">
                <div class="col-md-3 col-xs-6 col-sm-6">
                    <center>
                        <?php
                        $res = Yii::$app->request->baseUrl.'/web/' . $model->photo;
                        if (!empty($model->photo)) {
                            echo Html::img('/'.$model->photo, [
                                'style' => 'width: 150px; height:150px',
                                'class' => 'img-circle main_photo img-thumbnail',
                            ]);
                        }
                        ?>
                    </center>
                </div>
                <div class="col-md-6 col-xs-6 col-sm-6" style="padding-top: 20px;">
                    <div>
                        <?php
                        if (!empty($model->fio)) echo '<span style=" font-weight: bold; font-size: 16px; margin-top: 20px;">' . $model->fio . '</span><br>';
                        if (!empty($year)) echo '<span style="font-size: 14px;">' . $year . '</span>' . ' лет,' . '<br>';
                        if (!empty($model->address)) echo '<span style=" font-size: 14px;">' . $model->address . '</span>';
                        ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <ul class="nav nav-tabs">
                        <li class="">
                            <a href="#tab-1" data-toggle="tab" aria-expanded="true" style="color:black; ">
                                <span class="visible-xs">О клиенте</span>
                                <span class="hidden-xs">О клиенте</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="#tab-3" data-toggle="tab" aria-expanded="false" style="color:black; ">
                                <span class="visible-xs">Тренировки</span>
                                <span class="hidden-xs">Тренировки</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="#tab-2" data-toggle="tab" aria-expanded="false" style="color:black; ">
                                <span class="visible-xs">Питание</span>
                                <span class="hidden-xs">Питание</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="#tab-4" data-toggle="tab" aria-expanded="false" style="color:black; ">
                                <span class="visible-xs">Фото</span>
                                <span class="hidden-xs">Фото</span>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade" id="tab-1">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table_profile" style="margin-top: 20px;">
                                        <tbody>
                                        <tr>
                                            <th>
                                                <p style="margin-left: 10px; font-size: 15px;">Род деятельности</p>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 13px;"><?= Html::encode($model->occupation) ?></td>
                                        </tr>

                                        <tr>
                                            <th>
                                                <p style="margin-left: 10px; font-size: 15px;">Стаж тренировок:</p>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 13px;"><?= Html::encode($model->training_experience) ?></td>
                                        </tr>

                                        <tr>
                                            <th>
                                                <p style="margin-left: 10px; font-size: 15px;">
                                                    Фитнес клуб, в котором занимается клиент
                                                </p>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 13px;"><?= Html::encode($model->fitnes) ?></td>
                                        </tr>

                                        <tr>
                                            <th>
                                                <p style="margin-left: 10px; font-size: 15px;">
                                                    Цель тренировок:
                                                </p>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 13px;"><?= Html::encode($model->training_goal) ?></td>
                                        </tr>

                                        <tr>
                                            <th>
                                                <p style="margin-left: 10px; font-size: 15px;">Дополнительная информация</p>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 13px;">
                                                <?= Html::encode($model->information) ?>
                                            </td>
                                        </tr>

                                        <tr>
                                            <th>
                                                <p style="margin-left: 10px; font-size: 15px;">
                                                    Тренер:
                                                </p>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 13px;"><?= Html::encode(Functions::getTrainer($model->trener)) ?></td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab-2">
                            <br><br>
                            <h4 class="text-center">В работе.</h4>
                        </div>
                        <div class="tab-pane fade" id="tab-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php if ($role == 'administrator' || $role == 'trener'): ?>

                                        <?php Pjax::begin(['enablePushState' => false, 'id' => 'training-pjax']) ?>
                                        <div class="col-md-12">
                                            <div class="panel-body">
                                                <div class="atelier-index">
                                                    <div id="ajaxCrudDatatable">
                                                        <?=GridView::widget([
                                                            'id'=>'calls-datatable',
                                                            'dataProvider' => $trainingDataProvider,
                                                            //'filterModel' => $searchModel,
                                                            'pjax'=>true,
                                                            'responsiveWrap' => false,
                                                            'columns' => require(__DIR__.'/_coach_columns.php'),
                                                            'toolbar'=> [
                                                                ['content'=>
                                                                    Html::a('Создать', ['/training/add-coach'],
                                                                        ['role'=>'modal-remote','title'=> 'Создать','class'=>'btn btn-primary']).
                                                                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                                                                        ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Обновить']).
                                                                    '{toggleData}'
                                                                ],
                                                            ],
                                                            'striped' => true,
                                                            'condensed' => true,
                                                            'responsive' => true,
                                                            'panel' => [
                                                                'type' => 'primary',
                                                                'heading' => '<i class="glyphicon glyphicon-list"></i> Список',
                                                                'before'=>'',
                                                                'after'=>'',
                                                            ]
                                                        ])?>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <?php Pjax::end() ?>
                                    <?php endif; ?>

                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab-4">
                            <div class="col-md-12 text-center">
                                <?php $images = json_decode($model->photos);
                                if (!empty($images)) {
                                    $res = '/web/' . $model->photo;
                                    foreach ($images as $image) { ?>
                                        <img
                                            style="width: 120px; height: 100px; object-fit: cover; margin-top: 20px; cursor: pointer;"
                                            src="/<?= $image ?>" href="/<?= $image ?>">
                                    <?php }
                                } else { ?>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <img
                                                style="width: 120px; height: 100px; object-fit: cover; margin-top: 20px; cursor: pointer;"
                                                src="/logos/photo.png" href="/logos/photo.png">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 text-right">
                                            <?= Html::a('<img src="/logos/plus.png" alt="">',
                                                [
                                                    '/users/update',
                                                    'id' => $model->id,
                                                    'only_photo' => 1,
                                                ],
                                                [
                                                    'role' => 'modal-remote',
                                                    'title' => 'Изменить',
                                                    'class' => 'btn'
                                                ]) ?>
                                        </div>
                                    </div>

                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php Pjax::end() ?>

<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
    "size" => "modal-lg",
    "options" => [
        "open.bs.modal" => "function(){ console.log('123'); }",
        "tabindex" => false,
    ],
]) ?>
<?php Modal::end(); ?>


<?php
$script = <<< JS
$(document).ready(function() {
    console.log("ready!");
    
    $('a[href="#tab-1"]').click();
    //send_exercises
    // $('input[type="checkbox"]');
    $(document).on('click','input[type="checkbox"]', function(event){
        console.log("checkbox clicked!");
    });   
    //chat
    $(document).on('change','.field-chat-trainer_id select, .field-chat-client_id select', function(event) {
        
        var chat_form = $(this).closest('#w3');
        console.log('chat_form');
        console.log(chat_form);
        
        var selected_item = $(chat_form).find(":selected");
        console.log('selected_item');
        console.log(selected_item);
        
        var client_id = null;
        var trainer_id = null;
        
        
        var check_client = $('select#chat-client_id');
        var check_trainer = $('select#chat-trainer_id');
        
        if (check_client.length > 0) {
            
            client_id = $(check_client).val();
            console.log('client_id');
            console.log(client_id);
            
            trainer_id = $(chat_form).find('input[name="trainer_id"]').val();
            console.log('trainer_id');
            console.log(trainer_id);
            
        } else if (check_trainer.length > 0) {
            
            trainer_id = $(check_trainer).val();
            console.log('trainer_id');
            console.log(trainer_id);
            
            client_id = $(chat_form).find('input[name="client_id"]').val();
            console.log('client_id');
            console.log(client_id);
            
        }
        
        $.ajax({
                url: '/chat/chat-by-user',
                data: {
                    trainer_id: trainer_id,
                    client_id: client_id,
                },
                async: false,
                // type: 'json',
                type: 'get',
                success: function (chat) {
                    console.log('chat');
                    console.log(chat);
                    //
                    var chats = JSON.parse(chat);
                    console.log('chats');
                    console.log(chats);
                    for(key in chats){
                        var chat = chats[key];
                        var message = chat.message;
                        var whose_message = chat.whose_message;
                        var trainer_image = chat.trainer_image;
                        var created_date = chat.created_date;
                       
                        // console.log('message');
                        // console.log(message);
                        // console.log('whose_message');
                        // console.log(whose_message);
                        // console.log('trainer_image');
                        // console.log(trainer_image);
                        // console.log('created_date');
                        // console.log(created_date);
                       
                        if (whose_message === 'Тренер') {
                            // <img class="msg_img" src="/web/' + trainer_image + '" alt="sunil"> \
                            var element =
                            '<div class="incoming_msg"> \
                                <div class="incoming_msg_img"> \
                                    <img class="msg_img" src="' + trainer_image + '" alt="sunil"> \
                                </div> \
                                <div class="received_msg"> \
                                    <div class="received_withd_msg"> \
                                        <p> \
                                            ' +  message + ' \
                                        </p> \
                                        <span class="time_date">' +  created_date + '</span></div> \
                                </div> \
                            </div>';
                            $('.msg_history').append(element);
                         } else if (whose_message === 'Клиент') {
                            var element =
                            '<div class="outgoing_msg"> \
                                <div class="sent_msg"> \
                                    <p> \
                                        ' +  message + ' \
                                    </p> \
                                    <span class="time_date">' +  created_date + '</span></div> \
                            </div>';
                            $('.msg_history').append(element);
                         }
                    }
                }, 
                fail: function(data) {
                   console.log('error data');
                    console.log(data);
                }
        });
        
    });

});
JS;
$this->registerJs($script);
?>