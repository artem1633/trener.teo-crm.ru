<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use app\models\Users;
use yii\helpers\Url;
use yii\web\JsExpression;
use kartik\file\FileInput;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="users-form" style="padding: 20px;">

    <?php $form = ActiveForm::begin([ 'options' => ['method' => 'post', 'enctype' => 'multipart/form-data']]); ?>

    <div id="trener" <?php if($model->permission == Users::USER_ROLE_CLIENT) echo 'style=" display: none;"';?> >

        <div class="row portfolio_photo">

            <div class="col-xs-12">
                <?php
                    if(!empty($model -> file)) {
                        $photo = \app\models\Functions::additionalPhotosHtmlEdit($model -> photo);
                        echo $form->field($model, 'file')->widget(FileInput::classname(), [
                            'name' => 'file',
                            'options'=>[
                                'multiple'=>false,
                                'accept' => 'image/*'
                            ],
                            'pluginOptions' => [
                                'initialPreviewShowDelete' => true,
                                'allowedFileExtensions' => ['jpg', 'jpeg', 'png'],
                                'overwriteInitial' => false,
                                'showPreview' => true,
                                'showCaption' => true,
                                'showRemove' => true,
                                'showUpload' => true,
                                'initialPreview' => $photo,
                                'initialPreviewConfig' => [
                                ],
                            ],
                        ])->label("Фото");
                    }
                    else {
                        echo 'this';
                        echo $form->field($model, 'file')->widget(FileInput::classname(), [
                            'name' => 'file',
                            'options' => [
                                'multiple' => false,
                                'accept' => 'image/*'
                            ],
                            'pluginOptions' => [
                                'browseClass' => 'btn btn-success',
                                'uploadClass' => 'btn btn-info',
                                'showRemove' => true,
                                'showUpload' => true,
                                'removeClass' => 'btn btn-danger',
                                'removeIcon' => '<i class="glyphicon glyphicon-trash"></i> ',
                                'uploadUrl' => Url::to(['/users/file-upload?id=' . $model->id]),
                            ],
                        ])->label("Фото");
                    }
                ?>
            </div>
        </div>

    </div>

  
    <?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton('Редактировать', ['class' => 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>


<?php 
$this->registerJs(<<<JS
    $('#type').on('change', function() 
    {  
        var type = this.value; //console.log(type);
        $('#trener').hide(); $('#client').hide();
        if(type == 'client') $('#client').show(); 
        if(type == 'trener') $('#trener').show(); 
    }
);
//
$( document ).ready(function() {
    var update_record = $('#updateRecord').val();
         console.log('update_record');
         console.log(update_record);
    if (update_record == 1) {
         var deleted_item = null;
     
         // var model_id = $('#model_id').val();
    
         $(".kv-file-remove.btn.btn-sm.btn-kv.btn-default.btn-outline-secondary").on('click', function () {
            console.log('v-file-remove clicked');
            var parent = $(this).closest('.file-preview-frame.krajee-default.file-preview-initial');
         
            deleted_item = parent.find('.file-preview-image').attr('src');
            console.log('deleted_item');
            console.log(deleted_item);
            //
            parent.addClass('hidden');
            // parent.addClass('hidden');
            $.post(
                '/users/additional-delete',
                {
                    deleted_item: deleted_item,
                    type: 'photos',
                    model_id: $('#model_id').val()
                },
                function(data) {
                    console.log('data');
                    console.log(data);
                }
            ).fail(function(data) {
                    console.log('error data');
                    console.log(data);
            });
        //     $('.nomenclature_update').on('click', function() {
        //     $.post(
        //         '/users/additional-delete',
        //         {
        //             deleted_item: deleted_item,
        //             education_scans: education_scans, 
        //             model_id: model_id
        //         },
        //         function(data) {
        //             console.log('data');
        //             console.log(data);
        //         }
        //     ).fail(function(data) {
        //             console.log('error data');
        //             console.log(data);
        //     });
        // });
    });   
    }
});

//
JS
);
?>