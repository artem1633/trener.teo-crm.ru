<?php

use Yii;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\Pjax;
use app\models\Users;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use app\models\Functions;
use yii\widgets\ActiveForm;
use kartik\rating\StarRating;

/** @var app\models\Users $model */

$this->title = '';
CrudAsset::register($this);

\app\assets\plugins\OwlCarouselAsset::register($this);

if (!file_exists('avatars/' . $model->photo) || $model->photo == '') {
    $path = '@web/avatars/nouser.jpg';
//    $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/nouser.jpg';
} else {
    $path = '@web/avatars/' . $model->photo;
//    $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/'.$model->photo;
}
function getAge($birthday)
{
    $now = new DateTime();
    $birthtime = new DateTime($birthday);
    $interval = $now->diff($birthtime);
    return $interval->format("%Y");
}

/**
 * @param Users $model
 * @return string
 */
function getProfileBaseInfo($model)
{
    $output = [];

    if($model->birthday != null)
    {
        $birthday = getAge($model->birthday);
        $output[] = getAge($model->birthday).' '.\php_rutils\RUtils::numeral()->choosePlural($birthday, ['год', 'года', 'лет']);
    }

    if($model->address != null)
    {
        $output[] = $model->address;
    }

    if($model->country != null)
    {
        $output[] = $model->countryName;
    }

    return implode(', ', $output);
}

if ($model->birthday != null) $year = getAge($model->birthday);
else $year = 0;


$currentUserId = Yii::$app->user->identity->id;


\app\assets\plugins\MagnificPopup::register($this);
?>

<?php Pjax::begin(['id' => 'profile-header-container', 'enablePushState' => false]) ?>
<div class="profile">
    <img src="/<?=$model->photo?>" <?=$model->id == Yii::$app->user->getId() ? 'data-img-upload="upload"' : ''?> class="profile-image image-bordered">
    <div class="profile-info">
        <div class="profile-name"><?=$model->fio?></div>
        <div class="profile-text"><?=getProfileBaseInfo($model)?></div>
    </div>
    <?php
    if($model->id == $currentUserId) {
        echo Html::a('Редактировать', ['update', 'id' => Yii::$app->user->getId()], ['class' => 'btn btn-theme', 'data-pjax' => 0, 'style' => 'margin-top: 15px; border-radius: 1em; display: block; width: 150px;']);
    } else if($model->permission == Users::USER_ROLE_CLIENT && $model->trener == $currentUserId) {
        echo Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-theme', 'data-pjax' => 0, 'style' => 'margin-top: 15px; border-radius: 1em; display: block; width: 150px;']);
    } else if (Yii::$app->user->identity->permission == 'administrator') {
        echo Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-theme', 'data-pjax' => 0, 'style' => 'margin-top: 15px; border-radius: 1em; display: block; width: 150px;']);
    }
//    else if($model->telephone != null) {
//        echo Html::a('<i class="fa fa-whatsapp"></i>', 'https://wa.me/'.$model->telephone);
//    }

    ?>
</div>
<?php Pjax::end() ?>

<div class="navs-data">
        <ul class="nav nav-tabs">
            <li class="">
                <a href="#tab-1" data-toggle="tab" aria-expanded="false" style="color:white; padding-left: 15px;">
                    <span class="visible-xs">Фото</span>
                    <span class="hidden-xs">Фото</span>
                </a>
            </li>
            <li class="">
                <a href="#tab-2" data-toggle="tab" aria-expanded="true" style="color:white; padding: 10px; ">
                    <span class="visible-xs">Обо мне</span>
                    <span class="hidden-xs">Обо мне</span>
                </a>
            </li>
            <?php if($model->permission == Users::USER_ROLE_TRENER): ?>
                <li class="">
                    <a href="#tab-3" data-toggle="tab" aria-expanded="false" style="color:white; padding: 10px; ">
                        <span class="visible-xs">Цены</span>
                        <span class="hidden-xs">Цены</span>
                    </a>
                </li>
                <li class="">
                    <a href="#tab-4" data-toggle="tab" aria-expanded="false" style="color:white; padding: 10px; ">
                        <span class="visible-xs">Отзывы</span>
                        <span class="hidden-xs">Отзывы</span>
                    </a>
                </li>
            <?php endif; ?>
            <?php if($model->trener == Yii::$app->user->getId()): ?>
                <li>
                    <a href="#tab-5" data-toggle="tab" aria-expanded="false" style="color:white; padding: 10px; ">
                        <span class="visible-xs">Тренировки</span>
                        <span class="hidden-xs">Тренировки</span>
                    </a>
                </li>
                <li>
                    <a href="#tab-6" data-toggle="tab" aria-expanded="false" style="color:white; padding: 10px; ">
                        <span class="visible-xs">Питание</span>
                        <span class="hidden-xs">Питание</span>
                    </a>
                </li>
            <?php endif; ?>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade" id="tab-1" style="text-align: left; padding: 0; word-spacing: -4px; word-wrap: normal;">
                <div class="box-panel">
                    <?php Pjax::begin(['id' => 'pjax-photos-container', 'enablePushState' => false]) ?>
                    <?php $images = json_decode($model->photos);
                    if (!empty($images)) {
//                        $res = '/web/' . $model->photo;
                        echo '<div class="popup-gallery">';

                        foreach ($images as $image) { ?>
                            <a class="image-popup-vertical-fit" href="/<?=$image?>"><img
                                        style="width: 25%; height: 100px; object-fit: cover; cursor: pointer;"
                                        src="/<?= $image ?>" /></a>
                            <?=Html::a("Принять запрос",'#', ['class' => 'btn btn-xs btn-theme btn-accept', 'style' => 'margin-left: 11px; margin-bottom: 15px; display: inline-block;',
                                'onclick' => ' var self = $(this);
                                Controlls.sendBinaryRequest($(this), "'.\yii\helpers\Url::toRoute(['delete-photo']).'", {"id": '.Yii::$app->user->identity->id.', "photo_path": '.$image.'}, null, "Ошибка", function(data){
                                    var btnRefuse = self.parent().find(".btn-refuse");
                                    var width = Math.round(self.width() + btnRefuse.width() + 2);
                                    self.parent().find(".btn-refuse").hide();
                                    self.width(width);
                                    self.html("Принято");
                                });
                            '])?>
                        <?php }

                        echo '</div>';

                    } else { ?>
                    <?php } ?>

                    <?php if($model->id == Yii::$app->user->getId() || Yii::$app->user->identity->permission == Users::USER_ROLE_ADMIN): ?>
                        <div class="row">
                            <div class="col-xs-12">
                                <img
                                        style="width: 120px; height: 100px; object-fit: cover; margin-top: 20px; cursor: pointer;" id="photos-upload"
                                        src="/logos/photo.png" href="/logos/photo.png">
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="row">
                    </div>
                    <?php Pjax::end() ?>
                </div>
            </div>
            <div class="tab-pane fade" id="tab-2">
                <?php if(Yii::$app->user->identity->permission == Users::USER_ROLE_CLIENT && $model->permission == Users::USER_ROLE_TRENER && $model->id != Yii::$app->user->identity->trener
                    && !\app\models\ClientsRequests::hasRequest(Yii::$app->user->getId(), $model->id)){ ?>
                    <div class="row">
                        <div class="col-md-12">
                            <?=Html::a('Отправить запрос', '#', ['class' => 'btn btn-theme btn-block transition-25',
                                'onclick' => 'Controlls.sendBinaryRequest($(this), "'.\yii\helpers\Url::toRoute(['training/send-client-request']).'", {trainer_id: '.$model->id.'});'])?>
                        </div>
                    </div>
                <?php } else if(\app\models\ClientsRequests::hasRequest(Yii::$app->user->getId(), $model->id)){ ?>
                    <?=Html::a('<i class="fa fa-check "></i> Запрос успешно отправлен', '#', ['class' => 'btn btn-default btn-block transition-25']) ?>
                <?php } ?>
                <div class="row">
                    <tbody class="col-md-12" style="padding: 0;">
<!--                        <input type="file" id="test-file-input">-->
<!--                        <span id="test-file-btn" class="btn btn-theme btn-lg">НАЖМИ НА МЕНЯ</span>-->
<!--                        <span id="test-file-btn-multi" class="btn btn-theme btn-lg">НАЖМИ НА МЕНЯ (множественный выбор)</span>-->
                        <table class="table table_profile" style="margin-top: 10px;">
                            <tbody>

                            <?php if ($model->permission == 'trener') { ?>
                                <tr>
                                    <th>
                                        <p><img src="/images/medal.png" alt="" style="height: 30px;"> <?= $model->getAttributeLabel('work_experience') ?></p>
                                    </th>
                                </tr>
                                <tr>
                                    <td class="th-value">
                                        <p><?= Html::encode($model->work_experience) ?></p>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <p><img src="/images/books.png" alt="" style="height: 20px; margin-right: 20px;"> <?= $model->getAttributeLabel('education') ?> и документы:</p>
                                        <?php if($model->id == Yii::$app->user->getId()): ?>
                                            <?= Html::a('<i class="glyphicon glyphicon-plus"></i>',
                                                '#',
                                               ['id' => 'documents-upload', 'title' => 'Добавить', 'class' => 'btn btn-theme-circle btn-xs pull-right', 'style' => 'position: absolute; right: 5%;']) ?>
                                        <?php endif; ?>
                                    </th>
                                </tr>
                                <tr>
                                    <th>

                                    </th>
                                </tr>
                                <tr>
                                    <td style="font-size: 13px; padding-top: 0;">
                                        <?php Pjax::begin(['id' => 'pjax-documents-container', 'enablePushState' => false]) ?>
                                        <div class="col-md-12">
                                            <div id="photos-gallery" class="popup-gallery">
                                                <?php $images = json_decode($model->education_scans, true);
                                                foreach ($images as $image) {
                                                    ?>
                                                    <?php $res = '/web/'; ?>
                                                   <div class="image-item" style="width: 90px; height: 90px; display: inline-block;">
                                                       <a class="image-popup-vertical-fit" href="/<?=$image?>"><img
                                                                   style="width: 90px; height: 90px; object-fit: cover; margin-top: 20px; cursor: pointer;"
                                                                   src="/<?= $image ?>" />
                                                       </a>
                                                       <?=Html::button("Удалить", ['class' => 'btn btn-xs btn-danger btn-accept', 'style' => 'margin: auto; margin-top: 10px; display: block;',
                                                           'onclick' => 'var self = $(this);
                                                                Controlls.sendBinaryRequest($(this), "'.\yii\helpers\Url::toRoute(['delete-document']).'", {"id": '.Yii::$app->user->identity->id.', "photo_path": "'.$image.'"}, null, "Ошибка", function(data){
                                                                    // var btnRefuse = self.parent().find(".btn-refuse");
                                                                    // var width = Math.round(self.width() + btnRefuse.width() + 2);
                                                                    // self.parent().find(".btn-refuse").hide();
                                                                    // self.width(width);
                                                                    self.html("Уделено");
                                                                });
                                                                return;
                                                            '])?>
                                                   </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <?php Pjax::end() ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <h3>Контакты</h3>
                        <table class="table table_profile table_contacts" style="margin-top: 10px;">
                            <tbody>
                                <tr>
                                    <th>
                                        <p><img src="/images/house.png" alt="" style="height: 20px; margin-right: 20px;"> <span><?= Html::encode($model->address) ?></span> </p>
                                    </th>
                                </tr>
                                <tr>
                                    <th>
                                        <p><img src="/images/phone.png" alt="" style="height: 20px; margin-right: 20px;"> <span><?= Html::encode($model->telephone) ?></span> </p>
                                    </th>
                                </tr>
                                <tr style="margin-top: 20px;">
                                    <th>
                                        <p><img src="/images/instagram.png" alt="" style="height: 20px; margin-right: 20px;"> <?= Html::a($model->instagram, $model->instagramProfileUrl, ['target' => '_blank']) ?></p>
                                    </th>
                                </tr>
                            </tbody>
                        </table>
                            <?php } ?>
                            <?php if ($model->permission == 'client') { ?>
                                <tr>
                                    <th>
                                        <p><img src="/images/medal.png" alt="" style="height: 30px;"> <?= $model->getAttributeLabel('training_experience') ?></p>
                                    </th>
                                </tr>
                                <tr>
                                    <td class="th-value">
                                        <p><?= Html::encode($model->training_experience) ?></p>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <p><i class="fa fa-info" style="color: #0087e0; margin-right: 35px; font-size: 20px;"></i> <?= $model->getAttributeLabel('information') ?></p>
                                    </th>
                                </tr>
                                <tr>
                                    <td class="th-value">
                                        <p><?= Html::encode($model->information) ?></p>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <p><i class="fa fa-user" style="color: #0087e0; margin-right: 30px; font-size: 20px;"></i> <?= $model->getAttributeLabel('occupation') ?></p>
                                    </th>
                                </tr>
                                <tr>
                                    <td class="th-value">
                                        <p><?= Html::encode($model->occupation) ?></p>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <p><i class="fa fa-map-marker" style="color: #0087e0; margin-right: 33px; font-size: 20px;"></i> <?= $model->getAttributeLabel('fitnes') ?></p>
                                    </th>
                                </tr>
                                <tr>
                                    <td class="th-value">
                                        <p><?= Html::encode($model->fitnes) ?></p>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <p><i class="fa fa-crosshairs" style="color: #0087e0; margin-right: 30px; font-size: 20px;"></i> <?= $model->getAttributeLabel('training_goal') ?></p>
                                    </th>
                                </tr>
                                <tr>
                                    <td class="th-value">
                                        <p><?= Html::encode($model->training_goal) ?></p>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <p><i class="fa fa-user-o" style="color: #0087e0; margin-right: 30px; font-size: 20px;"></i> <?= $model->getAttributeLabel('trener') ?></p>
                                    </th>
                                </tr>
                                <tr>
                                    <td class="th-value">
                                        <p><?= Html::a($model->myTrainer->fio, ['profile', 'id' => $model->myTrainer->id]) ?></p>
                                    </td>
                                </tr>
                    </tbody>
                    </table>
                                <h3>Контакты</h3>
                                <table class="table table_profile table_contacts" style="margin-top: 10px;">
                                    <tbody>
                                    <tr>
                                        <th>
                                            <p><img src="/images/house.png" alt="" style="height: 20px; margin-right: 20px;"> <span><?= Html::encode($model->address) ?></span> </p>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th>
                                            <p><img src="/images/phone.png" alt="" style="height: 20px; margin-right: 20px;"> <span><?= Html::encode($model->telephone) ?></span> </p>
                                        </th>
                                    </tr>
                                    <tr style="margin-top: 20px;">
                                        <th>
                                            <p><img src="/images/instagram.png" alt="" style="height: 20px; margin-right: 20px;"> <?= Html::a($model->instagram, $model->instagramProfileUrl, ['target' => '_blank']) ?></p>
                                        </th>
                                    </tr>
                                    </tbody>
                                </table>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            <div class="tab-pane fade" id="tab-3" style="padding: 0;">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box-panel">
                            <?php if ($model->permission == 'administrator' || $model->permission == 'trener'): ?>
                                <?php Pjax::begin(['enablePushState' => false, 'id' => 'training-pjax']) ?>
                                <?php $orders = $ordersdataProvider->models; ?>
                                <table class="table table_profile" style="margin-top: 10px;">
                                    <tbody>
                                    <tr>
                                        <th>
                                            <p><i class="fa fa-calendar-o"></i> Стоимость одного занятия</p>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td class="th-value">
                                            <p><?= Html::encode($model->cost) ?> руб.</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            <p><i class="fa fa-calendar"></i> Стоимость месяца занятий</p>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td class="th-value">
                                            <p><?= Html::encode($model->cost_month) ?> руб.</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            <p><i class="fa fa-calendar"></i> Реквизиты</p>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td class="th-value">
                                            <p><?= Html::encode($model->props) ?></p>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <?php Pjax::end() ?>
                            <?php endif; ?>
                        </div>

                    </div>
                </div>
            </div>
            <div class="tab-pane fade trainer_feedback" id="tab-4" style="padding: 0;">
                <div class="box-panel">
                    <?php
                    if (!empty($ratings)) {
                        foreach ($ratings as $rating) {
                            $client = Functions::getUser($rating->client_id);
                            ?>

                            <div class="row" style="margin-top: 15px;">
                                <div class="col-xs-5">
                                    <div class="row text-center">
                                        <?php if (!empty($client->photo)) { ?>
                                            <?php
                                            $res1 = '/web/';
                                            ?>
                                            <img class="trainer_feedback_img"
                                                 style="border-radius: 50%;width: 85px;height: 85px;"
                                                 src="/<?= $client->photo ?>" alt="">
                                        <?php } else { ?>
                                            <img class="trainer_feedback_img"
                                                 style="border-radius: 50%;width: 85px;"
                                                 src="/logos/nouser.png" alt="">
                                            <?php
                                            $res2 = '/web/';
                                            ?>
                                        <?php } ?>
                                    </div>
                                    <p class="text-center" style="width: 110px; display: inline-block; font-family: Ubuntu; color: #303030;"><?= $client->fio; ?></p>
                                </div>
                                <div class="col-xs-7 text-left">
                                    <div class="row">
                                        <div class="raiting">
                                            <?php
                                            //                                        echo StarRating::widget([
                                            //                                            'name' => 'rating',
                                            //                                            'value' => $rating->rating,
                                            //                                            'pluginOptions' => [
                                            //                                                'displayOnly' => true
                                            //                                            ]
                                            //                                        ]);
                                            ?>

                                            <?php

                                            $max = 5;
                                            $value = $rating->rating;
                                            $remain = $max - $value;

                                            for ($i = 0; $i < $value; $i++) {
                                                echo '<i class="fa fa-star text-gold"></i>';
                                            }

                                            for ($i = 0; $i < $remain; $i++) {
                                                echo '<i class="fa fa-star text-light-gray"></i>';
                                            }

                                            echo "<span class='text-default' style='margin-left: 10px; font-family: Ubuntu;'>$value</span>";

                                            ?>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <p class="raiting-text">
                                            <?= $rating->message ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <br>

                        <?php }
                    } else { ?>
                        <h4 class="text-center">Отзывов пока нет.</h4>
                    <?php }
                    ?>
                </div>
            </div>
            <div class="tab-pane fade" id="tab-5" style="padding: 0">
                <div class="box-panel">
                    <?php if($model->trener == Yii::$app->user->getId()): ?>
                        <?= Html::a('Добавить тренировку', ['attach-training', 'id' => $model->id], ['class' => 'btn btn-theme btn-block']) ?>
                    <?php endif; ?>
                    <ul class="items-list text-left">
                        <?php foreach ($model->coachings as $coaching): ?>
                            <li>
                                <?= Html::a($coaching->name.'<br><span style="font-size: 12px;">14.01.2019</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size: 12px;">Выполнена 0 раз</span>',
                                    ['training/view', 'id' => $coaching->id], ['class' => 'list-item', 'style' => 'width: 100%;', 'data-pjax' => 0]) ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
            <div class="tab-pane fade" id="tab-6" style="padding: 0">
                <div class="box-panel">
                    <?php if($model->trener == Yii::$app->user->getId()): ?>
                        <?= Html::a('Добавить программу', ['attach-nutrition', 'id' => $model->id], ['class' => 'btn btn-theme btn-block']) ?>
                    <?php endif; ?>
                    <ul class="items-list text-left">
                        <?php foreach ($model->nutritions as $nutrition): ?>
                            <li>
                                <?= Html::a($nutrition->name,
                                    ['nutritions/view', 'id' => $nutrition->id], ['class' => 'list-item', 'style' => 'width: 100%;', 'data-pjax' => 0]) ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
</div>

<?php Pjax::begin(['enablePushState' => false, 'id' => 'profile-pjax']) ?>
    <div class="box box-solid box-warning" style="display: none;">
        <div class="box-header">
            <h3 class="box-title">Мой профиль</h3>
            <?php if ($role == 'administrator' || $role == 'trener'): ?>
                <span class="pull-right">
            <?= Html::a('<i class="glyphicon glyphicon-pencil"></i>', ['/users/update', 'id' => $model->id], ['role' => 'modal-remote', 'title' => 'Изменить', 'class' => 'btn btn-primary btn-xs']) ?>
        </span>
            <?php endif; ?>
        </div>
        <div class="box-body" style="padding: 10px;">

            <div class="row" style="padding-top:20px;padding-bottom:20px;padding-left:20px;">
                <div class="col-md-3 col-xs-6 col-sm-6" >
                    <?php Pjax::begin(['id' => 'profile-header-container', 'enablePushState' => false]) ?>
                    <center>
                        <?php
                        $res = Yii::$app->request->baseUrl.'/web/' . $model->photo;
                        if (!empty($model->photo)) {
                            echo Html::img('/'.$model->photo, [
                                'style' => 'width: 90px; height:90px;min-width: 90px; min-height:90px;',
                                'class' => 'img-circle main_photo img-thumbnail',
                            ]);
                        }
                        ?>
                    </center>
                    <?php Pjax::end() ?>
                </div>
                <div class="col-md-6 col-xs-6 col-sm-6" style="padding-top: 10px; padding-right:10px; width:65%;margin-left:20px;">
                    <div>
                        <?php
                        if (!empty($model->fio)) echo '<span style=" font-weight: bold; font-size: 16px; margin-top: 20px;">' . $model->fio . '</span><br>';
                        if (!empty($year)) echo '<span style="font-size: 14px;">' . $year . '</span>' . ' ' . \php_rutils\RUtils::numeral()->choosePlural($year, ['год', 'года', 'лет']) . ',' . '<br>';
                        if (!empty($model->address)) echo '<span style=" font-size: 14px;">' . $model->address . '</span>';
                        ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <ul class="nav nav-tabs" style="background-color: #0084ea; margin: 5px; ">
                        <li class="">
                            <a href="#tab-1" data-toggle="tab" aria-expanded="false" style="color:white; padding-left: 15px;">
                                <span class="visible-xs">Фото</span>
                                <span class="hidden-xs">Фото</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="#tab-2" data-toggle="tab" aria-expanded="true" style="color:white; padding: 10px; ">
                                <span class="visible-xs">Обо мне</span>
                                <span class="hidden-xs">Обо мне</span>
                            </a>
                        </li>

                        <li class="">
                            <a href="#tab-3" data-toggle="tab" aria-expanded="false" style="color:white; padding: 10px; ">
                                <span class="visible-xs">Цены</span>
                                <span class="hidden-xs">Цены</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="#tab-4" data-toggle="tab" aria-expanded="false" style="color:white; padding: 10px; ">
                                <span class="visible-xs">Отзывы</span>
                                <span class="hidden-xs">Отзывы</span>
                            </a>
                        </li>

                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade" id="tab-1">
                            <div class="col-md-12 text-center">
                                <div class="box-panel">
                                    <?php Pjax::begin(['id' => 'pjax-photos-container', 'enablePushState' => false]) ?>
                                    <?php $images = json_decode($model->photos);
                                    if (!empty($images)) {
                                        $res = '/web/' . $model->photo;
                                        foreach ($images as $image) { ?>
                                            <img
                                                    style="width: 120px; height: 100px; object-fit: cover; margin-top: 20px; cursor: pointer;"
                                                    src="http://trener.loc/<?= $image ?>" href="http://trener.loc/<?= $image ?>">
                                        <?php }
                                    } else { ?>
                                        <?php if($model->id == Yii::$app->user->getId() || Yii::$app->user->identity->permission == Users::USER_ROLE_ADMIN): ?>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <img
                                                            style="width: 120px; height: 100px; object-fit: cover; margin-top: 20px; cursor: pointer;"
                                                            src="/logos/photo.png" id="photos-upload-2" href="/logos/photo.png">
                                                </div>
                                            </div>
                                            <div class="row">
                                            </div>
                                        <?php endif; ?>

                                    <?php } ?>
                                    <?php Pjax::end() ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab-2">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table_profile" style="margin-top: 20px;">
                                        <tbody>

                                        <?php if ($model->permission == 'trener') { ?>
                                            <tr>
                                                <th>
                                                    <p style="margin-left: 10px; font-size: 15px;"><?= $model->getAttributeLabel('work_experience') ?></p>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td style="font-size: 13px;">
                                                    <p><?= Html::encode($model->work_experience) ?></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <p style="margin-left: 10px; font-size: 15px;"><?= $model->getAttributeLabel('education') ?></p>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td style="font-size: 13px;">
                                                    <p><?= Html::encode($model->education) ?></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <p style="margin-left: 10px; font-size: 15px;"><?= $model->getAttributeLabel('education_scans') ?>
                                                        <?= Html::a('<i class="glyphicon glyphicon-plus"></i>',
                                                            '#',
                                                            ['id' => 'documents-upload', 'title' => 'Добавить', 'class' => 'btn btn-primary btn-xs']) ?>
                                                    </p>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td style="font-size: 13px;">
                                                    <?php Pjax::begin(['id' => 'pjax-documents-container', 'enablePushState' => false]) ?>
                                                    <div class="col-md-12">
                                                        <?php $images = json_decode($model->education_scans, true);
                                                        foreach ($images as $image) {
                                                            ?>
                                                            <?php $res = '/web/'; ?>
                                                            <img
                                                                style="width: 120px; height: 100px; object-fit: cover; margin-top: 20px; cursor: pointer;"
                                                                src="<?= $image ?>" href="/<?= $image ?>">
                                                        <?php } ?>
                                                    </div>
                                                    <?php Pjax::end(); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <p style="margin-left: 10px; font-size: 15px;"><?= $model->getAttributeLabel('instagram') ?></p>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td style="font-size: 13px;"><?= Html::encode($model->instagram) ?></td>
                                            </tr>
                                        <?php } ?>
                                        <?php if ($model->permission == 'client') { ?>
                                            <tr>
                                                <th>
                                                    <b style="margin-left: 10px; font-size: 15px;"><?= $model->getAttributeLabel('information') ?></b>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td style="font-size: 13px;"><?= Html::encode($model->information) ?></td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <b style="margin-left: 10px; font-size: 15px;"><?= $model->getAttributeLabel('occupation') ?></b>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td style="font-size: 13px;"><?= Html::encode($model->occupation) ?></td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <b style="margin-left: 10px; font-size: 15px;"><?= $model->getAttributeLabel('training_experience') ?></b>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td style="font-size: 13px;"><?= Html::encode($model->training_experience) ?></td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <b style="margin-left: 10px; font-size: 15px;"><?= $model->getAttributeLabel('fitnes') ?></b>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td style="font-size: 13px;"><?= Html::encode($model->fitnes) ?></td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <b style="margin-left: 10px; font-size: 15px;"><?= $model->getAttributeLabel('training_goal') ?></b>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td style="font-size: 13px;"><?= Html::encode($model->training_goal) ?></td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <b style="margin-left: 10px; font-size: 15px;"><?= $model->getAttributeLabel('trener') ?></b>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td style="font-size: 13px;"><?= Html::encode($model->trener) ?></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box-panel">
                                        <?php if ($role == 'administrator' || $role == 'trener'): ?>

                                            <?php Pjax::begin(['enablePushState' => false, 'id' => 'training-pjax']) ?>
                                            <div class="col-md-12">
                                                <div class="panel-body">
                                                    <div id="ajaxCrudDatatable">
                                                        <?= GridView::widget([
                                                            'id' => 'orders-datatable',
                                                            'dataProvider' => $ordersdataProvider,
                                                            'pjax' => true,
                                                            'responsiveWrap' => false,
                                                            'columns' => require(__DIR__ . '/_exercise_columns.php'),
                                                            'panelBeforeTemplate' => Html::a('Добавить <i class="fa fa"></i>', ['/exercise/add', 'trener_id' => $model->id],
                                                                    ['role' => 'modal-remote', 'title' => 'Добавить', 'class' => 'btn btn-primary']) . '&nbsp',
                                                            'striped' => true,
                                                            'condensed' => true,
                                                            'responsive' => true,
                                                            'panel' => [
                                                                'headingOptions' => ['style' => 'display: none;'],
                                                                'after' => '',
                                                            ]
                                                        ]) ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php Pjax::end() ?>
                                        <?php endif; ?>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade trainer_feedback" id="tab-4">
                            <br><br>
                            <div class="box-panel">
                                <?php
                                if (!empty($ratings)) {
                                    foreach ($ratings as $rating) {
                                        $client = Functions::getUser($rating->client_id);
                                        ?>

                                        <div class="row">
                                            <div class="col-xs-5">
                                                <div class="row text-center">
                                                    <?php if (!empty($client->photo)) { ?>
                                                        <?php
                                                        $res1 = '/web/';
                                                        ?>
                                                        <img class="trainer_feedback_img"
                                                             style="border-radius: 50%;width: 85px;height: 85px;"
                                                             src="/<?= $client->photo ?>" alt="">
                                                    <?php } else { ?>
                                                        <img class="trainer_feedback_img"
                                                             style="border-radius: 50%;width: 85px;"
                                                             src="/logos/nouser.png" alt="">
                                                        <?php
                                                        $res2 = '/web/';
                                                        ?>
                                                    <?php } ?>
                                                </div>
                                                <div class="row">
                                                    <p class="text-center"><?= $client->fio; ?></p>
                                                </div>
                                            </div>
                                            <div class="col-xs-7">
                                                <div class="row">
                                                    <div class="raiting">
                                                        <?php
                                                        echo StarRating::widget([
                                                            'name' => 'rating',
                                                            'value' => $rating->rating,
                                                            'pluginOptions' => [
                                                                'displayOnly' => true
                                                            ]
                                                        ]);
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <p>
                                                        <?= $rating->message ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <br>

                                    <?php }
                                } else { ?>
                                    <h4 class="text-center">Отзывов пока нет.</h4>
                                <?php }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php Pjax::end() ?>

    <?php $form = ActiveForm::begin([
        'id' => 'upload-photos-form',
        'action' => ['users/add-profile-photo'],
        'options' => ['enctype' => 'multipart/form-data', 'class' => 'hidden'],
    ]);
    $uploadModel = new \app\models\form\UploadForm();
    ?>

    <?= $form->field($uploadModel, 'imageFile[]')->fileInput(['multiple' => true, 'accept' => 'image/*']) ?>

    <?php ActiveForm::end() ?>

    <?php $form = ActiveForm::begin([
        'id' => 'upload-documents-form',
        'action' => ['users/add-documents-photo'],
        'options' => ['enctype' => 'multipart/form-data', 'class' => 'hidden'],
    ]);
    $uploadModel = new \app\models\form\UploadForm();
    ?>

    <?= $form->field($uploadModel, 'imageFile[]')->fileInput(['multiple' => true, 'accept' => 'image/*']) ?>

    <?php ActiveForm::end() ?>

<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
    "size" => "modal-lg",
    "options" => [
        "open.bs.modal" => "function(){ console.log('123'); }",
        "tabindex" => false,
    ],
]) ?>
<?php Modal::end(); ?>


<?php
$path = \yii\helpers\Url::toRoute(['users/add-profile-photo'], true);
$pathDoc = \yii\helpers\Url::toRoute(['users/add-documents-photo'], true);

$script = <<< JS
$(document).ready(function() {
    console.log("ready!");
    
	$('.popup-gallery').magnificPopup({
	    delegate: 'a',
		type: 'image',
		closeOnContentClick: true,
		mainClass: 'mfp-img-mobile',
		image: {
			verticalFit: true
		},
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1],
			tCounter: '<span class="mfp-counter">%curr% из %total%</span>'
		},
	});
	
    $('a[href="#tab-2"]').click();
    
    $('#test-file-btn').click(function(){
        android.selectFile('{$path}', 0, yii.getCsrfToken());
    });
    
    $('#test-file-btn-multi').click(function(){
        android.selectFile('{$path}', 0, yii.getCsrfToken());
    });
    
    var photosFormSubmited = false;

    $('#photos-gallery').owlCarousel({
        items: 3,
        margin: 10,
        dots: false,
        // itemElement: '.image-item',
        navigation : false, 
        gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1],
			tCounter: '<span class="mfp-counter">%curr% из %total%</span>'
		},
    });
    
    function uploadingPhotos()
    {
       $('#photos-upload, #photos-upload-2, #photos-upload-3').click(function(e){
            e.preventDefault;
            $('#upload-photos-form input').trigger('click');
            var action = '{$path}';
            android.selectFile(action, 0, yii.getCsrfToken()); 
        });
       
        $('#upload-photos-form').unbind('submit');
        
        $('#upload-photos-form input').change(function(){
            $('#upload-photos-form').submit();
        });
        
        $("#upload-photos-form").submit(function(e){
            e.preventDefault();
            var formData = new FormData($(this)[0]);
        
            if(photosFormSubmited == false)
            {
                photosFormSubmited = true;
                 $.ajax({
                    url: $(this).attr('action'),
                    type: 'POST',
                    data: formData,
                    async: false,
                    success: function (data) {
                        $.pjax.reload('#pjax-photos-container');
                       
                        $(document).on('pjax:complete', function(event) {
                            $('#photos-upload, #photos-upload-2, #photos-upload-3').click(function(e){
                                e.preventDefault;
                                $('#upload-photos-form input').trigger('click');
                            });
                        });
                       
                        setTimeout(function(){
                            photosFormSubmited = false;
                        }, 1000);
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });       
            }
            return false;
        });
        
        $('#documents-upload').click(function(e){
            e.preventDefault;
            $('#upload-documents-form input').trigger('click');
            var action = '{$pathDoc}';
            android.selectFile(action, 0, yii.getCsrfToken()); 
        });
        
        $('#upload-documents-form').unbind('submit');
        
        $('#upload-documents-form input').change(function(){
            $('#upload-documents-form').submit();
        });
        
        $("#upload-documents-form").submit(function(e){
            e.preventDefault();
            var formData = new FormData($(this)[0]);
        
            $.ajax({
                url: $(this).attr('action'),
                type: 'POST',
                data: formData,
                async: false,
                success: function (data) {
                    $.pjax.reload('#pjax-documents-container');
                  
                    $(document).on('pjax:complete', function(event) {
                        $('#documents-upload').click(function(e){
                            e.preventDefault;
                            $('#upload-documents-form input').trigger('click');
                        });
                    });
                },
                cache: false,
                contentType: false,
                processData: false
            });
        
            return false;
        });
    }
   
    uploadingPhotos();
    
    //send_exercises
    $('input[type="checkbox"]')
    $(document).on('click','input[type="checkbox"]', function(event){
        console.log("checkbox clicked!");
    });   
    //chat
    $(document).on('change','.field-chat-trainer_id select, .field-chat-client_id select', function(event) {
        
        var chat_form = $(this).closest('#w3');
        console.log('chat_form');
        console.log(chat_form);
        
        var selected_item = $(chat_form).find(":selected");
        console.log('selected_item');
        console.log(selected_item);
        
        var client_id = null;
        var trainer_id = null;
        
        
        var check_client = $('select#chat-client_id');
        var check_trainer = $('select#chat-trainer_id');
        
        if (check_client.length > 0) {
            
            client_id = $(check_client).val();
            console.log('client_id');
            console.log(client_id);
            
            trainer_id = $(chat_form).find('input[name="trainer_id"]').val();
            console.log('trainer_id');
            console.log(trainer_id);
            
        } else if (check_trainer.length > 0) {
            
            trainer_id = $(check_trainer).val();
            console.log('trainer_id');
            console.log(trainer_id);
            
            client_id = $(chat_form).find('input[name="client_id"]').val();
            console.log('client_id');
            console.log(client_id);
            
        }
        
        $.ajax({
                url: '/chat/chat-by-user',
                data: {
                    trainer_id: trainer_id,
                    client_id: client_id,
                },
                async: false,
                // type: 'json',
                type: 'get',
                success: function (chat) {
                    console.log('chat');
                    console.log(chat);
                    //
                    var chats = JSON.parse(chat);
                    console.log('chats');
                    console.log(chats);
                    for(key in chats){
                        var chat = chats[key];
                        var message = chat.message;
                        var whose_message = chat.whose_message;
                        var trainer_image = chat.trainer_image;
                        var created_date = chat.created_date;
                       
                        // console.log('message');
                        // console.log(message);
                        // console.log('whose_message');
                        // console.log(whose_message);
                        // console.log('trainer_image');
                        // console.log(trainer_image);
                        // console.log('created_date');
                        // console.log(created_date);
                       
                        if (whose_message === 'Тренер') {
                            // <img class="msg_img" src="/web/' + trainer_image + '" alt="sunil"> \
                            var element =
                            '<div class="incoming_msg"> \
                                <div class="incoming_msg_img"> \
                                    <img class="msg_img" src="' + trainer_image + '" alt="sunil"> \
                                </div> \
                                <div class="received_msg"> \
                                    <div class="received_withd_msg"> \
                                        <p> \
                                            ' +  message + ' \
                                        </p> \
                                        <span class="time_date">' +  created_date + '</span></div> \
                                </div> \
                            </div>';
                            $('.msg_history').append(element);
                         } else if (whose_message === 'Клиент') {
                            var element =
                            '<div class="outgoing_msg"> \
                                <div class="sent_msg"> \
                                    <p> \
                                        ' +  message + ' \
                                    </p> \
                                    <span class="time_date">' +  created_date + '</span></div> \
                            </div>';
                            $('.msg_history').append(element);
                         }
                    }
                }, 
                fail: function(data) {
                   console.log('error data');
                    console.log(data);
                }
        });
        
    });

});
JS;
$this->registerJs($script);
?>