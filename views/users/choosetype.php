<?php
use yii\helpers\Html;
//use yii\bootstrap\ActiveForm;
use yii\widgets\ActiveForm;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Выбор типа профиля';
$script = <<< JS
    $(document).ready(function() {
        $('#pills-trener-tab').on('click', function (e) {
		  $('#choosetypeform-permission').val('trener');
		});
        $('#pills-client-tab').on('click', function (e) {
		  $('#choosetypeform-permission').val('client');
		});
    });         
JS;
$this -> registerJS($script,View::POS_READY);
?>


<h1><a class="back_arrow" href="<?=Yii::$app->homeUrl?>"></a><?=$this->title?></h1>
<div class="changetype-box-body">
    <?php $form = ActiveForm::begin(['id' => 'choosetype-form', 'enableClientValidation' => false]); ?>
    <p>Какой тип профиля вы хотите зарегистрировать?</p>
	<ul class="nav nav-pills mb-3 nav-justified" id="pills-tab" role="tablist">
	  <li class="nav-item">
	    <a class="nav-link active" id="pills-client-tab" data-toggle="pill" href="#pills-client" role="tab" aria-controls="pills-client" aria-selected="true">Клиент</a>
	  </li>
	  <li class="nav-item">
	    <a class="nav-link" id="pills-trener-tab" data-toggle="pill" href="#pills-trener" role="tab" aria-controls="pills-trener" aria-selected="false">Тренер</a>
	  </li>
	</ul>
	<div class="tab-content" id="pills-tabContent">
		<!-- show active -->
	  <div class="tab-pane fade " id="pills-client" role="tabpanel" aria-labelledby="pills-client-tab"></div>
	  <div class="tab-pane fade" id="pills-trener" role="tabpanel" aria-labelledby="pills-trener-tab">
	  	Если у вас есть промо-код,<br> введите его здесь
	  	<?= $form
        ->field($model, 'code')
        ->label(false)->widget(\yii\widgets\MaskedInput::className(), [
		    'mask' => '9-9-9-9-9-9','options'=>['placeholder' => 'X-X-X-X-X-X']
		]) ?>
	  </div>
	</div>
	
    <?= $form
        ->field($model, 'permission')
        ->label(false)
        ->hiddenInput() ?>

    <?= Html::submitButton('Завершить регистрацию', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
	
    <?php ActiveForm::end(); ?>
		
</div>

