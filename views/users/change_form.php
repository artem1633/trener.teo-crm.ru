<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */

if (!file_exists('avatars/'.$model->photo) || $model->photo == '') {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/nouser.jpg';
} else {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/'.$model->photo;
}

?>

<div class="users-form" style="padding: 20px;">

    <?php $form = ActiveForm::begin([ 'options' => ['method' => 'post', 'enctype' => 'multipart/form-data']]); ?>

    <div class="row">
        <div class="col-md-4 col-xs-12">
            <center>
            <?=Html::img($path, [
                'style' => 'width:180px; height:180px;',
                'class' => 'img-circle',
            ])?>
            </center>
        </div>
        <div class="col-md-4 col-xs-6">
            <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4 col-xs-6">
            <?= $form->field($model, 'permission')->dropDownList($model->getRoleList(), ['prompt' => 'Выберите должность', 'disabled' => true ]) ?>
        </div>
        <div class="col-md-4 col-xs-6">
            <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4 col-xs-6">
            <?= $model->isNewRecord ? $form->field($model, 'password')->textInput(['maxlength' => true]) : $form->field($model, 'new_password')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'file')->fileInput(); ?>
        </div>
        <div class="col-md-4 col-xs-6">
            <?= $form->field($model, 'birthday')->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => 'Выберите дату',],
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                            'startView'=>'year',
                            //'minViewMode'=>'years',
                            'format' => 'yyyy-mm-dd'
                    ]
                ]);
                ?>  
        </div>
        <div class="col-md-4 col-xs-6">
            <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'training_experience')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'fitnes')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'training_goal')->textInput(['maxlength' => true]) ?>
        </div>
        
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'information')->textArea(['rows' => 4]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'occupation')->textArea(['rows' => 4]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'address')->textArea(['rows' => 4]) ?>
        </div>
    </div>

    <div class="row">
    </div>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
