<?php

namespace app\commands;

use app\models\Coaching;
use app\models\Exercise;
use app\models\ExerciseGroup;
use app\models\Users;
use yii\console\Controller;
use yii\db\Expression;
use yii\helpers\Console;
use yii\web\JsExpression;

/**
 * Class UsersController
 * @package app\commands
 */
class UsersController extends Controller
{
    /**
     * Ставит загдушку для фото всем пользотелям у которых нету изображения аватара
     */
    public function actionSetNoUsersPhotos()
    {
        $count = Users::updateAll(['photo' => Users::NO_USER_PHOTO_PATH], ['photo' => null]);

        $message = "$count users updated";
        echo "$message";

        return 0;
    }

    public function actionClearPhotos()
    {
        $count = Users::updateAll(['photos' => ''], ['is not', 'photos', null]);

        $message = "$count users updated";
        echo "$message";

        return 0;
    }

    public function actionDeleteTreners()
    {
        $coaching = Coaching::find()->all();
        foreach ($coaching as $c){
            $c->delete();
        }

        $exercise = Exercise::find()->all();
        $exerciseCount = 0;
        foreach ($exercise as $ex){
            $exerciseCount++;
            $ex->delete();
        }

        $exerciseGroup = ExerciseGroup::find()->all();
        foreach ($exerciseGroup as $ex){
            $ex->delete();
        }

        $users = Users::find()->where(['permission' => Users::USER_ROLE_TRENER])->all();
        $trenersCount = 0;
        foreach ($users as $user) {
            $trenersCount++;
            $user->delete();
        }

        $message = "{$trenersCount} users deleted | {$exerciseCount} exercises deleted";
        echo "$message";

        return 0;
    }
}