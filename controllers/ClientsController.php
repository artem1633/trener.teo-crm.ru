<?php

namespace app\controllers;

use app\models\ClientsRequests;
use app\models\Directory;
use app\models\Users;
use php_rutils\RUtils;
use Yii;
use app\models\UsersSearch;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * ClientsController
 */
class ClientsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Users models.
     * @throws HttpException
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['permission' => Users::USER_ROLE_CLIENT]);

        $this->view->params['header-after'] = [
            ['label' => 'Все клиенты', 'active' => true, 'url' => Url::toRoute(['index'])],
            ['label' => 'Мои клиенты', 'active' => false, 'url' => Url::toRoute(['index-my'])],
        ];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Users models.
     * @throws HttpException
     * @return mixed
     */
    public function actionIndexMy()
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->searchMyClients(Yii::$app->request->queryParams);

        $this->view->params['header-after'] = [
            ['label' => 'Все клиенты', 'active' => false, 'url' => Url::toRoute(['index'])],
            ['label' => 'Мои клиенты', 'active' => true, 'url' => Url::toRoute(['index-my'])],
        ];

        $this->view->params['header-action-button'] = Html::a('<i class="fa fa-plus"></i>', ['create']);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Получаем список запросов клиентов
     */
    public function actionRequests()
    {
        /** @var Users $user */
        $user = Yii::$app->user->identity;
        $user->disableMarker('client_requests_trainer');

        $requests = ClientsRequests::find()->where(['trainer_id' => $user->id, 'accepted' => null])->with('client')->orderBy('created_at desc')->all();

        return $this->render('requests', [
            'requests' => $requests,
        ]);
    }

    /**
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     * @return array
     */
    public function actionAccept()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if(isset($_POST['client_id']) == false || isset($_POST['trainer_id']) == false){
            throw new BadRequestHttpException();
        }
        $client_id = $_POST['client_id'];
        $trainer_id = $_POST['trainer_id'];
        $request = ClientsRequests::findOne(['client_id' => $client_id, 'trainer_id' => $trainer_id]);
        if($request == null) {
            throw new NotFoundHttpException();
        }

        $result = $request->accept();

        return ['result' => $result];
    }

    /**
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     * @return array
     */
    public function actionRefuse()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if(isset($_POST['client_id']) == false || isset($_POST['trainer_id']) == false){
            throw new BadRequestHttpException();
        }
        $client_id = $_POST['client_id'];
        $trainer_id = $_POST['trainer_id'];
        $request = ClientsRequests::findOne(['client_id' => $client_id, 'trainer_id' => $trainer_id]);
        if($request == null) {
            throw new NotFoundHttpException();
        }

        $result = $request->refuse();

        return ['result' => $result];
    }

    /**
     * Creates a new Users model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Users(['trener' => Yii::$app->user->getId()]);
        $number = Users::getLastId();
        $targetDir = Directory::createProductsDirectory($number);
        $role = Users::getRole();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){

                $last_id = $model->getDb()->lastInsertID;
                $userRole = Yii::$app->authManager->getRole($model->permission);
                Yii::$app->authManager->assign($userRole, $last_id);

                $this -> actionFilesUpload($model);

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Пользователи",
                    'size' => 'normal',
                    'content'=>'<span class="text-success">Успешно выполнено</span>',
                    'footer'=> Html::button('Ок',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::a('Создать ещё',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'number' => $number,
                        'role' => $role,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            if ($model->load($request->post()) && $model->save()) {
                if($_POST['client_year'] != null && $_POST['client_month'] != null){
                    $client_year = intval($_POST['client_year'] == null ? 0 :$_POST['client_year']);
                    $client_month = intval($_POST['client_month'] == null ? 0 :$_POST['client_month']);
                    $model->training_experience = $client_year.' '.RUtils::numeral()->choosePlural($client_year, ['год', 'года', 'лет']).' и '.
                        $client_month.' '.RUtils::numeral()->choosePlural($client_month, ['месяц', 'месяца', 'месяцев']);
                    $model->save(false);
                }
                return $this->redirect(['users/profile', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'role' => $role,
                ]);
            }
        }

    }
}
