<?php

namespace app\controllers;

use app\models\CoachingSearch;
use app\models\CoachingUsers;
use app\models\Feedback;
use app\models\form\UploadForm;
use app\models\Nutritions;
use app\models\NutritionsUsers;
use app\values\Country;
use php_rutils\RUtils;
use Yii;
use app\models\Users;
use app\models\UsersSearch;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;
use app\models\Directory;
use app\models\ExerciseSearch;
use app\models\TrainingSearch;
use app\models\Functions;
use app\models\ChoosetypeForm;
use yii2mod\rbac\filters\AccessControl;

/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout', 'index'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
//                        'roles' => ['administrator','trener'],
                    ],
                    [
                        'actions' => ['choosetype'],
                        'allow' => '@',
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->identity->permission != Users::USER_ROLE_ADMIN) {
            return $this->redirect(['profile', 'id' => Yii::$app->user->getId()]);
        }

        $searchModel = new UsersSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @test
     */
    public function actionTest()
    {
        var_dump(Yii::$app->devicedetect->isiOS());

    }

    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => 'ФИО : ' . $this->findModel($id)->fio,
                'size' => 'normal',
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Изменить', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     */
    public function actionProfile($id)
    {
        $request = Yii::$app->request;
        $this_user = Users::find()->where(['id' => $id])->one();
        $this_permission = $this_user->permission;
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->searchByClient(Yii::$app->request->queryParams, $id);

        $exerciseSearchModel = new ExerciseSearch();
        $ordersdataProvider = $exerciseSearchModel->searchByUser(Yii::$app->request->queryParams, $id);


        $role = Users::getRole();
        $trainingSearchModel = new CoachingSearch();
        $coachDataProvider = $trainingSearchModel->search(Yii::$app->request->queryParams);

        $ratings = Feedback::find()
            ->where(['trainer_id' => $id])
            ->all();

        $chat = null;

        return $this->render('profile', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'ordersdataProvider' => $ordersdataProvider,
            'coachDataProvider' => $coachDataProvider,
//            'chatDataProvider' => $chatDataProvider,
            'chat' => $chat,
            'ratings' => $ratings,
            'role' => $role,
//            'training' => $training,
        ]);
    }

    public function actionSetAvatar()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;


        if(Yii::$app->devicedetect->isiOS() == false){
            $fileName = strval($_FILES['UploadForm']['name']);
            unset($_FILES['UploadForm']['name']);
            ArrayHelper::setValue($_FILES,'UploadForm.name.imageFile', [0 => $fileName]);

            $fileType = strval($_FILES['UploadForm']['type']);
            unset($_FILES['UploadForm']['type']);
            ArrayHelper::setValue($_FILES,'UploadForm.type.imageFile', [0 => $fileType]);

            $tmpFile = strval($_FILES['UploadForm']['tmp_name']);
            unset($_FILES['UploadForm']['tmp_name']);
            ArrayHelper::setValue($_FILES,'UploadForm.tmp_name.imageFile', [0 => $tmpFile]);

            $fileError = intval($_FILES['UploadForm']['error']);
            unset($_FILES['UploadForm']['error']);
            ArrayHelper::setValue($_FILES,'UploadForm.error.imageFile', [0 => $fileError]);

            $size = intval($_FILES['UploadForm']['size']);
            unset($_FILES['UploadForm']['size']);
            ArrayHelper::setValue($_FILES,'UploadForm.size.imageFile', [0 => $size]);

            $uploadModel = new UploadForm(['uploadPath' => 'uploads/avatars/', 'imageFile' => $_FILES['UploadForm']]);
        } else {
            $uploadModel = new UploadForm(['uploadPath' => 'uploads/avatars/']);
        }


//        $uploadModel = new UploadForm(['uploadPath' => 'uploads/avatars/', 'imageFile' => $_FILES['UploadForm']]);

        if (Yii::$app->request->isPost) {
            $uploadModel->imageFile = UploadedFile::getInstances($uploadModel, 'imageFile');
            if ($uploadModel->upload()) {
                // file is uploaded successfully

                $user = Yii::$app->user->identity;
                $user->photo = $uploadModel->fullUploadedFilePath[0];
                $user->save(false);

                return ['path' => $uploadModel->fullUploadedFilePath[0]];
            }


            return ['errors' => $uploadModel->errors];
        }
    }

//    public function actionTestFile()
//    {
//        $format = [];
//        $format['UploadForm'] = [
//            'name' => 'IMG-20190305-WA0003.jpg',
//            'type' => 'multipart/form-data',
//            'tmp_name' => '/tmp/php1jrnTb',
//            'error' => 0,
//            'size' => 160318,
//        ];
//
//        $fileName = strval($format['UploadForm']['name']);
//        unset($format['UploadForm']['name']);
//        ArrayHelper::setValue($format,'UploadForm.name.imageFile', [0 => $fileName]);
//
//        $fileType = strval($format['UploadForm']['type']);
//        unset($format['UploadForm']['type']);
//        ArrayHelper::setValue($format,'UploadForm.type.imageFile', [0 => $fileType]);
//
//        $tmpFile = strval($format['UploadForm']['tmp_name']);
//        unset($format['UploadForm']['tmp_name']);
//        ArrayHelper::setValue($format,'UploadForm.tmp_name.imageFile', [0 => $tmpFile]);
//
//        $fileError = intval($format['UploadForm']['error']);
//        unset($format['UploadForm']['error']);
//        ArrayHelper::setValue($format,'UploadForm.error.imageFile', [0 => $fileError]);
//
//        $size = intval($format['UploadForm']['size']);
//        unset($format['UploadForm']['size']);
//        ArrayHelper::setValue($format,'UploadForm.size.imageFile', [0 => $size]);
//
//        echo "<br>";
//
//        var_dump($format['UploadForm']);
//    }

    public function actionAddProfilePhoto()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        // $fileName = strval($_FILES['UploadForm']['name']);
        // unset($_FILES['UploadForm']['name']);
        // ArrayHelper::setValue($_FILES,'UploadForm.name.imageFile', [0 => $fileName]);

        // $fileType = strval($_FILES['UploadForm']['type']);
        // unset($_FILES['UploadForm']['type']);
        // ArrayHelper::setValue($_FILES,'UploadForm.type.imageFile', [0 => $fileType]);

        // $tmpFile = strval($_FILES['UploadForm']['tmp_name']);
        // unset($_FILES['UploadForm']['tmp_name']);
        // ArrayHelper::setValue($_FILES,'UploadForm.tmp_name.imageFile', [0 => $tmpFile]);

        // $fileError = intval($_FILES['UploadForm']['error']);
        // unset($_FILES['UploadForm']['error']);
        // ArrayHelper::setValue($_FILES,'UploadForm.error.imageFile', [0 => $fileError]);

        // $size = intval($_FILES['UploadForm']['size']);
        // unset($_FILES['UploadForm']['size']);
        // ArrayHelper::setValue($_FILES,'UploadForm.size.imageFile', [0 => $size]);



        $uploadModel = new UploadForm(['uploadPath' => 'uploads/avatars/', 'imageFile' => $_FILES['UploadForm']]);

        if (Yii::$app->request->isPost) {
            $uploadModel->imageFile = UploadedFile::getInstances($uploadModel, 'imageFile');
            if ($uploadModel->upload()) {
                // file is uploaded successfully

                $user = Yii::$app->user->identity;
                $userPhotos = json_decode($user->photos);
                if($userPhotos != null)
                {
                    $userPhotos = array_merge($userPhotos, $uploadModel->fullUploadedFilePath);
                } else {
                    $userPhotos = $uploadModel->fullUploadedFilePath;
                }
                $user->photos = json_encode($userPhotos);
                if($user->save(false)){
                    Yii::debug('Успешно сохранен');
                } else {
                    Yii::debug('Ошибка при сохранении модели'.$user->errors[0]);
                }

                return ['paths' => $uploadModel->fullUploadedFilePath];
            }

            return ['errors' => $uploadModel->errors];
        }
    }

    public function actionAddDocumentsPhoto()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

//        $fileName = strval($_FILES['UploadForm']['name']);
//        unset($_FILES['UploadForm']['name']);
//        ArrayHelper::setValue($_FILES,'UploadForm.name.imageFile', [0 => $fileName]);
//
//        $fileType = strval($_FILES['UploadForm']['type']);
//        unset($_FILES['UploadForm']['type']);
//        ArrayHelper::setValue($_FILES,'UploadForm.type.imageFile', [0 => $fileType]);
//
//        $tmpFile = strval($_FILES['UploadForm']['tmp_name']);
//        unset($_FILES['UploadForm']['tmp_name']);
//        ArrayHelper::setValue($_FILES,'UploadForm.tmp_name.imageFile', [0 => $tmpFile]);
//
//        $fileError = intval($_FILES['UploadForm']['error']);
//        unset($_FILES['UploadForm']['error']);
//        ArrayHelper::setValue($_FILES,'UploadForm.error.imageFile', [0 => $fileError]);
//
//        $size = intval($_FILES['UploadForm']['size']);
//        unset($_FILES['UploadForm']['size']);
//        ArrayHelper::setValue($_FILES,'UploadForm.size.imageFile', [0 => $size]);

        $uploadModel = new UploadForm(['uploadPath' => 'uploads/avatars/', 'imageFile' => $_FILES['UploadForm']]);

        if (Yii::$app->request->isPost) {
            $uploadModel->imageFile = UploadedFile::getInstances($uploadModel, 'imageFile');
            if ($uploadModel->upload()) {
                // file is uploaded successfully

                $user = Yii::$app->user->identity;
                $userPhotos = json_decode($user->education_scans);
                if($userPhotos != null)
                {
                    $userPhotos = array_merge($userPhotos, $uploadModel->fullUploadedFilePath);
                } else {
                    $userPhotos = $uploadModel->fullUploadedFilePath;
                }
                $user->education_scans = json_encode($userPhotos);
                $user->save(false);

                return ['paths' => $uploadModel->fullUploadedFilePath];
            }

            return ['errors' => $uploadModel->errors];
        }
    }

    public function actionUploadFormImage()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $uploadModel = new UploadForm(['uploadPath' => 'uploads/avatars/', 'imageFile' => $_FILES['file']]);

        if (Yii::$app->request->isPost) {
            $uploadModel->imageFile = UploadedFile::getInstance($uploadModel, 'imageFile');
            if ($uploadModel->upload()) {
                // file is uploaded successfully
                return ['path' => $uploadModel->fullUploadedFilePath];
            }

            return ['errors' => $uploadModel->errors];
        }
    }

    public function actionTrClient($id)
    {
        $request = Yii::$app->request;
        $this_user = Users::find() -> where(['id' => $id]) -> one();
        $this_permission = $this_user -> permission;
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->searchByClient(Yii::$app->request->queryParams, $id);

        $exerciseSearchModel = new ExerciseSearch();
        $ordersdataProvider = $exerciseSearchModel->searchByUser(Yii::$app->request->queryParams,$id);

        $role = Users::getRole();
        $trainingSearchModel = new CoachingSearch();
        $coachDataProvider = $trainingSearchModel->search(Yii::$app->request->queryParams);

        $trSearchModel = new TrainingSearch();
        $trainingDataProvider = $trSearchModel->searchByUser(Yii::$app->request->queryParams,$id);


        $ratings = Feedback::find()
            -> where(['trainer_id' => $id])
            -> all();

        $chat = null;


        return $this->render('tr_client', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'ordersdataProvider' => $ordersdataProvider,
            'coachDataProvider' => $coachDataProvider,
            'trainingDataProvider' => $trainingDataProvider,
            'chat' => $chat,
            'ratings' => $ratings,
            'role' => $role,
        ]);
    }

    public function actionClientProfile($id)
    {
        $request = Yii::$app->request;
        $this_user = Users::find() -> where(['id' => $id]) -> one();
        $this_permission = $this_user -> permission;
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->searchByClient(Yii::$app->request->queryParams, $id);

        $exerciseSearchModel = new ExerciseSearch();
        $ordersdataProvider = $exerciseSearchModel->searchByUser(Yii::$app->request->queryParams,$id);


//        $trainingSearchModel = new TrainingSearch();
//        $coachDataProvider = $trainingSearchModel->searchByCoach(Yii::$app->request->queryParams,$id);

        $trainingSearchModel = new CoachingSearch();
        $coachDataProvider = $trainingSearchModel->search(Yii::$app->request->queryParams);

        $ratings = Feedback::find()
            -> where(['trainer_id' => $id])
            -> all();

        $chat = null;

//        if ($this_permission != 'trainer') {
//            $chatSearchModel = new ChatSearch();
//            if (Functions::getUserRole(Yii::$app->user->identity->id) == "Клиент") {
//
//            }
////            $chat = $chatSearchModel->searchByChat(Yii::$app->user->identity->id);
//
////            $chat = Chat::find()->where(['trainer_id' => $id]) -> all();
//
////            Yii::info('chat_', 'file');
////            Yii::info($chat, 'file');
//
//        } else {
//
//        }

        return $this->render('client_profile', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'ordersdataProvider' => $ordersdataProvider,
            'coachDataProvider' => $coachDataProvider,
//            'chatDataProvider' => $chatDataProvider,
            'chat' => $chat,
            'ratings' => $ratings,
//            'training' => $training,
        ]);
    }


    /**
     * Creates a new Users model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Users(); 
        $number = Users::getLastId(); 
        $targetDir = Directory::createProductsDirectory($number);
        $role = Users::getRole();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){
                
                $last_id = $model->getDb()->lastInsertID;
                $userRole = Yii::$app->authManager->getRole($model->permission);
                Yii::$app->authManager->assign($userRole, $last_id);
                
                $this -> actionFilesUpload($model);

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Пользователи",
                    'size' => 'normal',
                    'content'=>'<span class="text-success">Успешно выполнено</span>',
                    'footer'=> Html::button('Ок',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать ещё',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'number' => $number,
                        'role' => $role,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            if ($model->load($request->post()) && $model->save()) {
                if(!empty($_POST['trainer_year']) && !empty($_POST['trainer_month'])){
                    $trainer_year = intval($_POST['trainer_year'] == null ? 0 :$_POST['trainer_year']);
                    $trainer_month = intval($_POST['trainer_month'] == null ? 0 :$_POST['trainer_month']);
                    $model->work_experience = $trainer_year.' '.RUtils::numeral()->choosePlural($trainer_year, ['год', 'года', 'лет']).' и '.
                        $trainer_month.' '.RUtils::numeral()->choosePlural($trainer_month, ['месяц', 'месяца', 'месяцев']);
                    $model->save(false);
                }
                if(!empty($_POST['client_year']) && !empty($_POST['client_month'])){
                    $client_year = intval($_POST['client_year'] == null ? 0 :$_POST['client_year']);
                    $client_month = intval($_POST['client_month'] == null ? 0 :$_POST['client_month']);
                    $model->training_experience = $client_year.' '.RUtils::numeral()->choosePlural($client_year, ['год', 'года', 'лет']).' и '.
                        $client_month.' '.RUtils::numeral()->choosePlural($client_month, ['месяц', 'месяца', 'месяцев']);
                    $model->save(false);
                }
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'role' => $role,
                ]);
            }
        }
    }

    /**
     * @param $id
     * @return string|Response
     */
    public function actionAttachTraining($id)
    {
        $request = Yii::$app->request;
        $model = new CoachingUsers(['users_id' => $id]);

        if($model->load($request->post()) && $model->save())
        {
            return $this->redirect(['profile', 'id' => $id]);
        } else {
            return $this->render('attach-training', [
                'user_id' => $id,
                'model' => $model
            ]);
        }
    }

    /**
     * @param $id
     * @return string|Response
     */
    public function actionAttachNutrition($id)
    {
        $request = Yii::$app->request;
        $model = new NutritionsUsers(['users_id' => $id]);

        if($model->load($request->post()) && $model->save())
        {
            return $this->redirect(['profile', 'id' => $id]);
        } else {
            return $this->render('attach-nutriotion', [
                'user_id' => $id,
                'model' => $model
            ]);
        }
    }

    /**
     * Updates an existing Users model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);     
        $number = $id;
        $only_photo = null;
        if (isset($_GET['only_photo'])) {
            $only_photo = $request -> get('only_photo');
        }

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post())){

                Yii::info('request->post()', 'test');
                Yii::info($request->post(), 'test');

                if($model->permission == Users::USER_ROLE_CLIENT) {

                    $client_year = $request->post('client_year');
                    if (!empty($client_year)) {
                        $client_year =
                            $client_year > 1
                                ? $client_year = $client_year . ' года '
                                : $client_year = $client_year . ' год ';
                    }

                    $client_month = $request->post('client_month');
                    if (!empty($client_month)) {
                        $client_month =
                            $client_month > 1
                                ? $client_month = $client_month . ' месяцев'
                                : $client_month = $client_month . ' месяц';
                    }

                    if (!empty($client_year) && !empty($client_month)) {

                        $model -> training_experience = $client_year . $client_month;

                    } else if (!empty($client_year)) {

                        $model -> training_experience = $client_year;

                    } else if (!empty($client_month)) {

                        $model -> training_experience = $client_month;
                    }
                } else {

                    $trainer_year = $request->post('trainer_year');
                    if (!empty($trainer_year)) {
                        $trainer_year =
                            $trainer_year > 1
                                ? $trainer_year = $trainer_year . ' года '
                                : $trainer_year = $trainer_year . ' год ';
                    }

                    $trainer_month = $request->post('trainer_month');
                    if (!empty($trainer_month)) {
                        $trainer_month =
                            $trainer_month > 1
                                ? $trainer_month = $trainer_month . ' месяцев'
                                : $trainer_month = $trainer_month . ' месяц';
                    }

                    if (!empty($trainer_year) && !empty($trainer_month)) {

                        $model -> work_experience = $trainer_year . $trainer_month;
                    } else if(!empty($trainer_year)) {

                        $model -> work_experience = $trainer_year;
                    } else if(!empty($trainer_month)) {

                        $model -> work_experience = $trainer_month;
                    }

                }

                if ($model->save()) {

                    if (Yii::$app->user->identity->permission == 'trener') {
                        $model -> trener = Yii::$app->user->identity->id;
                    }

                    Yii::info('request->post()', 'file');
                    Yii::info($request->post(), 'file');

                    $model->file = UploadedFile::getInstance($model, 'file');

                    if(!empty($model->file)) {

                        if (!empty($model -> photo)) {
                            Yii::info('model -> photo', 'file');
                            Yii::info($model -> photo, 'file');

                            if ($model -> photo != null && file_exists($model -> photo)) {
                                unlink(Yii::getAlias($model -> photo));
                            }
                        }

                        $path = 'avatars/';
                        $current_name = $path.time(). '_' .Functions::generateRandomString(5) . '.' . $model->file->extension;

                        if ($model->file->saveAs($current_name)) {

                            Yii::info('file', 'file');
                            Yii::info('avatar uploaded!', 'file');

                            Yii::$app->db->createCommand()->update('users',
                                [
                                    'photo' => $current_name
                                ],
                                [
                                    'id' => $model->id
                                ]
                            )->execute();
                        }
                    }

                    $model->files = UploadedFile::getInstances($model, 'files');

                    Yii::info('model->files', 'file');
                    Yii::info($model->files, 'file');

                    if(!empty($model->files)) {

                        $photos_from_db = $model -> photos;
                        $photos_from_db = json_decode($photos_from_db, true);

                        $path = 'images/';
                        $photos = [];
                        foreach ($model->files as $photo) {
                            $current_name = $path.time(). '_' .Functions::generateRandomString(5) . '.' . $photo->extension;
                            if ($photo->saveAs($current_name)) {
                                $photos[] = $current_name;
                            }
                        }
                        Yii::info('photos', 'file');
                        Yii::info($photos, 'file');

                        if (!empty($photos_from_db)) {
                            Yii::info('photos_from_db is not empty', 'file');
                            Yii::info($photos_from_db, 'file');
                            $photos = array_merge($photos, $photos_from_db);
                            Yii::info('after merge', 'file');
                            Yii::info($photos, 'file');
                        }

                        Yii::$app->db->createCommand()->update('users',
                            [
                                'photos' => json_encode($photos, true)
                            ],
                            [
                                'id' => $model->id
                            ]
                        )->execute();
                    }

                    return [
                        'forceReload'=>'#profile-pjax',
                        'size' => 'large',
                        'title'=> 'ФИО : ' . $model->fio,
                        'content'=>$this->renderAjax('view', [
                            'model' => $model,
                        ]),
                        'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                    ];
                } else {
                    if (!empty($only_photo)) {
                        return [
                            'forceReload'=>'#profile-pjax',
                            'size' => 'large',
                            'title'=> 'Фото',
                            'content'=>$this->renderAjax('update_photo', [
                                'model' => $model,
                            ]),
                            'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                        ];
                    } else
                        return [
                            'title'=> "Изменить",
                            'size' => 'large',
                            'content'=>$this->renderAjax('update', [
                                'model' => $model,
                                'number' => $number,
                            ]),
                            'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                        ];
                }

            }else{
                if (!empty($only_photo)) {
                    return [
                        'forceReload'=>'#profile-pjax',
                        'size' => 'large',
                        'title'=> 'Фото',
                        'content'=>$this->renderAjax('update_photo', [
                            'model' => $model,
                        ]),
                        'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                    ];
                } else
                 return [
                    'title'=> "Изменить",
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                        'number' => $number,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            if ($model->load($request->post()) && $model->save()) {
                $trainerData = [];
                if(!empty($_POST['trainer_year'])){
                    $trainer_year = intval($_POST['trainer_year'] == null ? 0 :$_POST['trainer_year']);
                    $trainerData[] = $trainer_year.' '.RUtils::numeral()->choosePlural($trainer_year, ['год', 'года', 'лет']);
                }
                if(!empty($_POST['trainer_month'])){
                    $trainer_month = intval($_POST['trainer_month'] == null ? 0 :$_POST['trainer_month']);
                    $trainerData[] = $trainer_month.' '.RUtils::numeral()->choosePlural($trainer_month, ['месяц', 'месяца', 'месяцев']);;
                }
                if(count($trainerData) > 0){
                    $model->work_experience = implode(' и ', $trainerData);
                    $model->save(false);
                }

                $clientData = [];
                if(!empty($_POST['client_year'])){
                    $client_year = intval($_POST['client_year'] == null ? 0 :$_POST['client_year']);
                    $clientData[] = $client_year.' '.RUtils::numeral()->choosePlural($client_year, ['год', 'года', 'лет']);
                }
                if(!empty($_POST['client_month'])){
                    $client_month = intval($_POST['client_month'] == null ? 0 :$_POST['client_month']);
                    $clientData[] = $client_month.' '.RUtils::numeral()->choosePlural($client_month, ['месяц', 'месяца', 'месяцев']);
                }
                if(count($clientData) > 0){
                    $model->training_experience = implode(' и ', $clientData);
                    $model->save(false);
                }

                return $this->redirect(['profile', 'id' => $model->id]);
            } else {
                if (!empty($only_photo)) {
                    return $this->render('update_photo', [
                        'model' => $model,
                    ]);
                } else
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionChange($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       
        Yii::$app->response->format = Response::FORMAT_JSON;

        if($model->load($request->post()) && $model->save())
        {
            $model->file = UploadedFile::getInstance($model, 'file');
            if(!empty($model->file))
            {
                $model->file->saveAs('avatars/'.$model->file->baseName.'.'.$model->file->extension);
                Yii::$app->db->createCommand()->update('users', ['photo' => $model->file->baseName.'.'.$model->file->extension], [ 'id' => $model->id ])->execute();
            }

            return [
                'forceReload'=>'#crud-datatable-pjax',
                'title'=> "Профиль",
                'forceClose'=>true,                     
            ];    
        }
        else
        {
            return [
                'title'=> "Профиль",
                'content'=>$this->renderAjax('change_form', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
            ];        
        }
    }

    public function actionDeleteDocument()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $post = Yii::$app->request->post();

        if($post['id'] == null || $post['photo_path'] == null){
            throw new BadRequestHttpException('Пустые параметры');
        }

        var_dump($post['photo_path']);

        $user = Users::findOne($post['id']);

        if($user == null){
            throw new BadRequestHttpException('Пользователь не найден');
        }

        if($post['photo_path'])
        {
            $photos = json_decode($user->education_scans);
            $photos = array_filter($photos, function($photo) use($post){
                return $photo != $post['photo_path'];
            });
            $user->education_scans = json_encode($photos);
        }

        $user->save(false);

        unlink($post['path']);
        return ['result' => 1];
    }

    /**
     * Delete an existing Users model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $data = json_decode($this->findModel($id)->photos, true);
        foreach ($data as $value) {
            if (file_exists($value))  unlink(Yii::getAlias($value));
        }
        $photo = $this->findModel($id)->photo;
        foreach ($data as $value) {
            if (file_exists($photo))  unlink(Yii::getAlias($photo));
        }
        
        if($id != 1) $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

     /**
     * Delete multiple existing Users model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            
            if($pk != 1){                
                $model = $this->findModel($pk);
                $model->delete();
            }
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionFileUpload($model_id)
    {
        $path = 'uploads/doc_scans';

        if (isset($_FILES['doc_scans'])) {
            $file = UploadedFile::getInstanceByName('doc_scans');
            $current_name = $path.time(). '_' .Functions::generateRandomString(5) . '.' . $file->extension;
            // you can write save code here before uploading.
            if ($file->saveAs($current_name)) {
//                $update = Yii::$app->db->createCommand()->update(
//                    'users',
//                    ['education_scans' => json_encode($new_education_scans, true)],
//                    ['id' => $model_id]
//                )->execute();

//                return $update;
            }
        } else if (isset($_FILES['file'])) {

            $file = UploadedFile::getInstanceByName('file');
            $current_name = $path.time(). '_' .Functions::generateRandomString(5) . '.' . $file->extension;
            // you can write save code here before uploading.
            if ($file->saveAs($current_name)) {
                $update = Yii::$app->db->createCommand()->update(
                    'users',
                    ['education_scans' => $current_name],
                    ['id' => $model_id]
                )->execute();

                return $update;
            }
        }
        else {
            return "-";
        }
    }

    public function actionFilesUpload($model)
    {

        $model->file = UploadedFile::getInstance($model, 'file');
        Yii::info('file', 'file');
        Yii::info($model->file, 'file');

        $model->files = UploadedFile::getInstances($model, 'files');
        Yii::info('files', 'file');
        Yii::info($model->files, 'file');

        $model->doc_scans = UploadedFile::getInstances($model, 'doc_scans');
        Yii::info('doc_scans', 'file');
        Yii::info($model->doc_scans, 'file');

        if(!empty($model->file)) {
            $path = 'avatars/';
            $current_name = $path.time(). '_' .Functions::generateRandomString(5) . '.' . $model->file->extension;

            if ($model->file->saveAs($current_name)) {

                Yii::info('file', 'file');
                Yii::info('avatar uploaded!', 'file');

                Yii::$app->db->createCommand()->update('users',
                    [
                        'photo' => $current_name
                    ],
                    [
                        'id' => $model->id
                    ]
                )->execute();
            }
        }
        if(!empty($model->files)) {
            $path = 'images/';
            $photos = [];
            foreach ($model->files as $file) {
                $current_name = $path.time(). '_' .Functions::generateRandomString(5) . '.' . $file->extension;
                if ($file->saveAs($current_name)) {
                    $photos[] = $current_name;
                }
            }
            Yii::info('file', 'file');
            Yii::info($photos, 'file');
            Yii::info('uploaded images', 'file');

            Yii::$app->db->createCommand()->update('users',
                [
                    'photos' => json_encode($photos, true)
                ],
                [
                    'id' => $model->id
                ]
            )->execute();
        }
        if(!empty($model->doc_scans)) {
            $path = 'uploads/doc_scans/';
            $education_scans = [];
            foreach ($model->doc_scans as $doc_scan) {
                $current_name = $path.time(). '_' .Functions::generateRandomString(5) . '.' . $doc_scan->extension;
                if ($doc_scan->saveAs($current_name)) {
                    $education_scans[] = $current_name;
                }
            }
            Yii::info('doc_scans', 'file');
            Yii::info('education_scans', 'file');
            Yii::info($education_scans, 'file');

            Yii::$app->db->createCommand()->update('users',
                [
                    'education_scans' => json_encode($education_scans, true)
                ],
                [
                    'id' => $model->id
                ]
            )->execute();
        }

    }

    public function actionScan($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $number = $id;
        $education_scans_from_db = $model -> education_scans;
        $education_scans_from_db = json_decode($education_scans_from_db, true);

        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->load($request->post()) && $model->save()){
            $model->doc_scans = UploadedFile::getInstances($model, 'doc_scans');
            if(!empty($model->doc_scans)) {
                $path = 'uploads/doc_scans/';
                $education_scans = [];
                foreach ($model->doc_scans as $doc_scan) {
                    $current_name = $path.time(). '_' .Functions::generateRandomString(5) . '.' . $doc_scan->extension;
                    if ($doc_scan->saveAs($current_name)) {
                        $education_scans[] = $current_name;
                    }
                }
                Yii::info('doc_scans => education_scans', 'file');
                Yii::info($education_scans, 'file');

                if (!empty($education_scans_from_db)) {
                    Yii::info('education_scans_from_db is not empty', 'file');
                    Yii::info($education_scans_from_db, 'file');
                    $education_scans = array_merge($education_scans, $education_scans_from_db);
                    Yii::info('after merge', 'file');
                    Yii::info($education_scans, 'file');
                }

                Yii::$app->db->createCommand()->update('users',
                    [
                        'education_scans' => json_encode($education_scans, true)
                    ],
                    [
                        'id' => $model->id
                    ]
                )->execute();
            }
            return [
                'forceReload'=>'#profile-pjax',
                'forceClose'=>true,
            ];
        }else{
            return [
                'title'=> "Изменить",
                'size' => 'large',
                'content'=>$this->renderAjax('_form_scan', [
                    'model' => $model,
                    'number' => $number,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
            ];
        }
    }

    public static function actionAdditionalDelete()
    {


        $type = $_POST['type'];
        $deleted_item = $_POST['deleted_item'];
        if ($deleted_item != "")
        Yii::info('deleted_item', 'file');
        Yii::info($deleted_item, 'file');
        $deleted_item = substr($deleted_item, 5, strlen($deleted_item));

        $ok = false;

        $model_id = $_POST['model_id'];
        $model = Users::find() -> where(['id' => $model_id]) -> one();

        if ($type == "education_scans") {

            $education_scans_from_db = $model -> education_scans;
            $education_scans_from_db = json_decode($education_scans_from_db, true);

            Yii::info('education_scans_from_db', 'file');
            Yii::info($education_scans_from_db, 'file');

            if(!empty($education_scans_from_db)) $ok = true;

            $new_education_scans = [];
            if ($ok) {
                foreach ($education_scans_from_db as $education_scan) {
                    if ($education_scan != $deleted_item) {
                        $new_education_scans[] = $education_scan;
                    }
                }
            }

            Yii::info('new_education_scans', 'file');
            Yii::info($new_education_scans, 'file');

            if (!empty($deleted_item)) {

                Yii::info('deleted_item', 'file');
                Yii::info($deleted_item, 'file');

                if ($deleted_item != null && file_exists($deleted_item)) {
                    unlink(Yii::getAlias($deleted_item));
                }
            }
            if (count($new_education_scans) == 0) {
                Yii::$app->db->createCommand()->update(
                    'users',
                    ['education_scans' => null],
                    ['id' => $model_id]
                )->execute();
            } else {
                Yii::$app->db->createCommand()->update(
                    'users',
                    ['education_scans' => json_encode($new_education_scans, true)],
                    ['id' => $model_id]
                )->execute();
            }

        } else if ($type == "photos") {

            $photos_from_db = $model -> photos;
            $photos_from_db = json_decode($photos_from_db, true);

            Yii::info('photos_from_db', 'file');
            Yii::info($photos_from_db, 'file');

            if(!empty($photos_from_db)) $ok = true;

            $new_photos_from = [];
            if ($ok) {
                foreach ($photos_from_db as $photo) {
                    if ($photo != $deleted_item) {
                        $new_photos_from[] = $photo;
                    }
                }
            }

            Yii::info('new_photos_from', 'file');
            Yii::info($new_photos_from, 'file');

            if (!empty($deleted_item)) {

                Yii::info('deleted_item', 'file');
                Yii::info($deleted_item, 'file');

                if ($deleted_item != null && file_exists($deleted_item)) {
                    Yii::info('file unlinked', 'file');
                    unlink(Yii::getAlias($deleted_item));
                }
            }
            if (count($new_photos_from) == 0) {
                Yii::info('files count = 0', 'file');

                Yii::$app->db->createCommand()->update(
                    'users',
                    ['photos' => null],
                    ['id' => $model_id]
                )->execute();
            } else {
                Yii::info('file count', 'file');
                Yii::info(count($new_photos_from), 'file');
                Yii::info($new_photos_from, 'file');
                Yii::$app->db->createCommand()->update(
                    'users',
                    ['photos' => json_encode($new_photos_from, true)],
                    ['id' => $model_id]
                )->execute();
            }
        }
    }

    public function actionTrainerUsers() {

//        $clients = \app\models\Functions::getTrainerClients();
        $id = Yii::$app->user->identity->id;

        $trainer_users = Users::find()
            -> where(['trener' => $id])
            -> andWhere(['permission' => 'client'])
            -> all();

        $searchModel = new UsersSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this -> render('trainer_users', [
//            'dataProvider' => $dataProvider,
            'trainer_users' => $trainer_users
        ]);
    }

    public function actionTrainersList() {

//        $clients = \app\models\Functions::getTrainerClients();
        
        return $this -> render('trener_list');
    }

    public function actionTrenerView() {

        
        return $this -> render('trener_view');
    }

    public function actionChoosetype()
    {
        $model = new ChoosetypeForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->choosetype()) {
                return $this->redirect(['user/profile']);
            }
        }
        return $this->render('choosetype', [
            'model' => $model,
        ]);
    }

    private function fixFilesArray(&$files)
    {
        $names = array( 'name' => 1, 'type' => 1, 'tmp_name' => 1, 'error' => 1, 'size' => 1);

        foreach ($files as $key => $part) {
            // only deal with valid keys and multiple files
            $key = (string) $key;
            if (isset($names[$key]) && is_array($part)) {
                foreach ($part as $position => $value) {
                    $files[$position][$key] = $value;
                }
                // remove old key reference
                unset($files[$key]);
            }
        }
    }
}
