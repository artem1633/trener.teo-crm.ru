<?php

namespace app\controllers;

use app\models\CoachingCompleting;
use app\models\CoachingUsers;
use app\models\Exercise;
use app\models\Users;
use Codeception\Command\Completion;
use Yii;
use app\models\Training;
use app\models\TrainingSearch;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\Coaching;

/**
 * TrainingController implements the CRUD actions for Training model.
 */
class TrainingController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Training models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TrainingSearch();
        $dataProvider = $searchModel->searchCoach(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * @return mixed
     */
    public function actionCoachingsUsers()
    {
        $usersPks = ArrayHelper::getColumn(Users::find()->where(['permission' => 'client', 'trener' => Yii::$app->user->getId()])->all(), 'id');
        $coachingPks = ArrayHelper::getColumn(Coaching::find()->where(['trener_id' => Yii::$app->user->getId()])->all(), 'id');
        $coachingUsers = CoachingUsers::find()->where(['users_id' => $usersPks, 'coaching_id' => $coachingPks])->with('users')->with('coaching')->all();

        return $this->render('coachings-users', [
            'coachingUsers' => $coachingUsers,
        ]);
    }

    public function actionTrainingList()
    {
//        $searchModel = new TrainingSearch();
//        $dataProvider = $searchModel->searchCoach(Yii::$app->request->queryParams);
        $trainings = new Users();
        $trainings = $trainings->getTrainings();
        return $this->render('training-list', [
//            'searchModel' => $searchModel,
            'trainings' => $trainings,
        ]);
    }

    /**
     * @throws BadRequestHttpException
     * @return array
     */
    public function actionSendClientRequest()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if(isset($_POST['trainer_id']) == false){
            throw new BadRequestHttpException();
        }
        $trainer_id = $_POST['trainer_id'];

        /** @var Users $user */
        $user = Yii::$app->user->identity;

        $result = $user->sendRequest($trainer_id);

        return ['result' => $result];
    }

    public function actionClientView()
    {
        return $this->render('client-view', [
//            'trainings' => $trainings,
        ]);
    }

    /**
     * @throws BadRequestHttpException
     * @return array
     */
    public function actionMakeCoachingCompleted()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if(isset($_POST['coaching_id']) == false){
            throw new BadRequestHttpException();
        }
        $coaching_id = $_POST['coaching_id'];
        /** @var Coaching $coaching */
        $coaching = $this->findCoaching($coaching_id);

        $result = $coaching->makeCompleted();

        return ['result' => $result];
    }

    /**
     * Displays a single Training model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findCoaching($id);
        $trainnings = Training::find()->where(['coaching_id' => $model->id])->orderBy('id asc')->all();
        $exercisesIds = array_values(ArrayHelper::map($trainnings, 'id', 'exercise_id'));
        $exercisesData = Exercise::findAll($exercisesIds);
        $exercises = ArrayHelper::map($exercisesData, 'id', 'name');
        $exercisesGroups = ArrayHelper::map($exercisesData, 'id', 'exercise_group_id');
        $exercisesTrainnings = [];

        // Делаем карту {ExerciseId} => Training
        foreach ($exercises as $id => $name)
        {
            foreach ($trainnings as $trainning)
            {
                if($trainning->exercise_id == $id)
                {
                    $exercisesTrainnings[$id] = $trainning;
                }
            }
        }

        /** @var Training[] $coachingTrainings */
        $coachingTrainings = $trainnings;

        $trainingUnions = [];

        foreach ($coachingTrainings as $training){
            if($training->with != null){
                $trainingUnions[$training->with][] = $training->id;
            } else {
                $trainingUnions[$training->id][] = $training->id;
            }
        }

        ksort($exercises);
        // exit;

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Тренировка",
                'size' => 'normal',
                'content' => $this->renderAjax('view', [
                    'model' => $model,
                    'trainnings' => $trainnings,
                    'exercisesGroups' => $exercisesGroups,
                    'exercises' => $exercises,
                    'exercisesTrainnings' => $exercisesTrainnings,
                    'trainingUnions' => $trainingUnions,
                ]),
                'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Изменить', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {

            return $this->render('view', [
                'model' => $model,
                'trainnings' => $trainnings,
                'exercisesGroups' => $exercisesGroups,
                'exercises' => $exercises,
                'exercisesTrainnings' => $exercisesTrainnings,
                'trainingUnions' => $trainingUnions,
            ]);
        }
    }

    /**
     * Creates a new Training model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionAddCoach()
    {
        $request = Yii::$app->request;
        $model = new Coaching();

        if ($model->load($request->post()) && $model->save()) {
            if($request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'forceReload' => '#calls-datatable-pjax',
                    'title' => "Тренировка",
                    'forceClose' => true,
                ];
            } else {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {

            if($request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'title' => "Создать",
                    'size' => 'normal',
                    'content' => $this->renderAjax('add-coach', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else {
                return $this->render('add-coach', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Updates an existing Training model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateCoach($id)
    {
        $request = Yii::$app->request;
        $model = $this->findCoaching($id);

        if ($model->load($request->post()) && $model->save()) {
            if($request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'forceReload' => '#calls-datatable-pjax',
                    'title' => "Тренировка",
                    'forceClose' => true,
                ];
            } else {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {

            if($request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'title' => "Создать",
                    'size' => 'normal',
                    'content' => $this->renderAjax('add-coach', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else {
                return $this->render('add-coach', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Creates a new Training model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Training();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Тренировка",
                    'size' => 'normal',
                    'content' => '<span class="text-success">Успешно выполнено</span>',
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Создать ещё', ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                ];
            } else {
                return [
                    'title' => "Создать",
                    'size' => 'normal',
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * @param int $coaching_id
     * @throws NotFoundHttpException
     * @return mixed
     */
    public function actionAddNewTraining($coaching_id)
    {
       $coaching = $this->findCoaching($coaching_id);
       if($coaching->trener_id != Yii::$app->user->identity->getId()){
           throw new NotFoundHttpException('Тренировка не найдена');
       }

       $model = new Training([
           'trener_id' => Yii::$app->user->getId(),
           'coaching_id' => $coaching_id,
       ]);

       /** @var Training[] $coachingTrainings */
       $coachingTrainings = Training::find()->where(['coaching_id' => $coaching_id])->all();

       $trainingUnions = [];
       $trainingFree = [];

       foreach ($coachingTrainings as $training){
           if($training->with != null){
                $trainingUnions[$training->with][] = $training->exercise_id;
           } else {
               $trainingUnions[$training->id][] = $training->exercise_id;
           }
       }

       if($model->load(Yii::$app->request->post()) && $model->save())
       {
           return $this->redirect(['view', 'id' => $coaching_id]);
       } else {
           return $this->render('add_training', [
               'model' => $model,
               'coaching' => $coaching,
               'coachingTrainings' => $coachingTrainings,
               'trainingUnions' => $trainingUnions,
               // '$trainingFree' => $trainingFree,
           ]);
       }
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function actionUpdateTraining($id)
    {
        $model = $this->findModel($id);
        $coaching = $this->findCoaching($model->coaching_id);

        if($model->load(Yii::$app->request->post()) && $model->save())
        {
            return $this->redirect(['view', 'id' => $coaching->id]);
        } else {
            return $this->render('add_training', [
                'model' => $model,
                'coaching' => $coaching,
            ]);
        }
    }

    /**
     * @param $id
     * @return Response
     */
    public function actionDeleteTraining($id)
    {
        $model = $this->findModel($id);
        $model->delete();
        return $this->redirect(['view', 'id' => $model->coaching_id]);
    }

    /**
     * Creates a new Training model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionAdd($trener_id, $coaching_id = null)
    {
        $request = Yii::$app->request;
        $model = new Training();
        $model->trener_id = $trener_id;
        $model->coaching_id = $coaching_id;

        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($model->load($request->post()) && $model->save()) {
            return [
                'forceReload' => ($coaching_id != null ? '#calls-datatable-pjax' : '#training-pjax'),
                'title' => "Тренировка",
                'size' => 'normal',
                'content' => '<span class="text-success">Успешно выполнено</span>',
                'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Создать ещё', ['add', 'trener_id' => $trener_id, 'coaching_id' => $coaching_id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

            ];
        } else {
            return [
                'title' => "Создать",
                'size' => 'normal',
                'content' => $this->renderAjax('add_form', [
                    'model' => $model,
                ]),
                'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

            ];
        }
    }

    /**
     * Updates an existing Training model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Тренировка",
                    'size' => 'normal',
                    'content' => $this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Ок', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Изменить', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
            } else {
                return [
                    'title' => "Изменить",
                    'size' => 'normal',
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Training model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete an existing Coaching model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteCoach($id)
    {
        $request = Yii::$app->request;
        $this->findCoaching($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-list-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete an existing Training model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */

    public function actionRemove($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#calls-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete an existing Training model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionCoachRemove($id)
    {
        $request = Yii::$app->request;
        Coaching::findOne($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#calls-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Finds the Training model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Training the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Training::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Training model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Training the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findCoaching($id)
    {
        if (($model = Coaching::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
