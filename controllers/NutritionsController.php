<?php

namespace app\controllers;

use app\models\form\NutritionComment;
use app\models\Users;
use Yii;
use app\models\Nutritions;
use app\models\NutritionEating;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\NutritionComments;
use app\models\nutritionsEatingProducts;
use app\models\nutritionEatingToProducts;

/**
 * NutritionsController implements the CRUD actions for Nutritions model.
 */
class NutritionsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Nutritions models.
     * @return mixed
     */
    public function actionIndex()
    {
          $connection = Yii::$app->getDb();
          $nutriotionsIds = [];

          if(Yii::$app->user->identity->permission == Users::USER_ROLE_TRENER)
          {
              $trenerId = Yii::$app->user->getId();
              $command = $connection->createCommand("SELECT n.*,ne.id as eating_id, ne.name as eating_name, ne.begintime as eating_begintime, nep.name as nep_name, nep.portion as nep_portion, nep.calorie as nep_calorie  From nutritions n left join nutrition_eating ne on (n.id=ne.nutrition_id) left join nutrition_eating_to_products ep on (ne.id=ep.eating_id) left join nutritions_eating_products nep on (ep.id=nep.id)  where n.id and n.trener_id = ".$trenerId);
          } else {
              $nutriotionsIds = array_unique(array_values(ArrayHelper::map(Yii::$app->user->identity->getNutritionsUsers()->all(), 'users_id', 'nutritions_id')));
              $command = $connection->createCommand("SELECT n.*,ne.id as eating_id, ne.name as eating_name, ne.begintime as eating_begintime, nep.name as nep_name, nep.portion as nep_portion, nep.calorie as nep_calorie  From nutritions n left join nutrition_eating ne on (n.id=ne.nutrition_id) left join nutrition_eating_to_products ep on (ne.id=ep.eating_id) left join nutritions_eating_products nep on (ep.id=nep.id)  where n.id");
          }

        $results = $command->queryAll();

        if(count($nutriotionsIds) > 0)
          {
              $results = array_filter($results, function($result) use ($nutriotionsIds){
                    return in_array($result['id'], $nutriotionsIds);
              });
          }
         
           $programs=[];
         foreach ($results as $result){
            $neps[$result['eating_id']][]=[
                'name'=>$result['nep_name'],
                'portion'=>$result['nep_portion'],
                'calorie'=>$result['nep_calorie'],
            ]; 
             
           
            if(isset($callorySumm[$result['id']])){
                $callorySumm[$result['id']]+=intval($result['nep_calorie']);
            } else {
                $callorySumm[$result['id']]=intval($result['nep_calorie']);
            }
             if(isset($count[$result['id']])){
                $count[$result['id']]= count($result['eating_name']);
            } else {
                $count[$result['id']]= count($result['eating_name']);
            } 
            

            $eatings[$result['id']][$result['eating_id']]=[
                'name'=>$result['eating_name'],
                'eating_begintime'=>$result['eating_begintime'],
                'ep'=>$neps[$result['eating_id']],           
                
            ];

             $trener = Users::findOne($result['trener_id']);
             if($trener != null) {
                 $trenerName = $trener->fio;
             } else {
                 $trenerName = null;
             }
            
           
            $programs[$result['id']]=[
                'name'=>$result['name'],
                'description'=>$result['description'],
                'protein'=>$result['protein'],
                'fat'=>$result['fat'],
                'carbohydrate'=>$result['carbohydrate'],
                'trener_id'=>$result['trener_id'],
                'trenerName'=>$trenerName,
                'status'=>$result['status'],
                'callorySumm'=> $callorySumm[$result['id']],
                'count' => $count[$result['id']],
                'eating'=>$eatings[$result['id']],
            ];
            
         }

        return $this->render('index',compact('programs','eatings','neps'));
    }

    /**
     * Displays a single Nutritions model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
         $connection = Yii::$app->getDb();
          $command = $connection->createCommand("SELECT n.*,ne.id as eating_id, ne.name as eating_name, ne.begintime as eating_begintime, nep.name as nep_name, nep.portion as nep_portion, nep.calorie as nep_calorie  From nutritions n left join nutrition_eating ne on (n.id=ne.nutrition_id) left join nutrition_eating_to_products ep on (ne.id=ep.eating_id) left join nutritions_eating_products nep on (ep.id=nep.id)  where n.id=".$id);

          $nutrition = $this->findModel($id);

      $results = $command->queryAll();
         
           $programs=[];
         foreach ($results as $result){
            $nepss[$result['eating_id']]=[
                'name'=>$result['nep_name'],
                'portion'=>$result['nep_portion'],
                'calorie'=>$result['nep_calorie'],
            ]; 
            $neps[$result['eating_id']][]=[
                'name'=>$result['nep_name'],
                'portion'=>$result['nep_portion'],
                'calorie'=>$result['nep_calorie'],
            ]; 
             
           
            if(isset($callorySumm[$result['id']])){
                $callorySumm[$result['id']]+=intval($result['nep_calorie']);
            } else {
                $callorySumm[$result['id']]=intval($result['nep_calorie']);
            }
             if(isset($count[$result['id']])){
                $count[$result['id']]= count($result['eating_name']);
            } else {
                $count[$result['id']]= count($result['eating_name']);
            } 
             $eatingss[$result['id']]=[
                'name'=>$result['eating_name'],
                'eating_begintime'=>$result['eating_begintime'],
                'ep'=>$neps[$result['eating_id']],           
                
            ];

             $newGrant = [];
             foreach ($eatingss as $eatingId => $eatingData)
             {
                  foreach ($results as $row)
                  {
                      if($eatingId == $results['id'])
                      {
                          $newGrant[] = $row;
                      }
                  }
             }

            $eatings[$result['id']][$result['eating_id']]=[
                'name'=>$result['eating_name'],
                'eating_begintime'=>$result['eating_begintime'],
                'ep'=>$neps[$result['eating_id']],           
                
            ];


            $programs[$result['id']]=[
                'name'=>$result['name'],
                'description'=>$result['description'],
                'protein'=>$result['protein'],
                'fat'=>$result['fat'],
                'carbohydrate'=>$result['carbohydrate'],
                'trener_id'=>$result['trener_id'],
                'status'=>$result['status'],
                'callorySumm'=> $callorySumm[$result['id']],
                'count' => $count[$result['id']],
                'eating'=>$eatings[$result['id']],
                'newGrant' => $newGrant,
            ];
            
         }

          $comments = NutritionComments::find()->where(['nutrition_id' => $id])->orderBy('created desc')->all();

        $nutritionEatings = NutritionEating::find()->where(['nutrition_id' => $id])->with('nutritionsEatingProducts')->all();

        return $this->render('view',compact('programs','eatingss','nepss', 'comments', 'id', 'nutrition', 'nutritionEatings'));
    }

    /**
     * Creates a new Nutritions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
   public function actionCreate()
    {
        $model = new Nutritions();
        $newEating = new NutritionEating();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'newEating' => $newEating,
        ]);
    }

    /**
     *
     */
    public function actionAddComment()
    {
        $request = Yii::$app->request;
        $model = new NutritionComment();

        if($model->load($request->post()) && $model->comment())
        {

        } else {

        }
    }

    /**
     * Updates an existing Nutritions model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Nutritions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Nutritions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Nutritions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Nutritions::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
