<?php

namespace app\controllers;

use app\models\form\UploadForm;
use Yii;
use app\models\Exercise;
use app\models\ExerciseSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * ExerciseController implements the CRUD actions for Exercise model.
 */
class ExerciseController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Exercise models.
     * @param integer $group_id
     * @return mixed
     */
    public function actionIndex($group_id = null)
    {
        $searchModel = new ExerciseSearch();
        if($group_id != null)
        {
            $dataProvider = $searchModel->searchByGroup(Yii::$app->request->queryParams, $group_id);
        } else {
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionClientView()
    {
        return $this->render('client-view', [
//            'trainings' => $trainings,
        ]);
    }

    public function actionNutrition()
    {
        return $this->render('nutrition', [
//            'trainings' => $trainings,
        ]);
    }
    public function actionNutritionView()
    {
        return $this->render('nutrition-view', [
//            'trainings' => $trainings,
        ]);
    }

    /**
     * Displays a single Exercise model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Упражнение",
                'size' => 'normal',
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Изменить', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Exercise model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @param $group_id
     * @return mixed
     */
    public function actionCreate($group_id = null)
    {
        $request = Yii::$app->request;
        $model = new Exercise();
        $model->exercise_group_id = $group_id;

        if ($request->isAjax) {

            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {

                $model->file = UploadedFile::getInstance($model, 'file');
                if (!empty($model->file)) {
                    $fileName = Yii::$app->security->generateRandomString();
                    $model->file->saveAs('uploads/' . $fileName . '.' . $model->file->extension);
                    Yii::$app->db->createCommand()->update('exercise', ['photo' => 'uploads/'.$fileName . '.' . $model->file->extension], ['id' => $model->id])->execute();
                }

                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Упражнение",
                    'size' => 'normal',
                    'content' => '<span class="text-success">Успешно выполнено</span>',
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Создать ещё', ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                ];
            } else {
                return [
                    'title' => "Упражнение",
                    'size' => 'normal',
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            }
        } else {

            if ($model->load($request->post()) && $model->save()) {
                $model->file = UploadedFile::getInstance($model, 'file');
                if (!empty($model->file)) {
                    $fileName = Yii::$app->security->generateRandomString();
                    $model->file->saveAs('uploads/' . $fileName . '.' . $model->file->extension);
                    Yii::$app->db->createCommand()->update('exercise', ['photo' => 'uploads/'.$fileName . '.' . $model->file->extension], ['id' => $model->id])->execute();
                }

                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * @param int $id
     */
    public function actionUploadPhoto($id)
    {
        $model = $this->findModel($id);

        Yii::$app->response->format = Response::FORMAT_JSON;

        $fileName = strval($_FILES['UploadForm']['name']);
        unset($_FILES['UploadForm']['name']);
        ArrayHelper::setValue($_FILES,'UploadForm.name.imageFile', [0 => $fileName]);

        $fileType = strval($_FILES['UploadForm']['type']);
        unset($_FILES['UploadForm']['type']);
        ArrayHelper::setValue($_FILES,'UploadForm.type.imageFile', [0 => $fileType]);

        $tmpFile = strval($_FILES['UploadForm']['tmp_name']);
        unset($_FILES['UploadForm']['tmp_name']);
        ArrayHelper::setValue($_FILES,'UploadForm.tmp_name.imageFile', [0 => $tmpFile]);

        $fileError = intval($_FILES['UploadForm']['error']);
        unset($_FILES['UploadForm']['error']);
        ArrayHelper::setValue($_FILES,'UploadForm.error.imageFile', [0 => $fileError]);

        $size = intval($_FILES['UploadForm']['size']);
        unset($_FILES['UploadForm']['size']);
        ArrayHelper::setValue($_FILES,'UploadForm.size.imageFile', [0 => $size]);

        $uploadModel = new UploadForm(['uploadPath' => 'uploads/avatars/', 'imageFile' => $_FILES['UploadForm']]);

        if (Yii::$app->request->isPost) {
            $uploadModel->imageFile = UploadedFile::getInstances($uploadModel, 'imageFile');
            if ($uploadModel->upload()) {
                // file is uploaded successfully

                $model->photo = $uploadModel->fullUploadedFilePath[0];
                $model->save(false);

                return ['path' => $uploadModel->fullUploadedFilePath[0]];
            }

            return ['errors' => $uploadModel->errors];
        }
    }

    /**
     * Creates a new Exercise model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionAdd($trener_id)
    {
        $request = Yii::$app->request;
        $model = new Exercise();
        $model->trener_id = $trener_id;

        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($model->load($request->post()) && $model->save()) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if (!empty($model->file)) {
                $model->file->saveAs('uploads/' . $model->file->baseName . '.' . $model->file->extension);
                Yii::$app->db->createCommand()->update('exercise', ['photo' => $model->file->baseName . '.' . $model->file->extension], ['id' => $model->id])->execute();
            }
            return [
                'forceReload' => '#training-pjax',
                'title' => "Упражнение",
                'size' => 'normal',
                'content' => '<span class="text-success">Успешно выполнено</span>',
                'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Создать ещё', ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return [
                'title' => "Упражнение",
                'size' => 'normal',
                'content' => $this->renderAjax('create', [
                    'model' => $model,
                ]),
                'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
            ];
        }

    }

    /**
     * Updates an existing Exercise model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {

                $model->file = UploadedFile::getInstance($model, 'file');
                if (!empty($model->file)) {
                    $fileName = Yii::$app->security->generateRandomString();
                    $model->file->saveAs('uploads/' . $fileName . '.' . $model->file->extension);
                    Yii::$app->db->createCommand()->update('exercise', ['photo' => 'uploads/'.$fileName . '.' . $model->file->extension], ['id' => $model->id])->execute();
                }

                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Упражнение",
                    'size' => 'normal',
                    'content' => $this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Изменить', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
            } else {
                return [
                    'title' => "Изменить",
                    'size' => 'normal',
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                $model->file = UploadedFile::getInstance($model, 'file');
                if (!empty($model->file)) {
                    $fileName = Yii::$app->security->generateRandomString();
                    $model->file->saveAs('uploads/' . $fileName . '.' . $model->file->extension);
                    Yii::$app->db->createCommand()->update('exercise', ['photo' => 'uploads/'.$fileName . '.' . $model->file->extension], ['id' => $model->id])->execute();
                }

                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Updates an existing Exercise model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionChange($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {

                $model->file = UploadedFile::getInstance($model, 'file');
                if (!empty($model->file)) {
                    $model->file->saveAs('uploads/' . $model->file->baseName . '.' . $model->file->extension);
                    Yii::$app->db->createCommand()->update('exercise', ['photo' => $model->file->baseName . '.' . $model->file->extension], ['id' => $model->id])->execute();
                }

                return [
                    'forceReload' => '#training-pjax',
                    'title' => "Упражнение",
                    'size' => 'normal',
                    'forceClose' => true,
                ];
            } else {
                return [
                    'title' => "Изменить",
                    'size' => 'normal',
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Exercise model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-list-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete an existing Exercise model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionRemove($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#training-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Finds the Exercise model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Exercise the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Exercise::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
