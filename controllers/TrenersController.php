<?php

namespace app\controllers;

use app\models\Users;
use Yii;
use app\models\UsersSearch;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii2mod\rbac\filters\AccessControl;
use yii\helpers\Url;

/**
 * TrenersController
 */
class TrenersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'roles' => ['@'],
                        'allow' => true,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['permission' => Users::USER_ROLE_TRENER]);

        $this->view->params['header-after'] = [
            ['label' => 'Все тренеры', 'active' => true, 'url' => Url::toRoute(['index'])],
            ['label' => 'Мои тренеры', 'active' => false, 'url' => Url::toRoute(['index-my'])],
        ];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndexMy()
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->searchMyTreners(Yii::$app->request->queryParams);

        $this->view->params['header-after'] = [
            ['label' => 'Все тренеры', 'active' => false, 'url' => Url::toRoute(['index'])],
            ['label' => 'Мои тренеры', 'active' => true, 'url' => Url::toRoute(['index-my'])],
        ];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
