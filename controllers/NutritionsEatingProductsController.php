<?php

namespace app\controllers;

use app\models\NutritionEating;
use Yii;
use app\models\NutritionsEatingProducts;
use app\models\NutritionsEatingProductsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * NutritionsEatingProductsController implements the CRUD actions for NutritionsEatingProducts model.
 */
class NutritionsEatingProductsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all NutritionsEatingProducts models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NutritionsEatingProductsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single NutritionsEatingProducts model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new NutritionsEatingProducts model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param int $nutrition_eating_id
     * @return mixed
     */
    public function actionCreate($nutrition_eating_id)
    {
        $model = new NutritionsEatingProducts();

        $model->nutrition_eating_id = $nutrition_eating_id;
        $nutritionEating = NutritionEating::findOne($nutrition_eating_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['nutritions/view', 'id' => $nutritionEating->nutrition_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing NutritionsEatingProducts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing NutritionsEatingProducts model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the NutritionsEatingProducts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NutritionsEatingProducts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NutritionsEatingProducts::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
