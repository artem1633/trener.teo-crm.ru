<?php

namespace app\controllers;

use app\models\AffiliateProgram;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\HttpException;

/**
 * ClientsController
 */
class AffiliateProgramController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Users models.
     * @throws HttpException
     * @return mixed
     */
    public function actionIndex()
    {
        $myReferrals = Yii::$app->user->identity->getMyReferrals()->all();

        return $this->render('index', [
            'myReferrals' => $myReferrals,
        ]);
    }
}
