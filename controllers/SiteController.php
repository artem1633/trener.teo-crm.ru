<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\RegisterForm;
use app\models\ContactForm;
use app\models\User;
use app\models\PasswordResetRequestForm;
use app\models\PasswordResetCodeForm;
use app\models\ResetPasswordForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout','index'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                /*'actions' => [
                    'logout' => ['post'],
                ],*/
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
            ],
        ];
    }


    public function onAuthSuccess($client)
    {
    
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $attributes = $client->getUserAttributes();
        if(Yii::$app->request->get('authclient')=='google'){
            if(isset($attributes['emails'][0]['value'])){
                $user=User::findOne(['login' => 'gp'.$attributes['id']]);
            } else {
                $user=User::find()->where(['login' => 'gp'.$attributes['id']])->orWhere(['email' => $attributes['emails'][0]['value']])->one();
            }
            if(!$user){
                $user = new User();
                $user->fio=$attributes['displayName'];
                $user->login='gp'.$attributes['id'];
                $user->password=md5(time());
                if(isset($attributes['emails'][0]['value'])){
                    $user->email=$attributes['emails'][0]['value'];
                }
                $gender=['male'=>1,'female'=>2];
                $user->gender=$gender[$attributes['gender']];
                $user->save();
            }
            Yii::$app->getUser()->login($user);
        }else if(Yii::$app->request->get('authclient')=='facebook'){
            if(isset($attributes['email'])){
                $user=User::findOne(['login' => 'fb'.$attributes['id']]);
            } else {
                $user=User::find()->where(['login' => 'fb'.$attributes['id']])->orWhere(['email' => $attributes['email']])->one();
            }
            if(!$user){
                $user = new User();
                $user->fio=$attributes['name'];
                $user->login='fb'.$attributes['id'];
                $user->password=md5(time());
                $user->email=$attributes['email'];
                $user->save();
            }
            Yii::$app->getUser()->login($user);
        }else if(Yii::$app->request->get('authclient')=='vkontakte'){
            
            $user=User::findOne(['login' => 'vk'.$attributes['id']]);

            if(!$user){
                $user = new User();
                $user->fio=$attributes['first_name'].' '.$attributes['last_name'];
                $user->login='vk'.$attributes['id'];
                $user->password=md5(time());
                $user->gender=$attributes['sex'];
                $user->save();
            }
            Yii::$app->getUser()->login($user);
        }
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->render('index');
        }else {
            return $this->redirect(['site/login']);
        }
    }
    //
    public function actionAgreement()
    {
        return $this->render('agreement');
    }
    //
    public function actionPrivacyPolicy()
    {
        return $this->render('privacy-policy');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */

    public function actionAvtorizatsiya()
    {
        if(isset(Yii::$app->user->identity->id))
        {
            if(isset(Yii::$app->user->identity->permission)){
                return $this->render('index');
            } else {
                $this->redirect(['users/choosetype']);
            }
        }        
        else
        {
            Yii::$app->user->logout();
            $this->redirect(['login']);
        }
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionRegister()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }


        $model = new RegisterForm();

        if(isset($_GET['ref'])){
            $model->referralId = $_GET['ref'];
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->register()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->redirect(['user/choosetype']);
                }
            }
        }

        return $this->render('register', [
            'model' => $model,
        ]);
    }
    
    public function actionRecover()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Письмо с кодом подтверждения отправлен на указанный Email.');
                return $this->redirect(['site/reset-code']);
            } else {
                Yii::$app->session->setFlash('error', 'Извините не удалось отправить сообщение на указанный Email.');
            }
        }

        return $this->render('recover', [
            'model' => $model,
        ]);
    }

    public function actionResetCode(){
        $model = new PasswordResetCodeForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            return $this->redirect(['site/reset-password','code'=>$model->password_reset_token]);
            
        }

        return $this->render('reset-code', [
            'model' => $model,
        ]);
    }
    public function actionResetPassword($code=null)
    {
        if($code){
            if(!User::findByPasswordResetToken($code)){
                return $this->redirect(['site/reset-code']);
            }
            $model = new ResetPasswordForm($code);

            if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
                //Yii::$app->session->setFlash('success', 'New password saved.');
                return $this->goHome();
            }

            return $this->render('reset-password', [
                'model' => $model,
            ]);
        } else {
            return $this->redirect(['site/reset-code']);
        }
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        $this->redirect(['login']);
    }

    public function actionMenuPosition()
    {
        $session = Yii::$app->session;
        if($session['menu'] == null | $session['menu'] == 'large') $session['menu'] = 'small';
        else $session['menu'] = 'large';

        return $session['menu'];
    }
}
