<?php

namespace app\values;

use yii\base\Component;

/**
 * Class Country
 * @package app\values
 */
class Country extends Component
{
    const DATA_PATH = '../data/countries.php';

    /**
     * @return array
     */
    public static function getList()
    {
        return require(self::DATA_PATH);
    }

    /**
     * @param $code
     * @return string|null
     */
    public static function getCountryName($code)
    {
        $countries = self::getList();
        if(isset($countries[$code]))
        {
            return $countries[$code];
        }

        return null;
    }
}