<?php

namespace app\values;

use yii\base\Component;

/**
 * Class InstagramProfileUrl
 * @package app\values
 * Класс отдает ссылку на иснтаграм пользователя,
 * изходя из первоночальной строки указывающая на инстаграм.
 * Например: profile => https://www.instagram.com/profile
 *           @profile => https://www.instagram.com/profile
 *
 * @property string $url
 */
class InstagramProfileUrl extends Component
{
    const INSTAGRAM_URL = 'instagram://www.instagram.com/';

    /**
     * @var string Входящая строка
     */
    public $input;

    /**
     * Отдает ссылку на инстаграм изходя из $input
     * @return string
     */
    public function getUrl()
    {
        if($this->input == null)
        {
            return null;
        } else if(substr($this->input, 0, 4 ) === "http" ||
            substr($this->input, 0, 4 ) === "https" ||
            stripos($this->input, 'instagram.com'))
        {
            return $this->input;
        } else if(substr($this->input, 0,1) === '@')
        {
            $this->input = substr($this->input, 1);

            return self::INSTAGRAM_URL.$this->input;
        } else {
            return self::INSTAGRAM_URL.$this->input;
        }
    }
}