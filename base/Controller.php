<?php

namespace app\base;

use Yii;
use yii\web\Controller as WebController;
use app\models\Users;

/**
 * Class Controller
 * @package app\base
 */
class Controller extends WebController
{

    private $_viewPath;

    /**
     * {@inheritdoc}
     */
    public function getViewPath()
    {
        if ($this->_viewPath === null) {
            if(Yii::$app->user->isGuest){
                $this->_viewPath = $this->module->getViewPath() . DIRECTORY_SEPARATOR . $this->id;
            } else {
                if(Yii::$app->user->identity->permission == Users::USER_ROLE_TRENER) {
                    $this->_viewPath = $this->module->getViewPath() . DIRECTORY_SEPARATOR . $this->id . DIRECTORY_SEPARATOR . 'trener';
                } else if (Yii::$app->user->identity->permission == Users::USER_ROLE_CLIENT) {
                    $this->_viewPath = $this->module->getViewPath() . DIRECTORY_SEPARATOR . $this->id . DIRECTORY_SEPARATOR . 'client';
                } else {
                    $this->_viewPath = $this->module->getViewPath() . DIRECTORY_SEPARATOR . $this->id;
                }
            }
        }

        return $this->_viewPath;
    }
}