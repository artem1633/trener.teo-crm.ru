<?php

use yii\db\Migration;

/**
 * Handles the creation of table `coaching_completing`.
 */
class m190227_122214_create_coaching_completing_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('coaching_completing', [
            'id' => $this->primaryKey(),
            'coaching_id' => $this->integer()->comment('Тренировка'),
            'client_id' => $this->integer()->comment('Клиент'),
            'created_at' => $this->dateTime(),
        ]);
        $this->addCommentOnTable('coaching_completing', 'Фиксирования выполнения клиентами упражнений');

        $this->createIndex(
            'idx-coaching_completing-coaching_id',
            'coaching_completing',
            'coaching_id'
        );

        $this->addForeignKey(
            'fk-coaching_completing-coaching_id',
            'coaching_completing',
            'coaching_id',
            'coaching',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-coaching_completing-client_id',
            'coaching_completing',
            'client_id'
        );

        $this->addForeignKey(
            'fk-coaching_completing-client_id',
            'coaching_completing',
            'client_id',
            'users',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-coaching_completing-coaching_id',
            'coaching_completing'
        );

        $this->dropIndex(
            'idx-coaching_completing-coaching_id',
            'coaching_completing'
        );

        $this->dropForeignKey(
            'fk-coaching_completing-client_id',
            'coaching_completing'
        );

        $this->dropIndex(
            'idx-coaching_completing-client_id',
            'coaching_completing'
        );

        $this->dropTable('coaching_completing');
    }
}
