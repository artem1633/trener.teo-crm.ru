<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m180601_154502_create_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            //--------------------------Общие---------------------------------------//
            'fio' => $this->string(255)->comment('ФИО'),//
            'register_date' => $this->date()->comment('Дата регистрации'),//
            'address' => $this->text()->comment('Адрес, Город'),//
            'photo' => $this->string(255)->comment('Заглавное фото профиля'),//
            'status' => $this->boolean()->comment('Статус'),//
            'telephone' => $this->string(255)->comment('Телефон'),
            'permission' => $this->string(255)->comment('Должность'),
            'login' => $this->string(255)->unique()->comment('Логин'),
            'password' => $this->string(255)->comment('Пароль'),
            'gender' => $this->boolean()->comment('Пол'),//
            'photos' => $this->text()->comment('Фотографии'),//
            'birthday' => $this->date()->comment('Дата рождение'),
            //--------------------------полей для тренера----------------------------//
            'work_experience' => $this->integer()->comment('Стаж работы'),
            'education' => $this->string(255)->comment('Образование'),
            'education_scans' => $this->string(255)->comment('Сканы документов об образовании'),
            'instagram' => $this->string(255)->comment('Ссылка на Instagram-профиль'),
            'cost' => $this->float()->comment('Стоимость занятия'),
            'cost_month' => $this->float()->comment('Стоимость месяца тренировок'),
            'cost_program' => $this->float()->comment('Стоимость программы тренировок'),
            'props' => $this->string(255)->comment('Реквизиты'),
            'last_pay' => $this->date()->comment('Дата последней оплаты'),
            'total_pay' => $this->float()->comment('Общая сумма оплат'),
            'referal' => $this->integer()->comment('Кол-во тренеров , привлеченных по реферальной програме'),
            'referal_sum' => $this->float()->comment('Cумма бонусов по реферальной программе'),
            //---------------------------полей для клиента---------------------------//
            'trener' => $this->integer()->comment('Тренер клиента'),//
            'information' => $this->text()->comment('Информация обо мне'),
            'occupation' => $this->text()->comment('Род деятельности'),
            'training_experience' => $this->string(255)->comment('Стаж тренировок'),
            'fitnes' => $this->string(255)->comment('Фитнес-клуб'),
            'training_goal' => $this->string(255)->comment('Цель тренировок'),
        ]);

        $this->insert('users',array(
            'fio' => 'Иванов Иван Иванович',
            'register_date' => date('Y-m-d'),
            'address' => 'Россия, Санкт-Петербург',
            'photo' => 'nouser.png',
            'status' => 1,
            'telephone' => null,
            'permission' => 'administrator',
            'login' => 'admin',
            'password' => md5('admin'),
            'gender' => 1,
            'birthday' => '1994-06-05',
        ));


        $this->createTable('auth_item', [
            'name' => $this->string(64)->notNull(),
            'type'=>'tinyint(6) NOT NULL',
            'description'=>$this->text(),
            'rule_name'=>$this->string(64),
            'data'=>$this->binary(429496729),
            'created_at'=>$this->integer(11),
            'updated_at'=>$this->integer(11),
            'PRIMARY KEY (name)',
        ]);


        $this->createIndex(
            'idx-auth_item-type',
            'auth_item',
            'type'
        );
        
        $this->createTable('auth_rule', [
            'name' => $this->string(64)->notNull(),
            'data'=>$this->binary(429496729),
            'created_at'=>$this->integer(11),
            'updated_at'=>$this->integer(11),
            'PRIMARY KEY (name)',
        ]);

        $this->addForeignKey('auth_item_ibfk_1', '{{%auth_item}}', 'rule_name', '{{%auth_rule}}', 'name', 'cascade', 'cascade');
        
        $this->createTable('auth_item_child', [
            'parent'=>$this->string(64)->notNull(),
            'child'=>$this->string(64)->notNull(),
            'PRIMARY KEY (parent, child)',
        ]);


        $this->addForeignKey('auth_item_child_ibfk_1', '{{%auth_item_child}}', 'parent', '{{%auth_item}}', 'name', 'cascade', 'cascade');

        $this->addForeignKey('auth_item_child_ibfk_2', '{{%auth_item_child}}', 'child', '{{%auth_item}}', 'name', 'cascade', 'cascade');


        $this->insert('auth_item', array(
         'name' => 'administrator',
            'description' => 'admin',
            'type' => 1,
            'created_at' => time(),
            'updated_at' => time()
        ));
        $this->insert('auth_item', array(
         'name' => 'client',
            'description' => 'client',
            'type' => 1,
            'created_at' => time(),
            'updated_at' => time()
        ));
        $this->insert('auth_item', array(
            'name' => 'trener',
            'description' => 'client',
            'type' => 1,
            'created_at' => time(),
            'updated_at' => time()
        ));

        $this->createTable('auth_assignment', [
            'item_name'=>$this->string(64)->notNull(),
            'user_id'=>$this->string(64)->notNull(),
            'created_at'=>$this->integer(11),
            'PRIMARY KEY (item_name,user_id)',
        ]);

        $this->addForeignKey('auth_assignment_ibfk_1', '{{%auth_assignment}}', 'item_name', '{{%auth_item}}', 'name', 'cascade', 'cascade');
        
        $this->insert('auth_assignment', array(
            'item_name' => 'administrator',
            'user_id' => '1',
            'created_at' => time(),
        ));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('auth_assignment_ibfk_1', '{{%auth_assignment}}');

        $this->dropForeignKey('auth_item_child_ibfk_1', '{{%auth_item_child}}');
        $this->dropForeignKey('auth_item_child_ibfk_2', '{{%auth_item_child}}');

        $this->dropForeignKey('auth_item_ibfk_1', '{{%auth_item}}');

        $this->dropTable('auth_assignment');
        $this->dropTable('auth_item_child');
        $this->dropTable('auth_rule');
        $this->dropTable('auth_item');
        $this->dropTable('users');

    }
}
