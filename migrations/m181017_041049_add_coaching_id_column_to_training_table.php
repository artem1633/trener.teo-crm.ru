<?php

use yii\db\Migration;

/**
 * Handles adding coaching_id to table `training`.
 */
class m181017_041049_add_coaching_id_column_to_training_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('training', 'coaching_id', $this->integer());

        $this->createIndex('idx-training-coaching_id', 'training', 'coaching_id', false);
        $this->addForeignKey("fk-training-coaching_id", "training", "coaching_id", "coaching", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-training-coaching_id','training');
        $this->dropIndex('idx-training-coaching_id','training');
        
        $this->dropColumn('training', 'coaching_id');
    }
}
