<?php

use yii\db\Migration;

/**
 * Handles the creation of table `affiliate_program`.
 */
class m190207_201717_create_affiliate_program_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('affiliate_program', [
            'id' => $this->primaryKey(),
            'owner_id' => $this->integer()->comment('Пользователь, хозяин реферальной ссылки'),
            'registered_id' => $this->integer()->comment('Зарегистрировавшийся пользователь'),
            'register_date' => $this->dateTime()->comment('Дата и время регистрации'),
            'referral_link' => $this->string()->comment('Ссылка, по которой была проведена регистрация')
        ]);
        $this->addCommentOnTable('affiliate_program', 'Партнерская программа');

        $this->createIndex(
            'idx-affiliate_program-owner_id',
            'affiliate_program',
            'owner_id'
        );

        $this->addForeignKey(
            'fk-affiliate_program-owner_id',
            'affiliate_program',
            'owner_id',
            'users',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-affiliate_program-registered_id',
            'affiliate_program',
            'registered_id'
        );

        $this->addForeignKey(
            'fk-affiliate_program-registered_id',
            'affiliate_program',
            'registered_id',
            'users',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-affiliate_program-registered_id',
            'affiliate_program'
        );

        $this->dropIndex(
            'idx-affiliate_program-registered_id',
            'affiliate_program'
        );

        $this->dropForeignKey(
            'fk-affiliate_program-owner_id',
            'affiliate_program'
        );

        $this->dropIndex(
            'idx-affiliate_program-owner_id',
            'affiliate_program'
        );

        $this->dropTable('affiliate_program');
    }
}
