<?php

use yii\db\Migration;

/**
 * Class m181112_175014_chat_table
 */
class m181112_175014_chat_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('chat', [
            'id' => $this->primaryKey(),
            'client_id' => $this->integer()->comment('Клиент'),
            'trainer_id' => $this->integer()->comment('Тренер'),
            'chat_key' => $this->string(255)->comment('Ключ чата'),
            'message' => $this->text()->comment('Сообщение'),
            'created_date' => $this->string(255)->comment('Дата создания и время создания'),
            'created_by' => $this->integer()->comment('Кто написал'),
        ]);
        //request handbooks for order_part
        $this->createIndex('idx-chat-client_id', 'chat', 'client_id', false);
        $this->addForeignKey("fk-chat-client_id", "chat", "client_id", "users", "id", 'CASCADE');
        //nomenclature handbooks for order_part
        $this->createIndex('idx-chat-trainer_id', 'chat', 'trainer_id', false);
        $this->addForeignKey("fk-chat-trainer_id", "chat", "trainer_id", "users", "id", 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('chat');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181112_175014_chat_table cannot be reverted.\n";

        return false;
    }
    */
}
