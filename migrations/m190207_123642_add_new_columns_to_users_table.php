<?php

use yii\db\Migration;

/**
 * Handles adding new columns to table `users`.
 */
class m190207_123642_add_new_columns_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'country', $this->string()->after('register_date')->comment('Страна (ISO 3166-1)'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('uses', 'country');
    }
}
