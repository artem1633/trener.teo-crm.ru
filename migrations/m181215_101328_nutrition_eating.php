<?php

use yii\db\Migration;

/**
 * Handles the creation of table `nutrition_eating`.
 */
class m181215_101328_nutrition_eating extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
       $tableOptions = null;
        if($this->db->driverName === 'mysql'){
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%nutrition_eating}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'nutrition_id' => $this->integer(11)->notNull(),
            'product' => $this->string(252)->notNull(),
            'portion' => $this->integer(11),
            'calorie' => $this->integer(11),
            'begintime' => $this->time()->notNull(),

        ], $tableOptions);

        $this->AddForeignKey('FK_nutrition_eating_nutritions', '{{%nutrition_eating}}', 'nutrition_id', '{{%nutritions}}','id', 'cascade', 'cascade');
         //$this->addForeignKey('auth_item_child_ibfk_1', '{{%auth_item_child}}', 'parent', '{{%auth_item}}', 'name', 'cascade', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()

    {
        $this->dropForeignKey('FK_nutrition_eating_nutritions', '{{%nutrition_eating}}');
        $this->dropTable('{{%nutrition_eating}}');
    }
}
