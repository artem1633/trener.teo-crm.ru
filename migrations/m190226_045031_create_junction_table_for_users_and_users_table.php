<?php

use yii\db\Migration;

/**
 * Handles the creation of table `clients_requests`.
 * Has foreign keys to the tables:
 *
 * - `users`
 * - `users`
 */
class m190226_045031_create_junction_table_for_users_and_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('clients_requests', [
            'client_id' => $this->integer()->comment('Клиент (Делает запрос)'),
            'trainer_id' => $this->integer()->comment('Тренер (Отвечает на запрос)'),
            'accepted' => $this->boolean()->comment('Ответ'),
            'created_at' => $this->dateTime(),
            'PRIMARY KEY(client_id, trainer_id)',
        ]);
        $this->addCommentOnTable('clients_requests','Запросы клиентов тренерам на ведение');

        // creates index for column `users_id`
        $this->createIndex(
            'idx-clients_requests-client_id',
            'clients_requests',
            'client_id'
        );

        // add foreign key for table `users`
        $this->addForeignKey(
            'fk-clients_requests-client_id',
            'clients_requests',
            'client_id',
            'users',
            'id',
            'CASCADE'
        );

        // creates index for column `users_id`
        $this->createIndex(
            'idx-clients_requests-trainer_id',
            'clients_requests',
            'trainer_id'
        );

        // add foreign key for table `users`
        $this->addForeignKey(
            'fk-clients_requests-trainer_id',
            'clients_requests',
            'trainer_id',
            'users',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `users`
        $this->dropForeignKey(
            'fk-clients_requests-client_id',
            'clients_requests'
        );

        // drops index for column `users_id`
        $this->dropIndex(
            'idx-clients_requests-client_id',
            'clients_requests'
        );

        // drops foreign key for table `users`
        $this->dropForeignKey(
            'fk-clients_requests-trainer_id',
            'clients_requests'
        );

        // drops index for column `users_id`
        $this->dropIndex(
            'idx-clients_requests-trainer_id',
            'clients_requests'
        );

        $this->dropTable('clients_requests');
    }
}
