<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_notifications_marker`.
 */
class m190228_085340_create_user_notifications_marker_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user_notifications_marker', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Пользователь'),
            'client_requests_trainer' => $this->integer()->defaultValue(false)->comment('Уведомление о новом запросе клиента'),
        ]);
        $this->addCommentOnTable('user_notifications_marker', 'Маркеры уведомлений пользователей');

        $this->createIndex(
            'idx-user_notifications_marker-user_id',
            'user_notifications_marker',
            'user_id'
        );

        $this->addForeignKey(
            'fk-user_notifications_marker-user_id',
            'user_notifications_marker',
            'user_id',
            'users',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-user_notifications_marker-user_id',
            'user_notifications_marker'
        );

        $this->dropIndex(
            'idx-user_notifications_marker-user_id',
            'user_notifications_marker'
        );

        $this->dropTable('user_notifications_marker');
    }
}
