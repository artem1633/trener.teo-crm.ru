<?php

use yii\db\Migration;

/**
 * Class m181203_162836_payments_table
 */
class m181203_162836_payments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('payments', [
            'id' => $this->primaryKey(),
            'client_id' => $this->integer()->comment('Клиент'),
            'trainer_id' => $this->integer()->comment('Тренер'),
            'quantity' => $this->float()->comment('Сумма'),
            'payments_date' => $this->string(255)->comment('Дата совешения платежа'),
        ]);
        $this -> addColumn('{{payments}}', 'period', "enum('all_time','month', 'week', 'day') NOT NULL DEFAULT 'all_time'");

        $this->createIndex('idx-payments-client_id', 'payments', 'client_id', false);
        $this->addForeignKey("fk-payments-client_id", "payments", "client_id", "users", "id", 'CASCADE');

        $this->createIndex('idx-payments-trainer_id', 'payments', 'trainer_id', false);
        $this->addForeignKey("fk-payments-trainer_id", "payments", "trainer_id", "users", "id", 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('payments');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181203_162836_payments_table cannot be reverted.\n";

        return false;
    }
    */
}
