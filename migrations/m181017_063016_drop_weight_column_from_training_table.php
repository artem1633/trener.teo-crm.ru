<?php

use yii\db\Migration;

/**
 * Handles dropping weight from table `training`.
 */
class m181017_063016_drop_weight_column_from_training_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('training', 'weight');
        $this->addColumn('training', 'weight', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('training', 'weight', $this->float());
        $this->dropColumn('training', 'weight');
    }
}
