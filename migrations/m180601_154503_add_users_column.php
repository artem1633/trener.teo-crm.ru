<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m180601_154503_add_users_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('users', 'email', $this->string(255)->after('telephone'));
        $this->addColumn('users', 'auth_key', $this->string(255)->after('password'));
        $this->addColumn('users', 'password_reset_token', $this->string(255)->after('auth_key'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('users', 'email');
        $this->dropColumn('users', 'auth_key');
        $this->dropColumn('users', 'password_reset_token');


    }
}
