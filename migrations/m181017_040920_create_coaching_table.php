<?php

use yii\db\Migration;

/**
 * Handles the creation of table `coaching`.
 */
class m181017_040920_create_coaching_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('coaching', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'description' => $this->text(),
            'trener_id' => $this->integer(),
        ]);

        $this->createIndex('idx-coaching-trener_id', 'coaching', 'trener_id', false);
        $this->addForeignKey("fk-coaching-trener_id", "coaching", "trener_id", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-coaching-trener_id','coaching');
        $this->dropIndex('idx-coaching-trener_id','coaching');
        
        $this->dropTable('coaching');
    }
}
