<?php

use yii\db\Migration;

/**
 * Handles the creation of table `training`.
 */
class m181014_115211_create_training_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('training', [
            'id' => $this->primaryKey(),
            'exercise_id' => $this->integer(),
            'name' => $this->string(255),
            'weight' => $this->float(),
            'repitition' => $this->integer(),
            'approach' => $this->integer(),
            'trener_id' => $this->integer(),
        ]);

        $this->createIndex('idx-training-exercise_id', 'training', 'exercise_id', false);
        $this->addForeignKey("fk-training-exercise_id", "training", "exercise_id", "exercise", "id");

        $this->createIndex('idx-training-trener_id', 'training', 'trener_id', false);
        $this->addForeignKey("fk-training-trener_id", "training", "trener_id", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-training-exercise_id','training');
        $this->dropIndex('idx-training-exercise_id','training');

        $this->dropForeignKey('fk-training-trener_id','training');
        $this->dropIndex('idx-training-trener_id','training');

        $this->dropTable('training');
    }
}
