<?php

use yii\db\Migration;

/**
 * Handles the creation of table `nutritions_users`.
 * Has foreign keys to the tables:
 *
 * - `nutritions`
 * - `users`
 */
class m190213_162022_create_junction_table_for_nutritions_and_users_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('nutritions_users', [
            'nutritions_id' => $this->integer(),
            'users_id' => $this->integer(),
            'PRIMARY KEY(nutritions_id, users_id)',
        ]);

        // creates index for column `nutritions_id`
        $this->createIndex(
            'idx-nutritions_users-nutritions_id',
            'nutritions_users',
            'nutritions_id'
        );

        // add foreign key for table `nutritions`
        $this->addForeignKey(
            'fk-nutritions_users-nutritions_id',
            'nutritions_users',
            'nutritions_id',
            'nutritions',
            'id',
            'CASCADE'
        );

        // creates index for column `users_id`
        $this->createIndex(
            'idx-nutritions_users-users_id',
            'nutritions_users',
            'users_id'
        );

        // add foreign key for table `users`
        $this->addForeignKey(
            'fk-nutritions_users-users_id',
            'nutritions_users',
            'users_id',
            'users',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `nutritions`
        $this->dropForeignKey(
            'fk-nutritions_users-nutritions_id',
            'nutritions_users'
        );

        // drops index for column `nutritions_id`
        $this->dropIndex(
            'idx-nutritions_users-nutritions_id',
            'nutritions_users'
        );

        // drops foreign key for table `users`
        $this->dropForeignKey(
            'fk-nutritions_users-users_id',
            'nutritions_users'
        );

        // drops index for column `users_id`
        $this->dropIndex(
            'idx-nutritions_users-users_id',
            'nutritions_users'
        );

        $this->dropTable('nutritions_users');
    }
}
