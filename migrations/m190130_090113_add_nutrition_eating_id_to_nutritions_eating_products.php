<?php

use yii\db\Migration;

/**
 * Class m190130_090113_add_nutrition_eating_id_to_nutritions_eating_products
 */
class m190130_090113_add_nutrition_eating_id_to_nutritions_eating_products extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('nutritions_eating_products', 'nutrition_eating_id', $this->integer()->comment('Прием питания'));

        $this->createIndex(
            'idx-nutritions_eating_products-nutrition_eating_id',
            'nutritions_eating_products',
            'nutrition_eating_id'
        );

        $this->addForeignKey(
            'fk-nutritions_eating_products-nutrition_eating_id',
            'nutritions_eating_products',
            'nutrition_eating_id',
            'nutrition_eating',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-nutritions_eating_products-nutrition_eating_id',
            'nutritions_eating_products'
        );

        $this->dropIndex(
            'idx-nutritions_eating_products-nutrition_eating_id',
            'nutritions_eating_products'
        );

        $this->dropColumn('nutritions_eating_products', 'nutrition_eating_id');
    }
}
