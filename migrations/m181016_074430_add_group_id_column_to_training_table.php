<?php

use yii\db\Migration;

/**
 * Handles adding group_id to table `training`.
 */
class m181016_074430_add_group_id_column_to_training_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('training', 'group_id', $this->integer());

        $this->createIndex('idx-training-group_id', 'training', 'group_id', false);
        $this->addForeignKey("fk-training-group_id", "training", "group_id", "group", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-training-group_id','training');
        $this->dropIndex('idx-training-group_id','training');
        
        $this->dropColumn('training', 'group_id');
    }
}
