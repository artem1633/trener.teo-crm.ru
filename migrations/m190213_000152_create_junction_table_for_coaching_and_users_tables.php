<?php

use yii\db\Migration;

/**
 * Handles the creation of table `coaching_users`.
 * Has foreign keys to the tables:
 *
 * - `coaching`
 * - `users`
 */
class m190213_000152_create_junction_table_for_coaching_and_users_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('coaching_users', [
            'coaching_id' => $this->integer(),
            'users_id' => $this->integer(),
            'PRIMARY KEY(coaching_id, users_id)',
        ]);

        // creates index for column `coaching_id`
        $this->createIndex(
            'idx-coaching_users-coaching_id',
            'coaching_users',
            'coaching_id'
        );

        // add foreign key for table `coaching`
        $this->addForeignKey(
            'fk-coaching_users-coaching_id',
            'coaching_users',
            'coaching_id',
            'coaching',
            'id',
            'CASCADE'
        );

        // creates index for column `users_id`
        $this->createIndex(
            'idx-coaching_users-users_id',
            'coaching_users',
            'users_id'
        );

        // add foreign key for table `users`
        $this->addForeignKey(
            'fk-coaching_users-users_id',
            'coaching_users',
            'users_id',
            'users',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `coaching`
        $this->dropForeignKey(
            'fk-coaching_users-coaching_id',
            'coaching_users'
        );

        // drops index for column `coaching_id`
        $this->dropIndex(
            'idx-coaching_users-coaching_id',
            'coaching_users'
        );

        // drops foreign key for table `users`
        $this->dropForeignKey(
            'fk-coaching_users-users_id',
            'coaching_users'
        );

        // drops index for column `users_id`
        $this->dropIndex(
            'idx-coaching_users-users_id',
            'coaching_users'
        );

        $this->dropTable('coaching_users');
    }
}
