<?php

use yii\db\Migration;

/**
 * Handles adding with to table `training`.
 */
class m190321_182221_add_with_column_to_training_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('training', 'with', $this->integer()->comment('Объдинить с'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('training', 'with');
    }
}
