<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users_add`.
 */
class m181221_065202_users_add_column_mycode extends Migration
{
    /**
     * {@inheritdoc}
     */


    public function up()
    {
//        $this->addColumn('users', 'mycode', $this->integer(6)->after('total_pay'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('users', 'mycode');
    }
}

