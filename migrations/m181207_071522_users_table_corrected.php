<?php

use yii\db\Migration;

/**
 * Class m181207_071522_users_table_corrected
 */
class m181207_071522_users_table_corrected extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('users', 'training_experience');
        $this->addColumn('users', 'training_experience', $this->string(255));

        $this->dropColumn('users', 'work_experience');
        $this->addColumn('users', 'work_experience', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181207_071522_users_table_corrected cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181207_071522_users_table_corrected cannot be reverted.\n";

        return false;
    }
    */
}
