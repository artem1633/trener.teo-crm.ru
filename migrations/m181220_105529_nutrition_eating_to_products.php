<?php

use yii\db\Migration;

/**
 * Handles the creation of table `eating_products`.
 */
class m181220_105529_nutrition_eating_to_products extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
         $tableOptions = null;
        if($this->db->driverName === 'mysql'){
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%nutrition_eating_to_products}}', [
            'id' => $this->primaryKey(),
            'eating_id' => $this->integer(11)->notNull(),
            'product_id' => $this->integer(11)->notNull(),
            
        ], $tableOptions);
        
        $this->AddForeignKey('FK_nutrition_eating_to_products_nutrition_eating', '{{%nutrition_eating_to_products}}', 'eating_id', '{{%nutrition_eating}}', 'id', 'cascade', 'cascade');
        $this->AddForeignKey('FK_nutrition_eating_to_products_nutritions_eating_products','{{%nutrition_eating_to_products}}', 'product_id', '{{%nutritions_eating_products}}', 'id', 'cascade', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
         $this->dropForeignKey('FK_nutrition_eating_to_products_nutrition_eating', '{{%nutrition_eating_to_products}}');
          $this->dropForeignKey('FK_nutrition_eating_to_products_nutritions_eating_products', '{{%nutrition_eating_to_products}}');
        $this->dropTable('{{%nutrition_eating_to_products}}');
    }
}
