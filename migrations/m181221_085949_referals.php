<?php

use yii\db\Migration;

/**
 * Handles the creation of table `referals`.
 */
class m181221_085949_referals extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $tableOptions = null;
        if($this->db->driverName === 'mysql'){
            $tableOpions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{referals}}', [
            'id' => $this->primaryKey(),
            'referer_id' => $this->integer(11)->notNull(),
            'referal_id' => $this->integer(11)->notNull(),
            'created' => $this->timestamp()->notNull(),

        ], $tableOptions);

        $this->AddForeignKey('FK_referal_user', '{{%referals}}', 'referal_id', 'users', 'id', 'cascade', 'cascade');
        $this->AddForeignKey('FK_referer_user', '{{%referals}}', 'referer_id' , 'users', 'id', 'cascade', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->DropForeignKey('FK_referal_user', '{{%referals}}');
        $this->DropForeignKey('FK_referer_user', '{{%referals}}');
        $this->dropTable('{{%referals}}');
    }
}
