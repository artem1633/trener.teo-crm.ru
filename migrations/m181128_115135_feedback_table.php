<?php

use yii\db\Migration;

/**
 * Class m181128_115135_feedback_table
 */
class m181128_115135_feedback_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('feedback', [
            'id' => $this->primaryKey(),
            'client_id' => $this->integer()->comment('Клиент'),
            'trainer_id' => $this->integer()->comment('Тренер'),
            'rating' => $this->float()->comment('Рейтинг'),
            'client_img' => $this->string(255)->comment('Фото клиента'),
            'message' => $this->text()->comment('Сообщение'),
            'created_date' => $this->string(255)->comment('Дата создания и время создания'),
            'created_by' => $this->integer()->comment('Кто написал'),
        ]);

        $this->createIndex('idx-feedback-client_id', 'feedback', 'client_id', false);
        $this->addForeignKey("fk-feedback-client_id", "feedback", "client_id", "users", "id", 'CASCADE');

        $this->createIndex('idx-feedback-trainer_id', 'feedback', 'trainer_id', false);
        $this->addForeignKey("fk-feedback-trainer_id", "feedback", "trainer_id", "users", "id", 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('feedback');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181128_115135_feedback_table cannot be reverted.\n";

        return false;
    }
    */
}
