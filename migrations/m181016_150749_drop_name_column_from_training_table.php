<?php

use yii\db\Migration;

/**
 * Handles dropping name from table `training`.
 */
class m181016_150749_drop_name_column_from_training_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk-training-group_id','training');
        $this->dropIndex('idx-training-group_id','training');

        $this->dropColumn('training', 'name');
        $this->dropColumn('training', 'group_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('training', 'name', $this->string(255));
        $this->addColumn('training', 'group_id', $this->integer());

        $this->createIndex('idx-training-group_id', 'training', 'group_id', false);
        $this->addForeignKey("fk-training-group_id", "training", "group_id", "group", "id");
    }
}
