<?php

use yii\db\Migration;

/**
 * Handles dropping education_scans from table `users`.
 */
class m181015_161254_drop_education_scans_column_from_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('users', 'education_scans');
        $this->addColumn('users', 'education_scans', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('users', 'education_scans', $this->string(255));
        $this->dropColumn('users', 'education_scans');
    }
}
