<?php

use yii\db\Migration;

/**
 * Handles the creation of table `nutrition_comments`.
 */
class m181215_092018_nutrition_comments extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if($this->db->driverName === 'mysql'){
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%nutrition_comments}}', [
            'id' => $this->primaryKey(),
            'nutrition_id' => $this->integer(11)->notNull(),
            'created' => $this->timestamp()->notNull(),
            'author_id' => $this->integer(11)->notNull(),
            'text' => $this->string(3000),
            'photo' => $this->string(255),
        ], $tableOptions);
        $this->AddForeignkey('FK_nutrition_comments_nutritions', '{{%nutrition_comments}}', 'nutrition_id', '{{%nutritions}}', 'id', 'cascade', 'cascade');
        $this->AddForeignKey('FK_nutrition_comments_users', '{{%nutrition_comments}}', 'author_id', '{{%users}}', 'id', 'cascade', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->DropForeignKey('FK_nurition_comments_nutritions', '{{%nutrition_comments}}');
        $this->DropForeignKey('FK_nurition_comments_users', '{{%nutrition_comments}}');
        $this->dropTable('{{%nutrition_comments}}');
    }
}
