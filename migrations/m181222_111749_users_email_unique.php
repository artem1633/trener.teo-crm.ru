<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users_adds`.
 */
class m181222_111749_users_email_unique extends Migration
{
     public function up()
    {
        $this->alterColumn('users', 'email', $this->string(255)->after('telephone')->unique());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->alterColumn('users', 'email', $this->string(255)->after('telephone'));
    }
}
