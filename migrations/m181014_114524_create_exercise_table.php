<?php

use yii\db\Migration;

/**
 * Handles the creation of table `exercise`.
 */
class m181014_114524_create_exercise_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('exercise', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'video' => $this->text(),
            'photo' => $this->string(255),
            'description' => $this->text(),
            'exercise_group_id' => $this->integer(),
            'trener_id' => $this->integer(),
        ]);

        $this->createIndex('idx-exercise-exercise_group_id', 'exercise', 'exercise_group_id', false);
        $this->addForeignKey("fk-exercise-exercise_group_id", "exercise", "exercise_group_id", "exercise_group", "id");

        $this->createIndex('idx-exercise-trener_id', 'exercise', 'trener_id', false);
        $this->addForeignKey("fk-exercise-trener_id", "exercise", "trener_id", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-exercise-exercise_group_id','exercise');
        $this->dropIndex('idx-exercise-exercise_group_id','exercise');

        $this->dropForeignKey('fk-exercise-trener_id','exercise');
        $this->dropIndex('idx-exercise-trener_id','exercise');

        $this->dropTable('exercise');
    }
}
