<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `group`.
 */
class m181016_150842_drop_group_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('group');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createTable('group', [
            'id' => $this->primaryKey(),
        ]);
    }
}
