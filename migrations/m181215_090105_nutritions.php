<?php

use yii\db\Migration;

/**
 * Handles the creation of table `nutritions`.
 */
class m181215_090105_nutritions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if($this->db->driverName === 'mysql'){
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%nutritions}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'description' => $this->string(3000),
            'protein' => $this->integer(11),
            'fat' => $this->integer(11),
            'carbohydrate' => $this->integer(11),
            'trener_id' => $this->integer(11)->notNull(),
            'status' => $this->boolean()->notNull(),
        ],$tableOptions);
        

         $this->addForeignKey('FK_nutritions_users', '{{%nutritions}}', 'trener_id', '{{%users}}', 'id', 'cascade', 'cascade');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->DropForeignKey('FK_nutritions_users', '{{%nutritions}}');
        $this->dropTable('{{%nutritions}}');
    }
}
