<?php

use yii\db\Migration;

/**
 * Handles adding check to table `exercise`.
 */
class m181015_065800_add_check_column_to_exercise_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('exercise', 'check', $this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('exercise', 'check');
    }
}
