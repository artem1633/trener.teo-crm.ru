<?php

use yii\db\Migration;

/**
 * Handles the creation of table `exercise_group`.
 */
class m181014_114311_create_exercise_group_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('exercise_group', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('exercise_group');
    }
}
