<?php

use yii\db\Migration;

/**
 * Handles the creation of table `nutritions_eating_products`.
 */
class m181220_105501_nutritions_eating_products extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    { 
        $tableOptions = null;
        if($this->db->driverName === 'mysql'){
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%nutritions_eating_products}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),           
            'portion' => $this->integer(11),
            'calorie' => $this->integer(11),
            

        ], $tableOptions);

       
         //$this->addForeignKey('auth_item_child_ibfk_1', '{{%auth_item_child}}', 'parent', '{{%auth_item}}', 'name', 'cascade', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()

    {
       
        $this->dropTable('{{%nutritions_eating_products}}');
    }
}