<?php
/**
 * Created by PhpStorm.
 * User: indigo
 * Date: 11/10/18
 * Time: 10:29 AM
 */

Yii::setAlias('webroot', dirname(dirname(__DIR__)) . '/web');
Yii::setAlias('images', '@webroot/images');
Yii::setAlias('avatars', '@webroot/avatars');
Yii::setAlias('doc_scans', '@webroot/uploads/doc_scans');