<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "coaching_completing".
 *
 * @property int $id
 * @property int $coaching_id Тренировка
 * @property int $client_id Клиент
 * @property string $created_at
 *
 * @property Users $client
 * @property Coaching $coaching
 */
class CoachingCompleting extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'coaching_completing';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['coaching_id', 'client_id'], 'integer'],
            [['created_at'], 'safe'],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['client_id' => 'id']],
            [['coaching_id'], 'exist', 'skipOnError' => true, 'targetClass' => Coaching::class, 'targetAttribute' => ['coaching_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'coaching_id' => 'Coaching ID',
            'client_id' => 'Client ID',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @param int $coaching_id
     * @param int $client_id
     * @return bool
     */
    public static function hasIsCompleted($coaching_id, $client_id)
    {
        $models = self::find()->where(['coaching_id' => $coaching_id, 'client_id' => $client_id])->limit(2)->orderBy('created_at desc')->all();
        if(count($models) >= 1) {
            /** @var CoachingCompleting $model */
            $model = $models[0];
            $now = time();
            $modelTime = strtotime($model->created_at);
            return ($now - $modelTime) < 86400;
        } else if(count($models) == 0) {
            return false;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Users::class, ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoaching()
    {
        return $this->hasOne(Coaching::class, ['id' => 'coaching_id']);
    }
}
