<?php

namespace app\models\form;

use Yii;
use app\models\NutritionComments;
use yii\base\Model;

/**
 * Class NutritionComment
 * @package app\models\form
 */
class NutritionComment extends Model
{
    /**
     * @var string
     */
    public $text;

    /**
     * @var int
     */
    public $nutritionId;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['text', 'nutritionId'], 'required'],
            [['nutritionId'], 'integer'],
            [['text'], 'string'],
        ];
    }

    /**
     * Оставляет комментарий
     * @return bool
     */
    public function comment()
    {
        $comment = new NutritionComments([
            'text' => $this->text,
            'author_id' => Yii::$app->user->getId(),
            'nutrition_id' => $this->nutritionId,
        ]);

        return $comment->save(false);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'text' => 'Комментарий'
        ];
    }
}