<?php

namespace app\models\form;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class UploadForm
 * @package app\models\form
 */
class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFile;

    /**
     * @var string
     */
    public $uploadPath = 'uploads/';

    /**
     * @var string[]
     */
    public $fullUploadedFilePath;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'maxFiles' => 10, 'maxSize' => 1024 * 1024 * 50],
        ];
    }

    public function upload()
    {
        if(file_exists ('uploads') == false) {
            mkdir('uploads');
        }

        if(file_exists ('uploads/avatars') ==  false) {
            mkdir('uploads/avatars');
        }

        if ($this->validate()) {
            foreach($this->imageFile as $file)
            {
                $fileName = Yii::$app->security->generateRandomString(32);
                $fullUploadedPath = $this->uploadPath . $fileName . '.' . $file->extension;
                $this->fullUploadedFilePath[] = $fullUploadedPath;
                $file->saveAs($fullUploadedPath);
            }
            return true;
        } else {
            return false;
        }
    }
}