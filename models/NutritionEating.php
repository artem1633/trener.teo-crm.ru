<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nutrition_eating".
 *
 * @property int $id
 * @property string $name
 * @property int $nutrition_id
 * @property string $begintime
 *
 * @property Nutritions $nutrition
 * @property NutritionEatingToProducts[] $nutritionEatingToProducts
 * @property NutritionsEatingProducts[] $nutritionsEatingProducts
 */
class NutritionEating extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nutrition_eating';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'nutrition_id', 'begintime'], 'required'],
            [['nutrition_id'], 'integer'],
            [['begintime'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['nutrition_id'], 'exist', 'skipOnError' => true, 'targetClass' => Nutritions::class, 'targetAttribute' => ['nutrition_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'НАЗВАНИЕ ПРИЕМА',
            'nutrition_id' => 'Нутритион ID',
            'begintime' => 'Время приема',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNutrition()
    {
        return $this->hasOne(Nutritions::class, ['id' => 'nutrition_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNutritionEatingToProducts()
    {
        return $this->hasMany(NutritionEatingToProducts::class, ['eating_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNutritionsEatingProducts()
    {
        return $this->hasMany(NutritionsEatingProducts::className(), ['nutrition_eating_id' => 'id']);
    }
}
