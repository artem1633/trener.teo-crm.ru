<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "exercise".
 *
 * @property int $id
 * @property string $name
 * @property string $video
 * @property string $photo
 * @property string $description
 * @property int $exercise_group_id
 * @property int $trener_id
 *
 * @property ExerciseGroup $exerciseGroup
 * @property Users $trener
 * @property Training[] $trainings
 */
class Exercise extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    
    public $file;

    public static function tableName()
    {
        return 'exercise';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'exercise_group_id'], 'required'],
            [['video', 'description'], 'string'],
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg',], 
            [['exercise_group_id', 'trener_id', 'check'], 'integer'],
            [['name', 'photo'], 'string', 'max' => 255],
            [['exercise_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => ExerciseGroup::class, 'targetAttribute' => ['exercise_group_id' => 'id']],
            [['trener_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['trener_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название упражнения',
            'video' => 'Ссылка на видео',
            'photo' => 'Прикрепленные фото',
            'description' => 'Описание упражнения',
            'exercise_group_id' => 'Группа упражнений',
            'trener_id' => 'Тренер',
            'file' => 'Фото',
            'check' => 'Поумолчанию',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->trener_id = Yii::$app->user->identity->id;
        }
        return parent::beforeSave($insert);
    }

    public function beforeDelete()
    {
        if ($this->photo != null && file_exists('uploads/' . $this->photo)) unlink(Yii::getAlias('uploads/' . $this->photo));
        $trainings = Training::find()->where(['exercise_id' => $this->id])->all();
        foreach ($trainings as $value) {
            $value->delete();
        }
        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExerciseGroup()
    {
        return $this->hasOne(ExerciseGroup::class, ['id' => 'exercise_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrener()
    {
        return $this->hasOne(Users::class, ['id' => 'trener_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainings()
    {
        return $this->hasMany(Training::class, ['exercise_id' => 'id']);
    }

    public function getGroupList()
    {
        $group = ExerciseGroup::find()->all();
        return ArrayHelper::map( $group , 'id', 'name');
    }

    public function getOrderGoodsData()
    {
        $query = Training::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
        ]);

        $query->andFilterWhere(['exercise_id' => $this->id]);
        return $dataProvider;
    }
}
