<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "nutritions_users".
 *
 * @property int $nutritions_id
 * @property int $users_id
 *
 * @property Nutritions $nutritions
 * @property Users $users
 */
class NutritionsUsers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nutritions_users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nutritions_id', 'users_id'], 'required'],
            [['nutritions_id', 'users_id'], 'integer'],
            [['nutritions_id', 'users_id'], 'unique', 'targetAttribute' => ['nutritions_id', 'users_id']],
            [['nutritions_id'], 'exist', 'skipOnError' => true, 'targetClass' => Nutritions::class, 'targetAttribute' => ['nutritions_id' => 'id']],
            [['users_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['users_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nutritions_id' => 'Программа питания',
            'users_id' => 'Пользователь',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNutritions()
    {
        return $this->hasOne(Nutritions::class, ['id' => 'nutritions_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasOne(Users::class, ['id' => 'users_id']);
    }

    /**
     * @param int $user_id
     * @return Coaching[]
     */
    public static function getAvailableNutritions($user_id)
    {
        $usesNutriotions = array_values(ArrayHelper::map(self::find()->where(['users_id' => $user_id])->all(), 'nutritions_id', 'nutritions_id'));
        return Nutritions::find()->andFilterWhere(['not in', 'id', $usesNutriotions])->andWhere(['trener_id' => Yii::$app->user->getId()])->all();
    }
}
