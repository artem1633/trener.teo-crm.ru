<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "coaching_users".
 *
 * @property int $coaching_id
 * @property int $users_id
 *
 * @property Coaching $coaching
 * @property Users $users
 */
class CoachingUsers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'coaching_users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['coaching_id', 'users_id'], 'required'],
            [['coaching_id', 'users_id'], 'integer'],
            [['coaching_id', 'users_id'], 'unique', 'targetAttribute' => ['coaching_id', 'users_id']],
            [['coaching_id'], 'exist', 'skipOnError' => true, 'targetClass' => Coaching::class, 'targetAttribute' => ['coaching_id' => 'id']],
            [['users_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['users_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'coaching_id' => 'Тренировка',
            'users_id' => 'Пользователь',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoaching()
    {
        return $this->hasOne(Coaching::class, ['id' => 'coaching_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasOne(Users::class, ['id' => 'users_id']);
    }

    /**
     * @param int $user_id
     * @return Coaching[]
     */
    public static function getAvailableCoachings($user_id)
    {
        $usesCoachings = array_values(ArrayHelper::map(self::find()->where(['users_id' => $user_id])->all(), 'coaching_id', 'coaching_id'));
        return Coaching::find()->andWhere(['not in', 'id', $usesCoachings])->andWhere(['trener_id' => Yii::$app->user->getId()])->all();
    }
}
