<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "payments".
 *
 * @property int $id
 * @property int $client_id Клиент
 * @property int $trainer_id Тренер
 * @property double $quantity Сумма
 * @property string $payments_date Дата совешения платежа
 * @property string $period
 *
 * @property bool $isPayed
 *
 * @property Users $client
 * @property Users $trainer
 */
class Payments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_id', 'trainer_id'], 'integer'],
            [['quantity'], 'number'],
            [['period'], 'string'],
            [['payments_date'], 'string', 'max' => 255],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['client_id' => 'id']],
            [['trainer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['trainer_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Клиент',
            'trainer_id' => 'Тренер',
            'quantity' => 'Сумма',
            'payments_date' => 'Дата оплаты',
            'period' => 'Период',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Users::class, ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainer()
    {
        return $this->hasOne(Users::class, ['id' => 'trainer_id']);
    }

    /**
     * @return array
     */
    public function getPeriodsLabels()
    {
        return ['all_time' => 'За все время', 'month' => 'Месяц', 'week' => 'Неделя', 'day' => 'День'];
    }

    /**
     * @return bool
     */
    public function getIsPayed()
    {
        // TODO: СДЕЛАТЬ ОБРАБОТКУ
       return false;
    }
}
