<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nutrition_eating_to_products".
 *
 * @property int $id
 * @property int $eating_id
 * @property int $product_id
 *
 * @property NutritionEating $eating
 * @property NutritionsEatingProducts $product
 */
class nutritionEatingToProducts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nutrition_eating_to_products';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['eating_id', 'product_id'], 'required'],
            [['eating_id', 'product_id'], 'integer'],
            [['eating_id'], 'exist', 'skipOnError' => true, 'targetClass' => NutritionEating::class, 'targetAttribute' => ['eating_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => NutritionsEatingProducts::class, 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'eating_id' => 'прием ID',
            'product_id' => 'продукт ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEating()
    {
        return $this->hasOne(NutritionEating::class, ['id' => 'eating_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(NutritionsEatingProducts::class, ['id' => 'product_id']);
    }
}
