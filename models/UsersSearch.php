<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Users;
use yii\helpers\ArrayHelper;

/**
 * UsersSearch represents the model behind the search form about `app\models\Users`.
 */
class UsersSearch extends Users
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'work_experience', 'referal', 'trener'], 'integer'],
            [['fio', 'register_date', 'address', 'photo', 'status', 'telephone', 'permission', 'login', 'password', 'gender', 'photos', 'birthday', 'education', 'education_scans', 'instagram', 'props', 'last_pay', 'information', 'occupation', 'training_experience', 'fitnes', 'training_goal'], 'safe'],
            [['cost', 'cost_month', 'cost_program', 'total_pay', 'referal_sum'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Users::find();

        $id = Yii::$app->user->identity->id;
        if (isset($id)) {
            $user = Functions::getUserRole($id);
            if ($user == "Тренер") {
                $query = Users::find() -> andWhere(['permission' => 'client']);
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'register_date' => $this->register_date,
            'birthday' => $this->birthday,
            'work_experience' => $this->work_experience,
            'cost' => $this->cost,
            'cost_month' => $this->cost_month,
            'cost_program' => $this->cost_program,
            'last_pay' => $this->last_pay,
            'total_pay' => $this->total_pay,
            'referal' => $this->referal,
            'referal_sum' => $this->referal_sum,
            'trener' => $this->trener,
        ]);

        $query->andFilterWhere(['like', 'fio', $this->fio])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'photo', $this->photo])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'telephone', $this->telephone])
            ->andFilterWhere(['like', 'permission', $this->permission])
            ->andFilterWhere(['like', 'login', $this->login])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'photos', $this->photos])
            ->andFilterWhere(['like', 'education', $this->education])
            ->andFilterWhere(['like', 'education_scans', $this->education_scans])
            ->andFilterWhere(['like', 'instagram', $this->instagram])
            ->andFilterWhere(['like', 'props', $this->props])
            ->andFilterWhere(['like', 'information', $this->information])
            ->andFilterWhere(['like', 'occupation', $this->occupation])
            ->andFilterWhere(['like', 'training_experience', $this->training_experience])
            ->andFilterWhere(['like', 'fitnes', $this->fitnes])
            ->andFilterWhere(['like', 'training_goal', $this->training_goal]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchTreners($params)
    {
        $query = Users::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'register_date' => $this->register_date,
            'birthday' => $this->birthday,
            'work_experience' => $this->work_experience,
            'cost' => $this->cost,
            'cost_month' => $this->cost_month,
            'cost_program' => $this->cost_program,
            'last_pay' => $this->last_pay,
            'total_pay' => $this->total_pay,
            'referal' => $this->referal,
            'referal_sum' => $this->referal_sum,
            'trener' => $this->trener,
        ]);

        $query->andFilterWhere(['like', 'fio', $this->fio])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'photo', $this->photo])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'telephone', $this->telephone])
            ->andFilterWhere(['like', 'permission', Users::USER_ROLE_TRENER])
            ->andFilterWhere(['like', 'login', $this->login])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'photos', $this->photos])
            ->andFilterWhere(['like', 'education', $this->education])
            ->andFilterWhere(['like', 'education_scans', $this->education_scans])
            ->andFilterWhere(['like', 'instagram', $this->instagram])
            ->andFilterWhere(['like', 'props', $this->props])
            ->andFilterWhere(['like', 'information', $this->information])
            ->andFilterWhere(['like', 'occupation', $this->occupation])
            ->andFilterWhere(['like', 'training_experience', $this->training_experience])
            ->andFilterWhere(['like', 'fitnes', $this->fitnes])
            ->andFilterWhere(['like', 'training_goal', $this->training_goal]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchMyTreners($params)
    {
        if(Yii::$app->user->identity->trener == null)
            $trenerId = -1;
        else
            $trenerId = Yii::$app->user->identity->trener;

        $query = Users::find()->where(['id' => $trenerId])->all();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

//        $this->load($params);
//
//        if (!$this->validate()) {
//            // uncomment the following line if you do not want to return any records when validation fails
//            // $query->where('0=1');
//            return $dataProvider;
//        }
//
//        $query->andFilterWhere([
//            'id' => $this->id,
//            'register_date' => $this->register_date,
//            'birthday' => $this->birthday,
//            'work_experience' => $this->work_experience,
//            'cost' => $this->cost,
//            'cost_month' => $this->cost_month,
//            'cost_program' => $this->cost_program,
//            'last_pay' => $this->last_pay,
//            'total_pay' => $this->total_pay,
//            'referal' => $this->referal,
//            'referal_sum' => $this->referal_sum,
//            'trener' => $this->trener,
//        ]);
//
//        $query->andFilterWhere(['like', 'fio', $this->fio])
//            ->andFilterWhere(['like', 'address', $this->address])
//            ->andFilterWhere(['like', 'photo', $this->photo])
//            ->andFilterWhere(['like', 'status', $this->status])
//            ->andFilterWhere(['like', 'telephone', $this->telephone])
//            ->andFilterWhere(['like', 'permission', Users::USER_ROLE_TRENER])
//            ->andFilterWhere(['like', 'login', $this->login])
//            ->andFilterWhere(['like', 'password', $this->password])
//            ->andFilterWhere(['like', 'gender', $this->gender])
//            ->andFilterWhere(['like', 'photos', $this->photos])
//            ->andFilterWhere(['like', 'education', $this->education])
//            ->andFilterWhere(['like', 'education_scans', $this->education_scans])
//            ->andFilterWhere(['like', 'instagram', $this->instagram])
//            ->andFilterWhere(['like', 'props', $this->props])
//            ->andFilterWhere(['like', 'information', $this->information])
//            ->andFilterWhere(['like', 'occupation', $this->occupation])
//            ->andFilterWhere(['like', 'training_experience', $this->training_experience])
//            ->andFilterWhere(['like', 'fitnes', $this->fitnes])
//            ->andFilterWhere(['like', 'training_goal', $this->training_goal]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchClients($params)
    {
        $query = Users::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'register_date' => $this->register_date,
            'birthday' => $this->birthday,
            'work_experience' => $this->work_experience,
            'cost' => $this->cost,
            'cost_month' => $this->cost_month,
            'cost_program' => $this->cost_program,
            'last_pay' => $this->last_pay,
            'total_pay' => $this->total_pay,
            'referal' => $this->referal,
            'referal_sum' => $this->referal_sum,
            'trener' => $this->trener,
        ]);


        $query->andFilterWhere(['like', 'fio', $this->fio])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'photo', $this->photo])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'telephone', $this->telephone])
            ->andFilterWhere(['like', 'permission', Users::USER_ROLE_CLIENT])
            ->andFilterWhere(['like', 'login', $this->login])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'photos', $this->photos])
            ->andFilterWhere(['like', 'education', $this->education])
            ->andFilterWhere(['like', 'education_scans', $this->education_scans])
            ->andFilterWhere(['like', 'instagram', $this->instagram])
            ->andFilterWhere(['like', 'props', $this->props])
            ->andFilterWhere(['like', 'information', $this->information])
            ->andFilterWhere(['like', 'occupation', $this->occupation])
            ->andFilterWhere(['like', 'training_experience', $this->training_experience])
            ->andFilterWhere(['like', 'fitnes', $this->fitnes])
            ->andFilterWhere(['like', 'training_goal', $this->training_goal]);

        return $dataProvider;
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function searchPaymentClients($params)
    {
        $query = Users::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $userIds = array_unique(array_values(ArrayHelper::map(Payments::find()->where(['trainer_id' => Yii::$app->user->getId()])->all(), 'client_id', 'client_id')));

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $userIds,
            'register_date' => $this->register_date,
            'birthday' => $this->birthday,
            'work_experience' => $this->work_experience,
            'cost' => $this->cost,
            'cost_month' => $this->cost_month,
            'cost_program' => $this->cost_program,
            'last_pay' => $this->last_pay,
            'total_pay' => $this->total_pay,
            'referal' => $this->referal,
            'referal_sum' => $this->referal_sum,
            'trener' => $this->trener,
        ]);


        $query->andFilterWhere(['like', 'fio', $this->fio])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'photo', $this->photo])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'telephone', $this->telephone])
            ->andFilterWhere(['like', 'permission', Users::USER_ROLE_CLIENT])
            ->andFilterWhere(['like', 'login', $this->login])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'photos', $this->photos])
            ->andFilterWhere(['like', 'education', $this->education])
            ->andFilterWhere(['like', 'education_scans', $this->education_scans])
            ->andFilterWhere(['like', 'instagram', $this->instagram])
            ->andFilterWhere(['like', 'props', $this->props])
            ->andFilterWhere(['like', 'information', $this->information])
            ->andFilterWhere(['like', 'occupation', $this->occupation])
            ->andFilterWhere(['like', 'training_experience', $this->training_experience])
            ->andFilterWhere(['like', 'fitnes', $this->fitnes])
            ->andFilterWhere(['like', 'training_goal', $this->training_goal]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchMyClients($params)
    {
        $query = Users::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'register_date' => $this->register_date,
            'birthday' => $this->birthday,
            'work_experience' => $this->work_experience,
            'cost' => $this->cost,
            'cost_month' => $this->cost_month,
            'cost_program' => $this->cost_program,
            'last_pay' => $this->last_pay,
            'total_pay' => $this->total_pay,
            'referal' => $this->referal,
            'referal_sum' => $this->referal_sum,
            'trener' => Yii::$app->user->getId(),
        ]);

//        var_dump($query->createCommand()->getRawSql());
//        exit;

        $query->andFilterWhere(['like', 'fio', $this->fio])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'photo', $this->photo])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'telephone', $this->telephone])
            ->andFilterWhere(['like', 'permission', Users::USER_ROLE_CLIENT])
            ->andFilterWhere(['like', 'login', $this->login])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'photos', $this->photos])
            ->andFilterWhere(['like', 'education', $this->education])
            ->andFilterWhere(['like', 'education_scans', $this->education_scans])
            ->andFilterWhere(['like', 'instagram', $this->instagram])
            ->andFilterWhere(['like', 'props', $this->props])
            ->andFilterWhere(['like', 'information', $this->information])
            ->andFilterWhere(['like', 'occupation', $this->occupation])
            ->andFilterWhere(['like', 'training_experience', $this->training_experience])
            ->andFilterWhere(['like', 'fitnes', $this->fitnes])
            ->andFilterWhere(['like', 'training_goal', $this->training_goal]);

        return $dataProvider;
    }

    public function searchByClient($params, $id)
    {
        $query = Users::find()->where(['trener' => $id, 'permission' => 'client']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'register_date' => $this->register_date,
            'birthday' => $this->birthday,
            'work_experience' => $this->work_experience,
            'cost' => $this->cost,
            'cost_month' => $this->cost_month,
            'cost_program' => $this->cost_program,
            'last_pay' => $this->last_pay,
            'total_pay' => $this->total_pay,
            'referal' => $this->referal,
            'referal_sum' => $this->referal_sum,
            'trener' => $this->trener,
        ]);

        $query->andFilterWhere(['like', 'fio', $this->fio])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'photo', $this->photo])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'telephone', $this->telephone])
            ->andFilterWhere(['like', 'permission', $this->permission])
            ->andFilterWhere(['like', 'login', $this->login])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'photos', $this->photos])
            ->andFilterWhere(['like', 'education', $this->education])
            ->andFilterWhere(['like', 'education_scans', $this->education_scans])
            ->andFilterWhere(['like', 'instagram', $this->instagram])
            ->andFilterWhere(['like', 'props', $this->props])
            ->andFilterWhere(['like', 'information', $this->information])
            ->andFilterWhere(['like', 'occupation', $this->occupation])
            ->andFilterWhere(['like', 'training_experience', $this->training_experience])
            ->andFilterWhere(['like', 'fitnes', $this->fitnes])
            ->andFilterWhere(['like', 'training_goal', $this->training_goal]);

        return $dataProvider;
    }
}
