<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "training".
 *
 * @property int $id
 * @property int $exercise_id
 * @property double $weight
 * @property int $repitition
 * @property int $approach
 * @property int $trener_id
 * @property int $coaching_id
 * @property int $with
 *
 * @property Coaching $coaching
 * @property Exercise $exercise
 * @property Users $trener
 */
class Training extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'training';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['exercise_id'], 'required'],
            [['exercise_id', 'repitition', 'approach', 'trener_id', 'coaching_id', 'with'], 'integer'],
            [['weight'], 'string', 'max' => 255],
            [['exercise_id'], 'exist', 'skipOnError' => true, 'targetClass' => Exercise::class, 'targetAttribute' => ['exercise_id' => 'id']],
            [['trener_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['trener_id' => 'id']],
            [['coaching_id'], 'exist', 'skipOnError' => true, 'targetClass' => Coaching::class, 'targetAttribute' => ['coaching_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'exercise_id' => 'Название упражнения',
            'weight' => 'Вес',
            'repitition' => 'Количество повторений ',
            'approach' => 'Количество подходов',
            'with' => 'Объединить с',
            'trener_id' => 'Тренер',
            'coaching_id' => 'Coaching ID',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->trener_id = Yii::$app->user->identity->id;
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoaching()
    {
        return $this->hasOne(Coaching::class, ['id' => 'coaching_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExercise()
    {
        return $this->hasOne(Exercise::class, ['id' => 'exercise_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrener()
    {
        return $this->hasOne(Users::class, ['id' => 'trener_id']);
    }

    public function getExerciseList()
    {
        $exercise = Exercise::find()->all();
        return ArrayHelper::map( $exercise , 'id', 'name');
    }
}
