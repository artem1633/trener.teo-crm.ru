<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "coaching".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $trener_id
 *
 * @property Users $trener
 * @property CoachingUsers[] $coachingUsers
 * @property Users[] $users
 * @property Training[] $trainings
 */
class Coaching extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'coaching';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['trener_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['trener_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['trener_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'description' => 'Описание',
            'trener_id' => 'Тренер',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->trener_id = Yii::$app->user->identity->id;
        }
        return parent::beforeSave($insert);
    }

    public function beforeDelete()
    {
        $trainings = Training::find()->where(['coaching_id' => $this->id])->all();
        foreach ($trainings as $value) {
            $value->delete();
        }
        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrener()
    {
        return $this->hasOne(Users::class, ['id' => 'trener_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoachingUsers()
    {
        return $this->hasMany(CoachingUsers::className(), ['coaching_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(Users::className(), ['id' => 'users_id'])->viaTable('coaching_users', ['coaching_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainings()
    {
        return $this->hasMany(Training::class, ['coaching_id' => 'id']);
    }

    public function getOrderTrainingData()
    {
        $query = Training::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
        ]);

        $query->andFilterWhere(['coaching_id' => $this->id]);
        return $dataProvider;
    }

    /**
     * Делает запись о выполненой тренировке
     * @return bool
     */
    public function makeCompleted()
    {
        $complete = new CoachingCompleting([
            'client_id' => Yii::$app->user->getId(),
            'coaching_id' => $this->id,
        ]);

        return $complete->save(false);
    }

    /**
     * Возвращает ID используемых упражнений
     * @return array
     */
    public function getUsingExercisesPks()
    {
        return array_values(ArrayHelper::map(Training::find()->where(['coaching_id' => $this->id])->all(), 'id', 'exercise_id'));
    }
}
