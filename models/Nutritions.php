<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nutritions".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $protein
 * @property int $fat
 * @property int $carbohydrate
 * @property int $trener_id
 * @property int $status
 *
 * @property NutritionComments[] $nutritionComments
 * @property NutritionEating[] $nutritionEatings
 * @property Users $trener
 */
class Nutritions extends \yii\db\ActiveRecord
{

    private $_eating;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nutritions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['protein', 'fat', 'carbohydrate', 'trener_id', 'status'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['Eating'], 'safe'],
            [['description'], 'string', 'max' => 3000],
            [['trener_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['trener_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название программы',
            'description' => 'Общие рекомендации',
            'protein' => 'Белки',
            'fat' => 'Жиры',
            'carbohydrate' => 'Углеводы',
            'trener_id' => 'Trener ID',
            'status' => 'Status',
            'eating' => 'Eating',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->trener_id = Yii::$app->user->identity->id;
        }

        if($this->status == null)
        {
            $this->status = 1;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNutritionComments()
    {
        return $this->hasMany(NutritionComments::class, ['nutrition_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNutritionEatings()
    {
        return $this->hasMany(NutritionEating::class, ['nutrition_id' => 'id']);
    }

    public function setNutritionEatings($eatings)
    {
        unset($eatings['id']); // remove the hidden "new Parcel" row
        $this->nutritionEatings = [];
        foreach ($eatings as $key => $eating) {
                $this->nutritionEatings[$key] = $this->getNutritionEatings($key);
                $this->nutritionEatings[$key]=$eating;
            /*if (is_array($eating)) {
            } elseif ($eating instanceof Eating) {
                $this->nutritionEatings[$eating->id] = $eating;
            }*/
        }
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrener()
    {
        return $this->hasOne(Users::class, ['id' => 'trener_id']);
    }

    public function getEating()
    {
        return $this->_eating;
    }

    /*public function setNutritionEatings($eating)
    {
        if ($eating instanceof Eating) {
            $this->_eating = $eating;
        } else if (is_array($eating)) {
            $this->_eating->setAttributes($eating);
        }
    }*/

    private function getAllModels()
    {
        $models = [
            'Eating' => $this->eating,
        ];
        /*foreach ($this->parcels as $id => $parcel) {
            $models['Parcel.' . $id] = $this->parcels[$id];
        }*/
        return $models;
    }
}
