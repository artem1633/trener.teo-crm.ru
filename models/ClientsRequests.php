<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "clients_requests".
 *
 * @property int $client_id Клиент (Делает запрос)
 * @property int $trainer_id Тренер (Отвечает на запрос)
 * @property int $accepted Ответ
 * @property string $created_at
 *
 * @property Users $client
 * @property Users $trainer
 */
class ClientsRequests extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clients_requests';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_id', 'trainer_id'], 'required'],
            [['client_id', 'trainer_id', 'accepted'], 'integer'],
            [['created_at'], 'safe'],
            [['client_id', 'trainer_id'], 'unique', 'targetAttribute' => ['client_id', 'trainer_id']],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['client_id' => 'id']],
            [['trainer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['trainer_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'client_id' => 'Client ID',
            'trainer_id' => 'Trainer ID',
            'accepted' => 'Accepted',
            'created_at' => 'Created At',
        ];
    }

    /**
     * Имеется ли запрос по следующим параметрам:
     * @param int $client_id
     * @param int $trainer_id
     * @return bool
     */
    public static function hasRequest($client_id, $trainer_id)
    {
        return boolval(self::findOne(['client_id' => $client_id, 'trainer_id' => $trainer_id]));
    }

    /**
     * Одобрить запрос
     * @return bool
     */
    public function accept()
    {
        $this->client->trener = $this->trainer_id;
        $result = $this->client->save();
        $this->accepted = $result == true ? 1 : null;
        return $this->save(false);
    }

    /**
     * Отказать в запросе
     * @return bool
     */
    public function refuse()
    {
        $this->client->trener = $this->trainer_id;
        $result = $this->client->save();
        $this->accepted = $result == true ? 0 : null;
        return $this->save(false);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Users::class, ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainer()
    {
        return $this->hasOne(Users::class, ['id' => 'trainer_id']);
    }
}
