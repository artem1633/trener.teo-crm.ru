<?php
/**
 * Created by PhpStorm.
 * User: indigo
 * Date: 11/9/18
 * Time: 11:28 AM
 */
namespace app\models;

use app\models\Users;
use Yii;
use yii\helpers\ArrayHelper;

class Functions {

    public static function rusDate($d, $format = 'j %MONTH% Y', $offset = 0)
    {
        $montharr = array('января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');
        $dayarr = array('понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота', 'воскресенье');

        $d += 3600 * $offset;

        $sarr = array('/%MONTH%/i', '/%DAYWEEK%/i');
        $rarr = array( $montharr[date("m", $d) - 1], $dayarr[date("N", $d) - 1] );

        $format = preg_replace($sarr, $rarr, $format);
        return date($format, $d);
    }


    public static function getUserRole($id) {
        $user = Users::find() -> where(['id' => $id]) -> one();
        if($user->permission == 'administrator') return 'Администратор';
        if($user->permission == 'client') return 'Клиент';
        if($user->permission == 'trener') return 'Тренер';
    }


    public static function generateRandomString($length = 40) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public  static function getTrainerClients() {

        if (isset(Yii::$app->user->identity->id)) {
//            $coach_clients = Users::find() -> all();
            $coach_clients = Users::find() -> where(['trener' => Yii::$app->user->identity->id]) -> all();
            return $coach_clients;
//            return ArrayHelper::map($coach_clients, 'id', 'fio');
        } else {
            return "";
        }
    }

    public  static function getTrainersList() {

        if (isset(Yii::$app->user->identity->id)) {
            $trainers = Users::find() -> where(['permission' => 'trener']) -> all();
//            return $coach_clients;
            return ArrayHelper::map($trainers, 'id', 'fio');
        } else {
            return "";
        }
    }
    public  static function getClientsList() {

        if (isset(Yii::$app->user->identity->id)) {
            $clients = Users::find() -> where(['permission' => 'client']) -> all();
            return ArrayHelper::map($clients, 'id', 'fio');
        } else {
            return "";
        }
    }


    public static function additionalPhotosHtmlEdit($education_scans) {
        $html_array = [];
        if (!empty($education_scans)) {
            $education_scans = json_decode($education_scans, true);
            foreach ($education_scans as $education_scan) {
                $html_array[] = "<img src='" . $education_scan . "' class='file-preview-image education_scans' style='width: 200px'>";
//                $html_array[] = "<img src='/web/" . $education_scan . "' class='file-preview-image education_scans' style='width: 200px'>";
            }
//        $html = implode(', ', $html_array);
//            \Yii::info('additionalPhotosHtmlEdit', 'test');
//            \Yii::info($html_array, 'test');
        }
        return $html_array;
    }

    public static function getUserImg($id) {
        $user = Users::find() -> where(['id' => $id]) -> one();
        return $user -> photo;
    }

    public static function getUser($id) {
        $user = Users::find() -> where(['id' => $id]) -> one();
        return $user;
    }

    public static function getTrainer($id) {
        $user = Users::find() -> where(['id' => $id]) -> one();
        return $user -> fio;
    }

    public static function getPeriod($name) {

        if ($name == 'all_time') return 'За все время';
        if ($name == 'month') return 'Месяц';
        if ($name == 'week') return 'Неделя';
        if ($name == 'day') return 'День';
    }
}