<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "chat".
 *
 * @property int $id
 * @property int $client_id Клиент
 * @property int $trainer_id Тренер
 * @property string $chat_key Ключ чата
 * @property string $message Сообщение
 * @property string $created_date Дата создания и время создания
 * @property int $created_by Кто написал
 *
 * @property Users $client
 * @property Users $trainer
 */
class Chat extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'chat';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_id', 'trainer_id'], 'required'],
            [['client_id', 'trainer_id', 'created_by'], 'integer'],
            [['message'], 'string'],
            [['chat_key', 'created_date'], 'string', 'max' => 255],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['client_id' => 'id']],
            [['trainer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['trainer_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Клиент',
            'trainer_id' => 'Тренер',
            'chat_key' => 'Chat Key',
            'message' => 'Сообщение',
            'created_date' => 'Дата',
            'created_by' => 'Создан',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Users::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainer()
    {
        return $this->hasOne(Users::className(), ['id' => 'trainer_id']);
    }
}
