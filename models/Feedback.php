<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "feedback".
 *
 * @property int $id
 * @property int $client_id Клиент
 * @property int $trainer_id Тренер
 * @property double $rating Рейтинг
 * @property string $client_img Фото клиента
 * @property string $message Сообщение
 * @property string $created_date Дата создания и время создания
 * @property int $created_by Кто написал
 *
 * @property Users $client
 * @property Users $trainer
 */
class Feedback extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'feedback';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_id', 'trainer_id', 'created_by'], 'integer'],
            [['rating'], 'number'],
            [['message'], 'string'],
            [['client_img', 'created_date'], 'string', 'max' => 255],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['client_id' => 'id']],
            [['trainer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['trainer_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Client ID',
            'trainer_id' => 'Trainer ID',
            'rating' => 'Rating',
            'client_img' => 'Client Img',
            'message' => 'Message',
            'created_date' => 'Created Date',
            'created_by' => 'Created By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Users::class, ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainer()
    {
        return $this->hasOne(Users::class, ['id' => 'trainer_id']);
    }
}
