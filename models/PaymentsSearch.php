<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Payments;

/**
 * PaymentsSearch represents the model behind the search form about `app\models\Payments`.
 */
class PaymentsSearch extends Payments
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'client_id', 'trainer_id'], 'integer'],
            [['quantity'], 'number'],
            [['payments_date', 'period'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Payments::find();
        $id = Yii::$app->user->identity->id;
        if (isset($id)) {
            $user = Functions::getUserRole($id);
            if ($user == "Тренер") {
                $query = Payments::find() -> where(['trainer_id' => $id]);
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'client_id' => $this->client_id,
            'trainer_id' => $this->trainer_id,
            'quantity' => $this->quantity,
        ]);

        $query->andFilterWhere(['like', 'payments_date', $this->payments_date])
            ->andFilterWhere(['like', 'period', $this->period]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchMy($params)
    {
        $query = Payments::find();
        $id = Yii::$app->user->identity->id;
        if (isset($id)) {
            $user = Functions::getUserRole($id);
            if ($user == "Тренер") {
                $query = Payments::find() -> where(['trainer_id' => $id]);
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'client_id' => $id,
            'trainer_id' => $this->trainer_id,
            'quantity' => $this->quantity,
        ]);

        $query->andFilterWhere(['like', 'payments_date', $this->payments_date])
            ->andFilterWhere(['like', 'period', $this->period]);

        return $dataProvider;
    }
}
