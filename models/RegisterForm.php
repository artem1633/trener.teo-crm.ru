<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * RegisterForm is the model behind the register form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class RegisterForm extends Model
{
    public $email;
    public $password;
    public $name;
    public $surname;
    private $_user = false;

    public $referralId;

    /**
     * @var Users|null
     */
    public $referrer;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // email and password are both required
            [['email','name', 'surname', 'password'], 'required'],
            ['email', 'email'],
            ['email', 'trim'],
            [['name', 'surname','email', 'password'], 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'Этот Email уже используется.'],
            ['password', 'string', 'min' => 6],
            [['referralId'], 'safe'],
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0);
        }
        return false;
    }

    public function register()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->fio = $this->name.' '.$this->surname;
        $user->login = 'site'.time();
        /*if(isset($_GET['type']) && $_GET['type']=='trener'){
            $user->permission = 'trener';
        }*/
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $result = $user->save();

        if($result){

            if($this->referralId != null)
            {
                $referrer = Users::findOne($this->referralId);
                if($referrer != null)
                {
                    $this->referrer = $referrer;
                    $referral = new AffiliateProgram();
                    $referral->owner_id = $this->referralId;
                    $referral->registered_id = $user->id;
                    $referral->register_date = date('Y-m-d H:i:s');
                    $referral->referral_link = $referrer->referralUrl;
                    $referral->save(false);
                }
            }

            return $user;
        }

        return null;
    }
    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }
    public function attributeLabels()
    {
        return [
            'email'=>'Email',
            'password'=>'Пароль',
            'name'=>'Имя', 
            'surname'=>'Фамилия',
        ];
    }
}
