<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_notifications_marker".
 *
 * @property int $id
 * @property int $user_id Пользователь
 * @property int $client_requests_trainer Уведомление о новом запросе клиента
 *
 * @property Users $user
 */
class UserNotificationsMarker extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_notifications_marker';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'client_requests_trainer'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'client_requests_trainer' => 'Client Requests Trainer',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::class, ['id' => 'user_id']);
    }
}
