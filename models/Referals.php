<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "referals".
 *
 * @property int $id
 * @property int $referer_id
 * @property int $referal_id
 * @property string $created
 *
 * @property Users $referal
 * @property Users $referer
 */
class Referals extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'referals';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['referer_id', 'referal_id'], 'required'],
            [['referer_id', 'referal_id'], 'integer'],
            [['created'], 'safe'],
            [['referal_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['referal_id' => 'id']],
            [['referer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['referer_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'referer_id' => 'Referer ID',
            'referal_id' => 'Referal ID',
            'created' => 'Created',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReferal()
    {
        return $this->hasOne(Users::class, ['id' => 'referal_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReferer()
    {
        return $this->hasOne(Users::class, ['id' => 'referer_id']);
    }
}
