<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Chat;

/**
 * ChatSearch represents the model behind the search form about `app\models\Chat`.
 */
class ChatSearch extends Chat
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'client_id', 'trainer_id', 'created_by'], 'integer'],
            [['chat_key', 'message', 'created_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Chat::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'client_id' => $this->client_id,
            'trainer_id' => $this->trainer_id,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'chat_key', $this->chat_key])
            ->andFilterWhere(['like', 'message', $this->message])
            ->andFilterWhere(['like', 'created_date', $this->created_date]);

        return $dataProvider;
    }

    public function searchByChat($client_id, $trainer_id)
    {
        $query = Chat::find()
            ->where(['trainer_id' => $trainer_id])
            ->andWhere(['client_id' => $client_id])
            ->orderBy(['id' => SORT_DESC])
            ->all();

        return $query;
    }
}
