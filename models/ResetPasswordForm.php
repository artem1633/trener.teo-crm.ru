<?php
namespace app\models;

use Yii;
use yii\base\InvalidArgumentException;
use yii\base\Model;
use app\models\User;

/**
 * Password reset form
 */
class ResetPasswordForm extends Model
{
    public $password;

    /**
     * @var \app\models\User
     */
    private $_user;


    /**
     * Creates a form model given a token.
     *
     * @param string $code
     * @param array $config name-value pairs that will be used to initialize the object properties
     * @throws InvalidArgumentException if token is empty or not valid
     */
    public function __construct($code, $config = [])
    {
        if (empty($code) || !is_string($code)) {
            throw new InvalidArgumentException('Код восстановления пароля не может быть пустым.');
        }
        $this->_user = User::findByPasswordResetToken($code);
        if (!$this->_user) {
            throw new InvalidArgumentException('Не правильный код.');
        } else {
            Yii::$app->user->login($this->_user);
        }
        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    public function attributeLabels()
    {
        return [
            'password' => 'Пароль',
        ];
    }

    /**
     * Resets password.
     *
     * @return bool if password was reset.
     */
    public function resetPassword()
    {
        $user = $this->_user;
        $user->setPassword($this->password);
        $user->removePasswordResetToken();

        return $user->save(false);
    }
}
