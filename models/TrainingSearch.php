<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Training;
use yii\helpers\ArrayHelper;

/**
 * TrainingSearch represents the model behind the search form about `app\models\Training`.
 */
class TrainingSearch extends Training
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'exercise_id', 'repitition', 'approach', 'trener_id'], 'integer'],
            [['weight'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Training::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'exercise_id' => $this->exercise_id,
            'weight' => $this->weight,
            'repitition' => $this->repitition,
            'approach' => $this->approach,
            'trener_id' => $this->trener_id,
        ]);

        return $dataProvider;
    }

    public function searchByUser($params,$id)
    {
        $query = Training::find()->where(['trener_id' => $id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'exercise_id' => $this->exercise_id,
            'weight' => $this->weight,
            'repitition' => $this->repitition,
            'approach' => $this->approach,
            'trener_id' => $this->trener_id,
        ]);

        return $dataProvider;
    }

    public function searchByCoach($params,$id)
    {
        $query = Coaching::find()->where(['trener_id' => $id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        return $dataProvider;
    }

    public function searchCoach($params)
    {
        $query = Coaching::find();

        if(Yii::$app->user->identity->permission == Users::USER_ROLE_TRENER){
            $query->andWhere(['trener_id' => Yii::$app->user->getId()]);
        } else if(Yii::$app->user->identity->permission == Users::USER_ROLE_CLIENT) {
            $coachinIds = ArrayHelper::getColumn(CoachingUsers::find()->where(['users_id' => Yii::$app->user->getId()])->all(), 'coaching_id');

            $query->andWhere(['id' => $coachinIds]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        return $dataProvider;
    }

//    public function searchByClient($params,$id)
//    {
//        $query = Training::find()->where(['client_id' => $id]);
//
//        $dataProvider = new ActiveDataProvider([
//            'query' => $query,
//        ]);
//
//        $this->load($params);
//
//        if (!$this->validate()) {
//            // uncomment the following line if you do not want to return any records when validation fails
//            // $query->where('0=1');
//            return $dataProvider;
//        }
//
//        $query->andFilterWhere([
//            'id' => $this->id,
//            'exercise_id' => $this->exercise_id,
//            'weight' => $this->weight,
//            'repitition' => $this->repitition,
//            'approach' => $this->approach,
//            'trener_id' => $this->trener_id,
//        ]);
//
//        return $dataProvider;
//    }
}
