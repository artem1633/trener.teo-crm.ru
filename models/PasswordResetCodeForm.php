<?php
namespace app\models;

use Yii;
use yii\base\Model;
use app\models\User;

/**
 * Password reset request form
 */
class PasswordResetCodeForm extends Model
{
    public $password_reset_token;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['password_reset_token', 'trim'],
            ['password_reset_token', 'required'],
            ['password_reset_token', 'exist',
                'targetClass' => '\app\models\User',
                'message' => 'Ваш код не верен.'
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'password_reset_token' => 'Код восстановления',
        ];
    }
    
}
