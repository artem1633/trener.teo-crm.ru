<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "nutrition_comments".
 *
 * @property int $id
 * @property int $nutrition_id
 * @property string $created
 * @property int $author_id
 * @property string $text
 * @property string $photo
 *
 * @property Nutritions $nutrition
 * @property Users $author
 */
class NutritionComments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nutrition_comments';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nutrition_id', 'author_id'], 'required'],
            [['nutrition_id', 'author_id'], 'integer'],
            [['created'], 'safe'],
            [['text'], 'string', 'max' => 3000],
            [['photo'], 'string', 'max' => 255],
            [['nutrition_id'], 'exist', 'skipOnError' => true, 'targetClass' => Nutritions::class, 'targetAttribute' => ['nutrition_id' => 'id']],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['author_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nutrition_id' => 'Nutrition ID',
            'created' => 'Created',
            'author_id' => 'Author ID',
            'text' => 'Text',
            'photo' => 'Photo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNutrition()
    {
        return $this->hasOne(Nutritions::class, ['id' => 'nutrition_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Users::class, ['id' => 'author_id']);
    }
}
