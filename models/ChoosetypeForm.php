<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\User;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class ChoosetypeForm extends Model
{
    public $permission;
    public $code;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // email and password are both required
            [['permission'], 'required', 'message' => 'Выберите тип профиля.'],
            ['permission', 'in', 'range' => ['client','trener']],
            ['code', 'trim'],
            ['code', 'string', 'min' => 6],
            ['code', 'exist','targetClass' => '\app\models\User', 'targetAttribute' => 'mycode', 'message' => 'Этот код не найден.'],

        ];
    }

    public function choosetype()
    {
        if($this->code){
            $this->code=str_replace('-','',$this->code);
        }
        if (!$this->validate()) {
            return null;
        }
        $user = Yii::$app->user;

        $user->identity->permission = $this->permission;
        if($this->permission=='trener'){
            $user->identity->setReferalTo($this->code);
        }
        return $user->identity->save() ? $user : false;
    }
    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }
    public function attributeLabels()
    {
        return [
            'usertype'=>'Тип профиля',
            'code'=>'Промо-код',
        ];
    }
}
