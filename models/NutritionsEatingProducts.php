<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nutritions_eating_products".
 *
 * @property int $id
 * @property string $name
 * @property int $portion
 * @property int $calorie
 * @property int $nutrition_eating_id Прием питания
 *
 * @property EatingProducts[] $eatingProducts
 * @property NutritionEatingToProducts[] $nutritionEatingToProducts
 * @property NutritionEating $nutritionEating
 */
class NutritionsEatingProducts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nutritions_eating_products';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['portion', 'calorie', 'nutrition_eating_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['nutrition_eating_id'], 'exist', 'skipOnError' => true, 'targetClass' => NutritionEating::class, 'targetAttribute' => ['nutrition_eating_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'portion' => 'Порция',
            'calorie' => 'Калорийность',
            'nutrition_eating_id' => 'Прием пищи',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEatingProducts()
    {
        return $this->hasMany(EatingProducts::class, ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNutritionEatingToProducts()
    {
        return $this->hasMany(NutritionEatingToProducts::class, ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNutritionEating()
    {
        return $this->hasOne(NutritionEating::class, ['id' => 'nutrition_eating_id']);
    }
}
