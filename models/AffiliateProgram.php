<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "affiliate_program".
 *
 * @property int $id
 * @property int $owner_id Пользователь, хозяин реферальной ссылки
 * @property int $registered_id Зарегистрировавшийся пользователь
 * @property string $register_date Дата и время регистрации
 * @property string $referral_link Ссылка, по которой была проведена регистрация
 *
 * @property Users $owner
 * @property Users $registered
 */
class AffiliateProgram extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'affiliate_program';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['owner_id', 'registered_id'], 'integer'],
            [['register_date'], 'safe'],
            [['referral_link'], 'string', 'max' => 255],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['owner_id' => 'id']],
            [['registered_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['registered_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'owner_id' => 'Owner ID',
            'registered_id' => 'Registered ID',
            'register_date' => 'Register Date',
            'referral_link' => 'Referral Link',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(Users::class, ['id' => 'owner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegistered()
    {
        return $this->hasOne(Users::class, ['id' => 'registered_id']);
    }
}
