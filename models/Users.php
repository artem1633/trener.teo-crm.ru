<?php

namespace app\models;

use app\values\Country;
use app\values\InstagramProfileUrl;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $fio ФИО
 * @property string $register_date Дата регистрации
 * @property string $country Страна (ISO 3166-1)
 * @property string $address Адрес, Город
 * @property string $photo Заглавное фото профиля
 * @property int $status Статус
 * @property string $telephone Телефон
 * @property string $permission Должность
 * @property string $login Логин
 * @property string $password Пароль
 * @property int $gender Пол
 * @property string $photos Фотографии
 * @property string $birthday Дата рождение
 * @property int $work_experience Стаж работы
 * @property string $education Образование
 * @property string $education_scans Сканы документов об образовании
 * @property string $instagram Ссылка на Instagram-профиль
 * @property double $cost Стоимость занятия
 * @property double $cost_month Стоимость месяца тренировок
 * @property double $cost_program Стоимость программы тренировок
 * @property string $props Реквизиты
 * @property string $last_pay Дата последней оплаты
 * @property double $total_pay Общая сумма оплат
 * @property int $referal Кол-во тренеров , привлеченных по реферальной програме
 * @property double $referal_sum Cумма бонусов по реферальной программе
 * @property int $trener Тренер клиента
 * @property string $information Информация обо мне
 * @property string $occupation Род деятельности
 * @property string $training_experience Стаж тренировок
 * @property string $fitnes Фитнес-клуб
 * @property string $training_goal Цель тренировок
 *
 * @property boolean $isAdmin
 * @property string $referralUrl
 * @property string $instagramProfileUrl
 * @property string $countryName
 *
 * @property \app\models\Payments $payments
 * @property \app\models\AffiliateProgram $myReferrer
 * @property \app\models\AffiliateProgram[] $myReferrals
 * @property Coaching[] $coachings
 * @property Nutritions[] $nutritions
 * @property CoachingUsers[] $coachingUsers
 * @property Users $myTrainer
 * @property ClientsRequests $clientRequests
 * @property ClientsRequests $requests
 * @property CoachingCompleting[] $coachingCompletings
 * @property UserNotificationsMarker $userNotificationsMarkers
 */
class Users extends \yii\db\ActiveRecord
{
    const USER_ROLE_ADMIN = 'administrator';
    const USER_ROLE_CLIENT = 'client';
    const USER_ROLE_TRENER = 'trener';

    const USER_MALE = 1;
    const USER_FEMALE = 0;

    const NO_USER_PHOTO_PATH = 'images/nouser.png';

    public $new_password;
    public $file;
    public $files;
    public $doc_scans;
    public $client_year;
    public $client_month;
    public $trainer_year;
    public $trainer_month;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fio', 'password', 'login'], 'required'],
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
            [['files'], 'file', 'maxFiles' => 5, 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
            [['doc_scans'], 'file', 'maxFiles' => 5, 'skipOnEmpty' => true],
            [['register_date', 'birthday', 'last_pay'], 'safe'],
            [['country', 'address', 'photos', 'information', 'occupation', 'education_scans'], 'string'],
            ['photo', 'default', 'value' => self::NO_USER_PHOTO_PATH],
            [
                [
                    'status',
                    'gender',
                    'referal',
                    'trener',
                    'client_year',
                    'client_month',
                    'trainer_year',
                    'trainer_month'
                ],
                'integer'
            ],
            [
                [
                    'cost',
                    'cost_month',
                    'cost_program',
                    'total_pay',
                    'referal_sum'
                ],
                'number'
            ],
            [
                [
                    'fio',
                    'photo',
                    'telephone',
                    'permission',
                    'login',
                    'password',
                    'education',
                    'instagram',
                    'props',
                    'training_experience',
                    'fitnes',
                    'training_goal',
                    'new_password',
                    'work_experience',
                ],
                'string', 'max' => 255
            ],
            [['login','email'], 'unique'],
            [['email'], 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'ФИО',
            'register_date' => 'Дата регистрации',
            'country' => 'Страна',
            'address' => 'Адрес, Город',
            'photo' => 'Заглавное фото профиля',
            'status' => 'Статус',
            'telephone' => 'Телефон',
            'permission' => 'Должность',
            'login' => 'E-mail',
            'password' => 'Пароль',
            'gender' => 'Пол',
            'photos' => 'Фото Портфолио',
            'birthday' => 'Дата рождение',
            'work_experience' => 'Стаж работы тренером:',
            'education' => 'Образование',
            'education_scans' => 'Документы об образовании:',
            'instagram' => 'Профиль в Instagram',
            'cost' => 'Стоимость занятия',
            'cost_month' => 'Стоимость месяца тренировок',
            'cost_program' => 'Стоимость программы тренировок',
            'props' => 'Реквизиты',
            'last_pay' => 'Дата последней оплаты',
            'total_pay' => 'Общая сумма оплат',
            'referal' => 'Кол-во тренеров , привлеченных по реферальной програме',
            'referal_sum' => 'Cумма бонусов по реф. программе',
            'trener' => 'Тренер клиента',
            'information' => 'Информация обо мне',
            'occupation' => 'Род деятельности',
            'training_experience' => 'Стаж тренировок',
            'fitnes' => 'Фитнес-клуб',
            'training_goal' => 'Цель тренировок',
            'new_password' => 'Новый пароль',
            'file' => 'Фотография',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
//            $this->password = md5($this->password);
            $this->register_date = date('Y-m-d');
        }

        if ($this->new_password != null) {
            $this->password = md5($this->new_password);
        }
        return parent::beforeSave($insert);
    }

    public function beforeDelete()
    {
        if ($this->photo != null && file_exists('avatars/' . $this->photo)) unlink(Yii::getAlias('avatars/' . $this->photo));
        return parent::beforeDelete();
    }

    public function getRoleList()
    {
        $id = Yii::$app->user->identity -> id;
        if (!empty($id) && Functions::getUserRole($id) == 'Тренер') {
            return ArrayHelper::map([
                ['id' => self::USER_ROLE_CLIENT, 'name' => 'Клиент',],
                ['id' => self::USER_ROLE_TRENER, 'name' => 'Тренер',],
            ], 'id', 'name');
        }
        return ArrayHelper::map([
            ['id' => self::USER_ROLE_ADMIN, 'name' => 'Администратор',],
            ['id' => self::USER_ROLE_CLIENT, 'name' => 'Клиент',],
            ['id' => self::USER_ROLE_TRENER, 'name' => 'Тренер',],
        ], 'id', 'name');
    }

    public function getRoleDescription()
    {
        if (self::USER_ROLE_ADMIN == $this->permission) return 'Администратор';
        if (self::USER_ROLE_CLIENT == $this->permission) return 'Клиент';
        if (self::USER_ROLE_TRENER == $this->permission) return 'Тренер';
    }

    public function getStatusList()
    {
        return ArrayHelper::map([
            ['id' => '1', 'title' => 'Активный',],
            ['id' => '0', 'title' => 'Неактивный',],
        ],
            'id', 'title');
    }

    public function getGenderList()
    {
        return ArrayHelper::map([
            ['id' => '1', 'title' => 'Мужской',],
            ['id' => '0', 'title' => 'Женский',],
        ],
            'id', 'title');
    }

    public function getGenderDescription()
    {
        if ($this->gender == 1) return 'Мужской';
        else return 'Женский';
    }

    public function getTrenerList()
    {
        $trener = Users::find()->where(['permission' => Users::USER_ROLE_TRENER])->all();
        return ArrayHelper::map($trener, 'id', 'fio');
    }

    //Получить последнего ид
    public function getLastId()
    {
        $q = new \yii\db\Query();
        $res = $q->select("AUTO_INCREMENT")
            ->from('INFORMATION_SCHEMA.TABLES')
            ->where("TABLE_SCHEMA = DATABASE() AND TABLE_NAME = 'users'")
            ->one();
        if ($res) return $res["AUTO_INCREMENT"];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoachingUsers()
    {
        return $this->hasMany(CoachingUsers::class, ['users_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNutritionsUsers()
    {
        return $this->hasMany(NutritionsUsers::class, ['users_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoachings()
    {
        if($this->permission == Users::USER_ROLE_TRENER) {
            return $this->hasMany(Coaching::class, ['trener_id' => 'id']);
        } else if($this->permission == Users::USER_ROLE_CLIENT) {
            return $this->hasMany(Coaching::class, ['id' => 'coaching_id'])->viaTable('coaching_users', ['users_id' => 'id']);
        }
    }

    public function getNutritions()
    {
        if($this->permission == Users::USER_ROLE_TRENER) {
            return $this->hasMany(Nutritions::class, ['trener_id' => 'id']);
        } else if ($this->permission == Users::USER_ROLE_CLIENT) {
            return $this->hasMany(Nutritions::class, ['id' => 'nutritions_id'])->viaTable('nutritions_users', ['users_id' => 'id']);
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExercises()
    {
        return $this->hasMany(Exercise::class, ['trener_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainings()
    {
        return $this->hasMany(Training::class, ['trener_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMyTrainer()
    {
        return $this->hasOne(Users::class, ['id' => 'trener']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments()
    {
        if($this->permission == Users::USER_ROLE_TRENER){
            return $this->hasMany(Payments::class, ['trainer_id' => 'id']);
        } else if ($this->permission == Users::USER_ROLE_CLIENT) {
            return $this->hasMany(Payments::class, ['client_id' => 'id']);
        }
    }

    /**
     * @return bool
     */
    public function getIsAdmin()
    {
        if($this->permission == self::USER_ROLE_ADMIN){
            return true;
        }

        return false;
    }

    /**
     * @return string
     */
    public function getInstagramProfileUrl()
    {
        $value = new InstagramProfileUrl(['input' => $this->instagram]);
        return $value->url;
    }

    /**
     * Возвращает партнерскую ссылку
     * @return string
     */
    public function getReferralUrl()
    {
        return Url::toRoute(['site/register', 'ref' => $this->id], true);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMyReferrals()
    {
        return $this->hasMany(AffiliateProgram::class, ['owner_id' => 'id']);
    }

    /**
     * @return AffiliateProgram
     */
    public function getMyReferrer()
    {
        return AffiliateProgram::find()->where(['registered_id' => $this->id])->one();
    }

    /**
     * Получить все запросы текущего клиента
     * @return \yii\db\ActiveQuery
     */
    public function getClientRequests()
    {
        return $this->hasMany(ClientsRequests::class, ['client_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoachingCompletings()
    {
        return $this->hasMany(CoachingCompleting::class, ['client_id' => 'id']);
    }

    /**
     * Включен ли маркер уведомления
     * @param $markerAttribute
     * @return bool
     */
    public function isEnableNotificationMarker($markerAttribute)
    {
        return $this->userNotificationsMarkers->$markerAttribute == 1 ? true : false;
    }

    /**
     * Включает маркер уведомления
     * @param $markerAttribute
     */
    public function enableMarker($markerAttribute)
    {
        $markers = $this->userNotificationsMarkers;
        $markers->$markerAttribute = 1;
        $markers->save(false);
    }

    /**
     * Выключает маркер уведомления
     * @param $markerAttribute
     */
    public function disableMarker($markerAttribute)
    {
        $markers = $this->userNotificationsMarkers;
        $markers->$markerAttribute = 0;
        $markers->save(false);
    }

    /**
     * @return UserNotificationsMarker
     */
    public function getUserNotificationsMarkers()
    {
        $markers = UserNotificationsMarker::findOne(['user_id' => $this->id]);
        if($markers == null){
            $markers = new UserNotificationsMarker(['user_id' => $this->id]);
            $markers->save(false);
        }

        return $markers;
    }

    /**
     * Получить запросы клиентов
     * @return \yii\db\ActiveQuery
     */
    public function getRequests()
    {
        return $this->hasMany(ClientsRequests::class, ['trainer_id' => 'id']);
    }

    /**
     * Отправляет запрос тренеру
     * @param integer $trainer_id
     * @return bool
     */
    public function sendRequest($trainer_id)
    {
        $trainer = Users::findOne($trainer_id);
        if($trainer == null){
            return false;
        }

        $request = new ClientsRequests([
            'client_id' => Yii::$app->user->getId(),
            'trainer_id' => $trainer_id,
        ]);

        $trainer->enableMarker('client_requests_trainer');

        return $request->save(false);
    }

    /**
     * @return null|string
     */
    public function getCountryName()
    {
        return Country::getCountryName($this->country);
    }

    public function getRole($id = null)
    {
        if ($id == null) {
            $id = Yii::$app->user->identity ->getId();
        }
        $roleModel = Yii::$app->db
            ->createCommand("Select * from auth_assignment where user_id=" . $id)
            ->queryOne();

        return $roleModel['item_name'];
    }
   
}
