<?php

namespace app\models;
use Yii;
use yii\base\NotSupportedException;

class User extends Users/*\yii\base\Object*/ implements \yii\web\IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::find()->where(['login' => $username])->orWhere(['email' => $username])->one();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
       return $this->password == md5($password);
    }

    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }


    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = $this->generateCode(5);
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function setPassword($password)
    {
        $this->password = md5($password);
    }

    private function generateMyCode(){
        $mycode='';
        for($i=0; $i<4;$i++){
            $mycode.=random_int(1,9);
        }
        $referer=User::find()->where(['mycode' => $mycode])->one();
        if($referer){
            $this->generateMyCode();
        } else {
            $this->mycode=$mycode;
            $this->save();
        }
    }
    private function generateCode($length=4){
        $code='';
        for($i=0; $i<$length;$i++){
            $code.=random_int(1,9);
        }
        return $code;
    }
    public function setReferalTo($code){
        $referer=User::find()->where(['mycode' => $code])->one();
        if($referer && $referer->id!=$this->id){
            $referal= new Referals();
            $referal->referer_id=$referer->id;
            $referal->referal_id=$this->id;
            $referal->save();
            $this->generateMyCode();
        }
        return true;
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        return static::findOne([
            'password_reset_token' => $token,
            /*'status' => self::STATUS_ACTIVE,*/
        ]);
    }

    
}
