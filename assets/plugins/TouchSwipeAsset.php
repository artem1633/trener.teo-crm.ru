<?php

namespace app\assets\plugins;

use yii\web\AssetBundle;

/**
 * Class TouchSwipeAsset
 * @package app\assets\plugins
 */
class TouchSwipeAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [

    ];
    public $js = [
        'plugins/touchSwipe/jquery.touchSwipe.min.js',
    ];

    public $depends = [
        'app\assets\AppAsset',
    ];
}