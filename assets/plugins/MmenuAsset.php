<?php

namespace app\assets\plugins;

use yii\web\AssetBundle;

/**
 * Class MmenuAsset
 * @package app\assets\plugins
 */
class MmenuAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'plugins/mmenu/jquery.mmenu.all.css',
        'plugins/hamburgers/hamburgers.min.css',
    ];
    public $js = [
        'plugins/mmenu/jquery.mmenu.all.js',
        'js/mmenu.init.js'
    ];

    public $depends = [
        'app\assets\AppAsset',
    ];
}