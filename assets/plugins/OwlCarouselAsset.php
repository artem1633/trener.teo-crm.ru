<?php

namespace app\assets\plugins;

use yii\web\AssetBundle;

/**
 * Class OwlCarouselAsset
 * @package app\assets\plugins
 */
class OwlCarouselAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'plugins/owlCarousel/owl.carousel.min.css',
    ];
    public $js = [
        'plugins/owlCarousel/owl.carousel.min.js',
    ];

    public $depends = [
        'app\assets\AppAsset',
    ];
}