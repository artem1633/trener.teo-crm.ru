<?php

namespace app\formatters;

use yii\i18n\Formatter;
use php_rutils\RUtils;

/**
 * Class RUtilsDtFormatter
 * @package app\formatters
 */
class RUtilsDtFormatter extends Formatter
{
    /**
     * @var int
     */
    public $accuracy = 3;

    /**
     * Сравнивает текущие время и время которое указано $datetime
     * @param string $datetime
     * @return string
     */
    public function asTimePass($datetime)
    {
        $now = time();
        $timestamp = strtotime($datetime);
        $diff = $now - $timestamp;

        if($diff < 86400 && date('d', $timestamp) == date('d', $now))
        {
            return RUtils::dt()->distanceOfTimeInWords(date('Y-m-d H:i:s', $timestamp), date('Y-m-d H:i:s'), $this->accuracy);
        } else if (date('Y-m-d', $timestamp) == date('Y-m-d', strtotime('-1 days'))){
            return 'Вчера в '.$this->asTime($datetime);
        } else {
            return $this->asDatetime($datetime);
        }
    }
}