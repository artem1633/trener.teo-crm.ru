if(window.android == undefined) {
    var submitType = '';

    $('[data-img-upload="upload"], [data-upload="upload"]').click(function(){
        submitType = 'self-avatar';
        var url = $('#upload-img-form').data('avatar-url');
        $('#upload-img-form').attr('action', url);

        $('#upload-img-form input').trigger('click');
    });

    $('[data-form-img-upload]').click(function(){
        submitType = 'form-avatar';
        var url = $('#upload-img-form').data('form-url');
        $('#upload-img-form').attr('action', url);

        console.log('click handler 2');
        $('#upload-img-form input').trigger('click');
    });

    $('#upload-img-form input').change(function(){
        console.log('change handler');

        $('#upload-img-form').submit();
    });

    $("#upload-img-form").submit(function(e){
        e.preventDefault();
        var formData = new FormData($(this)[0]);

        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: formData,
            async: false,
            success: function (data) {
                if(submitType == 'self-avatar')
                {
                    $('[data-img-upload="upload"]').each(function(){
                        $(this).attr('src', '/'+data.path);
                    });
                } else if (submitType == 'form-avatar') {
                    $('[data-form-img-upload]').attr('src', '/'+data.path);
                    var id = $('[data-form-img-upload]').data('form-img-upload');
                    $('#'+id).val(data.path);
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });

        return false;
    });
} else {
    $('[data-img-upload="upload"], [data-upload="upload"]').click(function(){
        var action = $("#upload-img-form").data('action');
        android.selectFile(action, 0, yii.getCsrfToken());
    });

    $('[data-form-img-upload]').click(function(){
        submitType = 'form-avatar';
        var action = $('#upload-img-form').data('form-url');
        // $('#upload-img-form').attr('action', url);

        // console.log('click handler 2');
        // $('#upload-img-form input').trigger('click');

        android.selectFile(action, 0, yii.getCsrfToken());

    });

    $('#upload-img-form input').change(function(){
        console.log('change handler');

        $('#upload-img-form').submit();
    });
}

