$(".items-list li").each(function(){
    var height = $(this).height();
    $(this).find('.item-action').css('height', height);
});


$(".items-list li").swipe({
    swipe:function(event, direction, distance, duration, fingerCount) {
        if(direction == 'left')
        {
            var count = $(this).find('.item-action').length;
            var actionsWidth = count * 10;
            var listItemWidth = 100 - actionsWidth;

            $(this).find('.list-item').css('width', listItemWidth+'%');

            $(this).find('.item-action').each(function(){
                $(this).css('width', '10%');
            });
        } else if (direction == 'right') {
            $(this).find('.list-item').css('width', '100%');
            $(this).find('.item-action').each(function(){
                $(this).css('width', '0');
            });
        }
    }
});
