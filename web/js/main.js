$(function(){
	$('#modalButton').click(function(){
		$('#modal').modal('show')
			.find('#modalContent')
			.load($(this).attr('value'));
	});
});

var Controlls = {
        sendBinaryRequest: function(el, url, data, successText, errorText, doneCallback){
            if(successText == undefined){
                successText = '<i class="fa fa-check"></i> Запрос успешно отправлен';
            }
            if(errorText == undefined){
                errorText = 'Ошибка при отправке запроса';
            }

            var csrfParam = yii.getCsrfParam();
            var csrfToken = yii.getCsrfToken();
            data[csrfParam] = csrfToken;
            $.ajax({
                'method': 'POST',
                'url': url,
                'data': data,
            }).done(function(data){
                if(doneCallback == undefined){
                    if(data.result) {
                        el.removeClass('btn-theme');
                        el.addClass('btn-default');
                        el.html(successText);
                    } else {
                        el.removeClass('btn-theme');
                        el.addClass('btn-danger');
                        el.html(errorText);
                    }
                } else {
                    doneCallback(data);
                }
            }).fail(function(error){
                el.removeClass('btn-theme');
                el.addClass('btn-danger');
                el.html('Ошибка '+error.statusText);
            }).always(function(data) {
                el.attr('onclick', '');
                el.unbind();
            });
        },
    sendOneRequest: function(el, url, data, successText, errorText, doneCallback){
        if(successText == undefined){
            successText = '<i class="fa fa-check"></i> Запрос успешно отправлен';
        }
        if(errorText == undefined){
            errorText = 'Ошибка при отправке запроса';
        }

        var csrfParam = yii.getCsrfParam();
        var csrfToken = yii.getCsrfToken();
        data[csrfParam] = csrfToken;
        $.ajax({
            'method': 'POST',
            'url': url,
            'data': data,
        }).done(function(data){
            if(doneCallback == undefined){
                if(data.result) {
                    el.removeClass('btn-theme');
                    el.addClass('btn-default');
                    el.html(successText);
                } else {
                    el.removeClass('btn-theme');
                    el.addClass('btn-danger');
                    el.html(errorText);
                }
            } else {
                doneCallback(data);
            }
        }).fail(function(error){
            // el.removeClass('btn-theme');
            // el.addClass('btn-danger');
            // el.html('Ошибка '+error.statusText);
        }).always(function(data) {
            // el.attr('onclick', '');
            // el.unbind();
        });
    },
};

function fileLoadedCallback(response){

    $('body').hide();
}

function copyToClipboard(elem) {
    // create hidden text element, if it doesn't already exist
    var targetId = "_hiddenCopyText_";
    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
    var origSelectionStart, origSelectionEnd;
    if (isInput) {
        // can just use the original source element for the selection and copy
        target = elem;
        origSelectionStart = elem.selectionStart;
        origSelectionEnd = elem.selectionEnd;
    } else {
        // must use a temporary form element for the selection and copy
        target = document.getElementById(targetId);
        if (!target) {
            var target = document.createElement("textarea");
            target.style.position = "absolute";
            target.style.left = "-9999px";
            target.style.top = "0";
            target.id = targetId;
            document.body.appendChild(target);
        }
        target.textContent = elem.textContent;
    }
    // select the content
    var currentFocus = document.activeElement;
    target.focus();
    target.setSelectionRange(0, target.value.length);

    // copy the selection
    var succeed;
    try {
        succeed = document.execCommand("copy");
    } catch(e) {
        succeed = false;
    }

    // restore original focus
    if (currentFocus && typeof currentFocus.focus === "function") {
        currentFocus.focus();
    }

    if (isInput) {
        // restore prior selection
        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    } else {
        // clear temporary content
        target.textContent = "";
    }
    return succeed;
}