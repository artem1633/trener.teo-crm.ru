<?php
/**
 * Created by PhpStorm.
 * User: indigo
 * Date: 11/9/18
 * Time: 10:35 PM
 */
use app\models\Users;
use app\models\Functions;

$permission = Yii::$app->user->identity->permission;
$id = Yii::$app->user->identity->id;
?>

<!-- The Right Sidebar -->
      <aside class="control-sidebar control-sidebar-light">
        <!-- Content of the sidebar goes here -->
          <?php
          $clients = Functions::getTrainerClients();
          print_r($clients);
          if (count($clients) > 0) { ?>
              <ul>
                  <?php
                    foreach ($clients as $client) { ?>
                        <li>
                            <a style="color: #fff" href="/users?id=<?php $client['id'] ?>"><?= $client['name'] ?></a>
                        </li>
                    <?php   }
                  ?>
              </ul>
         <?php }
          ?>
      </aside>
      <!-- The sidebar's background -->
      <!-- This div must placed right after the sidebar for it to work-->
      <div class="control-sidebar-bg"></div>


$a = [
"a" => "green","pink","blue","red","brown",
"r" => "red","red"
];
$b = [
"b" => "green","brown","black","yellow","pink",
];
print_r((array_diff($a, $b)));